
#ifndef UNROLLINGANDJAM_UIUC
#define UNROLLINGANDJAM_UIUC

#include <cstdlib>
#include <sage3basic.h>
#include <Cxx_Grammar.h>
#include <sageBuilder.h>
#include <Rose/StringUtility.h>
#include <string>
#include <vector>
#include <AnnotParser.hh>
#include <Result.hh>
#include <constantFolding.h>
#include <SageInterfaceUIUC.hh>

using namespace std;

namespace SB = SageBuilder;
namespace SI = SageInterface;
namespace SIUI = ICE::SageInterfaceUIUC;

namespace ICE
{
	namespace UnrollAndJamUIUC
	{
		bool enable_debug = false;
                bool needFringe = true;
		static const std::string PRAGMA_NAME("uiuc_unrollandjam");

 		void commandLineProcessing(std::vector<std::string> &argvList); 
		Result unrollandjam(SgPragmaDeclaration* prag);
		Result unrollandjamFortran(SgStatement * s, 
			      	     vector<int>& pragmainfo);
		Result unrollandjam(SgStatement * s, 
                              size_t unrollandjaming_factor,
                              size_t target_level,
                              SgPragmaDeclaration *p);
		size_t unrollandjamAll(SgProject * project);
		bool loopUnrollAndJaming(SgForStatement *s, 
        			   size_t unrollandjaming_factor, 
				   size_t target_level,
                                   SgPragmaDeclaration* p);
                //bool loopUnrollAndJaming(SgForStatement* target_loop, 
                //                size_t unrollandjaming_factor, SgPragmaDeclaration *p);

		bool loopUnrollAndJaming(SgFortranDo* loop_nest, 
				   size_t unrollandjaming_factor, 
				   size_t target_level);
		bool loopUnrollAndJaming(SgFortranDo* target_loop, 
				   size_t unrollandjaming_factor);

                //bool jamming(SgBasicBlock *& body, SgBasicBlock * orig_bodyi, size_t unroll_factor);

		//bool loopUnrolling(SgForStatement * target_loop, size_t unrollandjaming_factor);	

	}
}	

#endif
