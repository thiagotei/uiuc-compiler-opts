#ifndef ROSE_TRAV_UIUC
#define ROSE_TRAV_UIUC


//#include <AstProcessing.h>
#include <rose.h>
#include <iostream>

using namespace std;

class visitorTraversal : public AstSimpleProcessing{
public:
	visitorTraversal();
	virtual void visit(SgNode * n);
	virtual void atTraversalEnd();
};

#endif
