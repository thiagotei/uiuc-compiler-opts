#include <OutlinerUIUC.hh>

using namespace std;

//! Simplest outlining directives, applied to a single statement.
static const std::string PRAGMA_OUTLINE ("rose_outline");
//! Stores a list of valid outlining pragma directives.
typedef Rose_STL_Container<SgPragmaDeclaration *> PragmaList_t;
//! Stores a list of valid outlining targets, used only for Fortran for now
typedef Rose_STL_Container<SgStatement*> TargetList_t;

static void  dumpVars(const ASTtools::VarSymSet_t& syms) {
	cout << "[dumpVars] begin ";
	for (ASTtools::VarSymSet_t::const_iterator i = syms.begin (); i != syms.end (); ++i) 
	{
//		SageInterface::dumpInfo(i);			
		cout << (*i)->get_name().getString() << " ";	

	}
	cout << " end" << endl;
}

static void  dumpInitdNames(const std::set< SgInitializedName *>& syms) {
	cout << "[dumpVars] begin ";
	for (std::set< SgInitializedName *>::const_iterator i = syms.begin (); i != syms.end (); ++i) 
	{
//		sageinterface::dumpinfo(i);			
		cout << (*i)->get_name().getString() << " ";	

	}
	cout << " end" << endl;
}


//!  A helper function to decide if some variables need to be restored from their clones in the end of the outlined function
// This is needed to support variable cloning 
// Input are:
//      All the variables
//      read-only variables
//      live-out variables
//
// The output is restoreVars, which is isWritten && isLiveOut --> !isRead && isLiveOut 
static void calculateVariableRestorationSet(const ASTtools::VarSymSet_t& syms,
                                     const std::set<SgInitializedName*> & readOnlyVars,
                                     const std::set<SgInitializedName*> & liveOutVars,
                                     std::set<SgInitializedName*>& restoreVars)
{
  for (ASTtools::VarSymSet_t::const_reverse_iterator i = syms.rbegin ();
      i != syms.rend (); ++i)
  {
    SgInitializedName* i_name = (*i)->get_declaration ();
    //conservatively consider them as all live out if no liveness analysis is enabled,
    bool isLiveOut = true;
    if (Outliner::enable_liveness)
      if (liveOutVars.find(i_name)==liveOutVars.end())
        isLiveOut = false;

    // generate restoring statements for written and liveOut variables:
    //  isWritten && isLiveOut --> !isRead && isLiveOut --> (findRead==NULL && findLiveOut!=NULL)
    // must compare to the original init name (i_name), not the local copy (local_var_init)
    if (readOnlyVars.find(i_name)==readOnlyVars.end() && isLiveOut)   // variables not in read-only set have to be restored
      restoreVars.insert(i_name);
  }
}


//! A helper function to decide for the classic outlining, if a variable should be passed using its original type (a) or its pointer type (&a)
// For simplicity, we assuming Pass-by-reference (using AddressOf()) = all_variables - read_only_variables 
// So all variables which are written will use addressOf operation to be passed to the outlined function
// TODO: add more sophisticated logic, C++ reference, C array, Fortran variable etc. 
static void calculateVariableUsingAddressOf(const ASTtools::VarSymSet_t& syms, const std::set<SgInitializedName*> readOnlyVars,  ASTtools::VarSymSet_t& addressOfVarSyms)
{
  for (ASTtools::VarSymSet_t::const_reverse_iterator i = syms.rbegin ();
      i != syms.rend (); ++i)
  {
    // Basic information about the variable to be passed into the outlined function
    // Variable symbol name
    SgInitializedName* i_name = (*i)->get_declaration ();
    if (readOnlyVars.find(i_name)==readOnlyVars.end()) // not readonly ==> being written ==> use addressOf to be passed to the outlined function
      addressOfVarSyms.insert (*i);
  } // end for
}
/*!
 *  \brief Check whether the specified pragma is an outlining
 *  directive.
 *
 *  This routine checks whether the specified pragma is an outlining
 *  directive, and if so, returns the statement that should be
 *  outlined. Returns NULL if the pragma is not an outlining directive
 *  or no such statement exists.
 */
static
SgStatement *
processPragma (SgPragmaDeclaration* decl)
{
  if (!decl || !decl->get_pragma ())
    return 0;

  string pragmaString = decl->get_pragma ()->get_pragma ();
  if (pragmaString != PRAGMA_OUTLINE) // Not an outlining pragma.
    return 0;
    
  // Get statement to outline
  return const_cast<SgStatement *> (SageInterface::getNextStatement(decl));
  //return const_cast<SgStatement *> (ASTtools::findNextStatement (decl));
}

/*!
 *  \brief Collects all outlining pragmas.
 *
 * This routine scans the given project for all outlining pragmas, and
 * returns them in the order in which they should be processed.
 *
 * The ordering is important because neither preorder nor postorder
 * tree traversals yield the desired bottomup processing for outlining
 * pragmas. To see why, consider the following code example:
 *
 * \code
 * #pragma rose_outline
 * {
 *   #pragma rose_outline
 *   statement1;
 * }
 * \endcode
 *
 * The corresponding AST is:
 *
 *   SgBasicBlock1
 *     /      \
 *    /        \
 * SgPragma1  SgBasicBlock2
 *              /      \
 *          SgPragma2  SgStatement1
 *
 * The standard traversal orders are:
 *
 * - Preorder: bb1, pragma1, bb2, pragma2, stmt1
 * - Postorder: pragma1, pragma2, stmt1,bb2, bb1
 *
 * In both cases, pragma1 is always visited before pragma2.
 *
 * The routine obtains a "correct" ordering by using the default
 * preorder AST query and then reversing the results.  In this we, we
 * obtain the ordering:
 *
 * - stmt1, pragma2, bb2,pragma1, bb1
 */
static size_t
collectPragmas (SgProject* proj, PragmaList_t& pragmas)
{
  typedef Rose_STL_Container<SgNode *> NodeList_t;
  NodeList_t raw_list = NodeQuery::querySubTree (proj, V_SgPragmaDeclaration);
  size_t count = 0;
  for (NodeList_t::reverse_iterator i = raw_list.rbegin ();
       i != raw_list.rend (); ++i)
    {
      SgPragmaDeclaration* decl = isSgPragmaDeclaration (*i);
      if (processPragma (decl))
        {
          pragmas.push_back (decl);
          ++count;
        }
    }
  return count;
}

/*!
 *  \brief Check whether the specified comment is an outlining
 *  directive for Fortran: one of the following three cases: 
 *    !$rose_outline
 *    c$rose_outline
 *    *$rose_outline
 *  This routine checks whether the specified source comment an outlining
 *  directive, and if so, returns the statement that should be
 *  outlined. Returns NULL if the comment is not an outlining directive
 *  or no such statement exists.
 *  Liao, 12/19/2008
 */
static
SgStatement *
processFortranComment(SgLocatedNode* node)
{
  SgStatement* target = NULL;
  ROSE_ASSERT(node);
  AttachedPreprocessingInfoType *comments =
    node->getAttachedPreprocessingInfo ();
  if (comments==NULL)
    return 0;
  AttachedPreprocessingInfoType::iterator i;
  std::vector< PreprocessingInfo* > removeList;
  for (i = comments->begin (); i != comments->end (); i++)
  { 
    if ((*i)->getTypeOfDirective() == PreprocessingInfo::FortranStyleComment)
    {
      string commentString = (*i)->getString();
      boost::algorithm::trim(commentString);
      if (   (commentString == "!$"+PRAGMA_OUTLINE) 
          || (commentString == "c$"+PRAGMA_OUTLINE) 
          || (commentString == "*$"+PRAGMA_OUTLINE)) 
      {
        target = isSgStatement(node);
        if (target==NULL)
        {
          cerr<<"Unhandled case when a Fortran statement is attached to a non-statement node!!"<<endl;
          ROSE_ASSERT(false);
        }
        removeList.push_back(*i);
      }
    } // end if Fortran comment
  } // end for-loop

  // remove those special comments
  for (std::vector<PreprocessingInfo* >::iterator j = removeList.begin();
       j!=removeList.end();j++)
  {
    comments->erase(find(comments->begin(), comments->end(),*j));

 // DQ (2/27/2009): Don't mix free with C++ code (malloc/free for C and new/delete for C++)
 // free memory also
 // free(*j);
    delete(*j);
  }

  return target; // const_cast<SgStatement *> (target);
}
//! Collect target Fortran statement with matching comments for rose_outline
// Save them into targetList
static size_t
collectFortranTarget (SgProject* proj, TargetList_t& targets)
{
  typedef Rose_STL_Container<SgNode *> NodeList_t;
  NodeList_t raw_list = NodeQuery::querySubTree (proj, V_SgStatement);
  size_t count = 0;
  for (NodeList_t::reverse_iterator i = raw_list.rbegin ();
      i != raw_list.rend (); ++i)
  {
    SgStatement* decl = isSgStatement(*i);
    if (processFortranComment(decl))
    {
      targets.push_back (decl);
      ++count;
    }
  }
  return count;
}


Outliner::Result
Outliner::UIUC::outline (SgStatement* s)
{ 
	cout << "[OutlinerUIUC] outline (s)" << endl;
	string func_name;
	if(newFileName.size() == 0) {
		func_name = generateFuncName (s);
	} else {
		stringstream ss;
		ss << newFileIndex;
		func_name = Outliner::UIUC::newFileName + "_" + ss.str();
		newFileIndex++;
	}

//	if(Outliner::enable_debug) 
	cout << "[Outliner:UIUC:outline] file name used " << func_name << endl; 

  return UIUC::outline (s, func_name);
}

Outliner::Result
Outliner::UIUC::outline (SgStatement* s, const std::string& func_name)
{
	cout << "[OutlinerUIUC] outline (s, func_name)" << endl;
//cout<<"Debug Outliner::outline() input statement is:"<<s<<endl;  
  SgBasicBlock* s_post = preprocess (s);
//cout<<"Debug Outliner::outline() preprocessed statement is:"<<s_post<<endl;  
  ROSE_ASSERT (s_post);
#if 0
  //Liao, enable -rose:outline:preproc-only, 
  // then any translator can accept it even if their drivers do not handle it individually
  // Internal usage only for debugging
  static bool preproc_only_ = false; 
  SgFile * file= SageInterface::getEnclosingFileNode(s);
  SgStringList argvList = file->get_originalCommandLineArgumentList (); 


  if (CommandlineProcessing::isOption (argvList,"-rose:outline:",
                                     "preproc-only",true))
  {
    cout << "==> Running the outliner's preprocessing phase only." << endl;
    preproc_only_ = true;
  // Avoid passing the option to the backend compiler 
    file->set_originalCommandLineArgumentList(argvList);
  }  
#endif
  if (preproc_only_)
  { 
    Outliner::Result fake;
    return fake;
  }  
  else
  {
 // return Transform::outlineBlock (s_post, func_name);
    
    Outliner::Result returnResult = outlineBlock (s_post, func_name);

#if 0
// This is now done in ASTtools::replaceStatement().
// DQ (2/24/2009): I think that at this point we should delete the subtree represented by "s_post"
// But it might have made more sense to not do a deep copy on "s_post" in the first place.
// Why is there a deep copy on "s_post"?
   SageInterface::deleteAST(s_post);
#else
   if (enable_debug)
     printf ("############ Skipped deletion of AST subtree at s_post = %p = %s \n",s_post,s_post->class_name().c_str());
#endif

   return returnResult;
  }
}


  //! Build a SgFile node
SgFile*
Outliner::UIUC::buildFile(const std::string& inputFileName, const std::string& outputFileName, SgProject* project/*=NULL*/)
   {
	if(Outliner::enable_debug) cout << "[OutlinerUIUC][buildFile] Starting." << endl;
// Note that ROSE_USE_INTERNAL_FRONTEND_DEVELOPMENT defines a reduced set of ROSE to support front-end specific development.
// It is mostly used by quinlan to support laptop development where the smaller set of files permits one to do limited
// development work on a Mac (even with OSX's poor performance with large numbers of debug symbols).  This is an 
// infrequently used option.
#ifndef ROSE_USE_INTERNAL_FRONTEND_DEVELOPMENT

#if 0
     printf ("In SageBuilder::buildFile(inputFileName = %s, outputFileName = %s, project = %p \n",inputFileName.c_str(),outputFileName.c_str(),project);
#endif

     ROSE_ASSERT(inputFileName.size()!=0);// empty file name is not allowed.
     string sourceFilename = inputFileName, fullname;
     Rose_STL_Container<std::string> arglist;
     int nextErrorCode = 0;

     if (project == NULL)
      // SgProject is created on the fly
      // Make up an arglist in order to reuse the code inside SgFile::setupSourceFilename()
        {
#if 0
          printf ("In SageBuilder::buildFile(): build the SgProject \n");
#endif
          project = new SgProject();
          ROSE_ASSERT(project);
          project->get_fileList().clear();

          arglist.push_back("cc"); 
          arglist.push_back("-c"); 
          project->set_originalCommandLineArgumentList (arglist);
        }

     ifstream testfile(inputFileName.c_str());
     if (!testfile.is_open()) 
        {
       // create a temporary file if the file does not exist.
       // have to do this, otherwise StringUtility::getAbsolutePathFromRelativePath() complains
       // which is called by result->setupSourceFilename(arglist);
          testfile.close();
          ofstream outputfile(inputFileName.c_str(),ios::out); 
       // DQ (2/6/2009): I think this comment is helpful to put into the file (helps explain why the file exists).
       //
	if (SageInterface::is_Fortran_language ()) { 
          outputfile<<"! Output file generated so that StringUtility::getAbsolutePathFromRelativePath() will see a vaild file ... unparsed file will have rose_ prefix "<<endl;
	} else {
          outputfile<<"//Output file generated so that StringUtility::getAbsolutePathFromRelativePath() will see a vaild file ... unparsed file will have rose_ prefix "<<endl;
	}
          outputfile.close();
        }
       else // file already exists , load and parse it
        {
       // should not reparse all files in case their ASTs have unsaved changes, 
       // just parse the newly loaded file only.
       // use argv here, change non-existing input file later on
       // TODO add error code handling 

       // DQ (2/6/2009): Avoid closing this file twice (so put this here, instead of below).
          testfile.close();
          // should remove the old one here, Liao, 5/1/2009
        }

	if(Outliner::enable_debug) cout << "[OutlinerUIUC][buildFile] uow 1." << endl;

  // DQ (2/6/2009): Avoid closing this file twice (moved to false branch above).
  // testfile.close();

  // DQ (2/6/2009): Need to add the inputFileName to the source file list in the project, 
  // because this list will be used to subtract off the source files as required to build 
  // the commandline for the backend compiler.
     project->get_sourceFileNameList().push_back(inputFileName);

     Rose_STL_Container<string> sourceFilenames = project->get_sourceFileNameList();
  // printf ("In SageBuilder::buildFile(): sourceFilenames.size() = %" PRIuPTR " sourceFilenames = %s \n",sourceFilenames.size(),StringUtility::listToString(sourceFilenames).c_str());

     arglist = project->get_originalCommandLineArgumentList();

  // DQ (2/6/2009): We will be compiling the source code generated in the 
  // "rose_<inputFileName>" file, so we don't want this on the argument stack.
     arglist.push_back(sourceFilename);

  // DQ (2/6/2009): Modified.
  // There is output file name specified for rose translators
     if (outputFileName.empty() == false)
        {
          arglist.push_back("-rose:o");
       // arglist.push_back("-o");
          arglist.push_back(outputFileName);
        }

	if(Outliner::enable_debug) cout << "[OutlinerUIUC][buildFile] uow 2." << endl;
  // DQ (4/15/2010): Turn on verbose mode
  // arglist.push_back("-rose:verbose 2");

  // This handles the case where the original command line may have referenced multiple files.
     Rose_STL_Container<string> fileList = CommandlineProcessing::generateSourceFilenames(arglist,/* binaryMode = */ false);
     CommandlineProcessing::removeAllFileNamesExcept(arglist,fileList,sourceFilename);

  // DQ (9/3/2008): Added support for SgSourceFile IR node
  // SgFile* result = new SgFile (arglist, nextErrorCode, 0, project);
  // AS(10/04/08) Because of refactoring we require the determineFileType function to be called 
  // to construct the node.
  // SgSourceFile* result = new SgSourceFile (arglist, nextErrorCode, 0, project);
  // SgSourceFile* result = isSgSourceFile(determineFileType(arglist, nextErrorCode, project));
  // TH (2009-07-15): changed to more generig isSgFile, this also supports SgBinaryComposite
     SgFile* result = determineFileType(arglist, nextErrorCode, project);
     ROSE_ASSERT(result != NULL);

     if(Outliner::enable_debug) cout << "[OutlinerUIUC][buildFile] result file name "<<result->getFileName() << " ." << endl;

#if 0
     printf ("In SageBuilder::buildFile(): project = %p project->get_fileList_ptr()->get_listOfFiles().size() = %" PRIuPTR " \n",project,project->get_fileList_ptr()->get_listOfFiles().size());
#endif

#if 0
  // DQ (3/4/2014): This fix is only for Java and for C will cause a second SgFile to be redundently added to the file list.
  // For now I will provide a temporary fix and check is this is for a Java project so that we can continue. But the longer
  // term fix would be to make the semantics for Java the same as that of C/C++ (or the other way around, whatever is the 
  // cleaner semantics.
  // This just adds the new file to the list of files stored internally (note: this sets the parent of the newFile).
  // TOO1 (2/28/2014): This is definitely required for Java (ECJ frontend), though C passes without it (I think only
  //                   by luck :-).
  //                   The ECJ frontend uses the SgProject internally (via a global SgProject*). Therefore, the
  //                   SgProject must contain this newly created SgFile, otherwise ECJ won't be able to find it.
  // project->set_file ( *result );
     if (project->get_Java_only() == true)
        {
       // DQ (3/4/2014): For now we want to output a message and clean this up afterward (likely in the Java language support).
          printf ("WARNING: Java specific action to add new file to SgProject (using set_file()) (more uniform language handling symantics would avoid this problem) \n");
          project->set_file ( *result );
        }
#else
  // DQ (3/6/2014): The code below adresses the specific bug faced in the use of the outliner (so we use it directly).
  // This code was moved ahead of the call to "result->runFrontend(nextErrorCode);" because in the case of Java
  // the file must be set to be a part of the SgProject before calling the runFrontend() function.
  // project->set_file ( *result );

     result->set_parent(project);

#if 0
     printf ("In SageBuilder::buildFile(): Outliner::use_dlopen = %s \n",Outliner::use_dlopen ? "true" : "false");
#endif

	if(Outliner::enable_debug) cout << "[OutlinerUIUC][buildFile] uow 3." << endl;
  // DQ (3/5/2014): I need to check with Liao to understand this part of the code better.
  // I think that the default value for Outliner::use_dlopen is false, so that when the 
  // Java support is used the true branch is taken.  However, if might be the we need
  // to support the outliner using the code below and so this would be a bug for the 
  // outliner.
     if (!Outliner::use_dlopen)
        {
#if 0
          printf ("In SageBuilder::buildFile(): (after test for (!Outliner::use_dlopen) == true: project = %p project->get_fileList_ptr()->get_listOfFiles().size() = %" PRIuPTR " \n",project,project->get_fileList_ptr()->get_listOfFiles().size());
#endif
       // DQ (3/5/2014): If we added the file above, then don't add it here since it is redundant.
          project->set_file(*result);  // equal to push_back()
#if 0
          printf ("In SageBuilder::buildFile(): (after 2nd project->set_file()): project = %p project->get_fileList_ptr()->get_listOfFiles().size() = %" PRIuPTR " \n",project,project->get_fileList_ptr()->get_listOfFiles().size());
#endif
        }
       else
        {
#if 0
          printf ("In SageBuilder::buildFile(): (after test for (!Outliner::use_dlopen) == false: project = %p project->get_fileList_ptr()->get_listOfFiles().size() = %" PRIuPTR " \n",project,project->get_fileList_ptr()->get_listOfFiles().size());
#endif
       // Liao, 5/1/2009, 
       // if the original command line is: gcc -c -o my.o my.c and we want to  
       // add a new file(mynew.c), the command line for the new file would become "gcc -c -o my.o mynew.c "
       // which overwrites the object file my.o from my.c and causes linking error.
       // To avoid this problem, I insert the file at the beginning and let the right object file to be the last generated one
       //
       // TODO This is not an elegant fix and it causes some strange assertion failure in addAssociatedNodes(): default case node 
       // So we only turn this on if Outliner:: use_dlopen is used for now
       // The semantics of adding a new source file can cause changes to linking phase (new object files etc.)
       // But ROSE has a long-time bug in handling combined compiling and linking command like "translator -o a.out a.c b.c"
       // It will generated two command line: "translator -o a.out a.c" and "translator -o a.out b.c", which are totally wrong.
       // This problem is very relevant to the bug.
          SgFilePtrList& flist = project->get_fileList();
          flist.insert(flist.begin(),result);

#if 0
          printf ("In SageBuilder::buildFile(): (after flist.insert(flist.begin(),result)): project = %p project->get_fileList_ptr()->get_listOfFiles().size() = %" PRIuPTR " \n",project,project->get_fileList_ptr()->get_listOfFiles().size());
#endif
        }
#endif

#if 0
     printf ("In SageBuilder::buildFile(): (after project->set_file()): project = %p project->get_fileList_ptr()->get_listOfFiles().size() = %" PRIuPTR " \n",project,project->get_fileList_ptr()->get_listOfFiles().size());
#endif

  // DQ (3/6/2014): For Java, this function can only be called AFTER the SgFile has been added to the file list in the SgProject.
  // For C/C++ it does not appear to matter if the call is made before the SgFile has been added to the file list in the SgProject.
  // DQ (6/14/2013): Since we seperated the construction of the SgFile IR nodes from the invocation of the frontend, we have to call the frontend explicitly.
     result->runFrontend(nextErrorCode);

	if(Outliner::enable_debug) cout << "[OutlinerUIUC][buildFile] uow 5." << endl;
#if 0
     printf ("In SageBuilder::buildFile(): (after result->runFrontend()): project = %p project->get_fileList_ptr()->get_listOfFiles().size() = %" PRIuPTR " \n",project,project->get_fileList_ptr()->get_listOfFiles().size());
#endif

#if 0
     result->display("SageBuilder::buildFile()");
#endif

     ROSE_ASSERT(project != NULL);
     project->set_frontendErrorCode(max(project->get_frontendErrorCode(), nextErrorCode));

	if(Outliner::enable_debug) cout << "[OutlinerUIUC][buildFile] uow 6." << endl;
  // Not sure why a warning shows up from astPostProcessing.C
  // SgNode::get_globalMangledNameMap().size() != 0 size = %" PRIuPTR " (clearing mangled name cache)
     if (result->get_globalMangledNameMap().size() != 0)
        {
          result->clearGlobalMangledNameMap();
        }

     if(Outliner::enable_debug) cout << "[OutlinerUIUC][buildFile] Finishing." << endl;
     return result;
#else
     return NULL;
#endif
   }


SgSourceFile* 
Outliner::UIUC::generateNewSourceFile(SgBasicBlock* s, const string& file_name)
{
    if(Outliner::enable_debug) {
	cout << "[OutlinerUIUC][generateNewSourceFile] Starting." << endl;
    }

    SgSourceFile* new_file = NULL;
    SgProject * project = SageInterface::getEnclosingNode<SgProject> (s);
    ROSE_ASSERT(project != NULL);
    // s could be transformation generated, so use the root SgFile for file name
    SgFile* cur_file = SageInterface::getEnclosingNode<SgFile> (s);
    ROSE_ASSERT (cur_file != NULL);
    //grab the file suffix, 
    // TODO another way is to generate suffix according to source language type
    std::string orig_file_name = cur_file->get_file_info()->get_filenameString();
    if(Outliner::enable_debug) cout << "[OutlinerUIUC][generateNewSourceFile] orig_file_name="<<orig_file_name <<" ." << endl;
    //cout<<"debug:orig_file_name="<<orig_file_name<<endl;
    std::string file_suffix = StringUtility::fileNameSuffix(orig_file_name);
    ROSE_ASSERT(file_suffix !="");
    std::string new_file_name = file_name+"."+file_suffix;
    if (!output_path.empty())
    { // save the outlined function into a specified path
	new_file_name = StringUtility::stripPathFromFileName(new_file_name);
	new_file_name= output_path+"/"+new_file_name;
    }
    if(Outliner::enable_debug) cout << "[OutlinerUIUC][generateNewSourceFile] new_file_name="<<new_file_name <<" ." << endl;

    // remove pre-existing file with the same name
    remove (new_file_name.c_str());

    SgFile * tmp_file = UIUC::buildFile(new_file_name, new_file_name,project);
    if(Outliner::enable_debug) cout << "[OutlinerUIUC][generateNewSourceFile] build file done." << endl;
    new_file = isSgSourceFile(tmp_file);
    if(Outliner::enable_debug) cout << "[OutlinerUIUC][generateNewSourceFile] isSgSourceFile done." << endl;
    //new_file = isSgSourceFile(buildFile(new_file_name, new_file_name,project));
    //new_file = isSgSourceFile(buildFile(new_file_name, new_file_name));
    ROSE_ASSERT(new_file != NULL);
    if(Outliner::enable_debug) {
	cout << "[OutlinerUIUC][generateNewSourceFile] Finished." << endl;
    }
    return new_file;
}

/*Copy From SageInterface*/
static SgExpression* SkipCasting (SgExpression* exp)
{
  SgCastExp* cast_exp = isSgCastExp(exp);
   if (cast_exp != NULL)
   {
      SgExpression* operand = cast_exp->get_operand();
      assert(operand != 0);
      return SkipCasting(operand);
   }
  else
    return exp;
}

/**
 * Major work of outlining is done here
 *  Preparations: variable collection
 *  Generate outlined function
 *  Replace outlining target with a function call
 *  Append dependent declarations,headers to new file if needed
 */
Outliner::Result
Outliner::UIUC::outlineBlock (SgBasicBlock* s, const string& func_name_str)
{
  cout << "[OutlinerUIUC][outlineBlock] starting..." << endl;

  //---------step 1. Preparations-----------------------------------
  //new file, cut preprocessing information, collect variables
  // Generate a new source file for the outlined function, if requested
  SgSourceFile* new_file = NULL;
  if (Outliner::useNewFile) {
    new_file = UIUC::generateNewSourceFile(s,func_name_str);
    if(Outliner::enable_debug) {
      cout << "[OutlinerUIUC][outlineBlock] generated new source file." << endl;
    }
  }

  // Save some preprocessing information for later restoration. 
  AttachedPreprocessingInfoType ppi_before, ppi_after;
  ASTtools::cutPreprocInfo (s, PreprocessingInfo::before, ppi_before);
  ASTtools::cutPreprocInfo (s, PreprocessingInfo::after, ppi_after);

  // Determine variables to be passed to outlined routine.
  // ----------------------------------------------------------
  // Also collect symbols which must use pointer dereferencing if replaced during outlining
  ASTtools::VarSymSet_t syms, pdSyms;
  collectVars (s, syms);

  // prepare necessary analysis to optimize the outlining 
  //-----------------------------------------------------------------
  std::set<SgInitializedName*> readOnlyVars;
  std::set< SgInitializedName *> liveIns, liveOuts;
  // Collect read-only variables of the outlining target

  if (Outliner::temp_variable)
  {
    SageInterface::collectReadOnlyVariables(s,readOnlyVars);
    // Collect use by address plus non-assignable variables
    // They must be passed by reference if they need to be passed as parameters
    // TODO: this is not accurate: array variables are not assignable , but they should not using pointer dereferencing 
    ASTtools::collectPointerDereferencingVarSyms(s,pdSyms);

    // liveness analysis
    SgStatement* firstStmt = (s->get_statements())[0];
    if (isSgForStatement(firstStmt)&& enable_liveness)
    {
      LivenessAnalysis * liv = SageInterface::call_liveness_analysis (SageInterface::getProject());
      SageInterface::getLiveVariables(liv, isSgForStatement(firstStmt), liveIns, liveOuts);
    }

    if (Outliner::enable_debug)
    {
      cout<<"Outliner::Transform::generateFunction() -----Found "<<readOnlyVars.size()<<" read only variables..:";
      for (std::set<SgInitializedName*>::const_iterator iter = readOnlyVars.begin();
          iter!=readOnlyVars.end(); iter++)
        cout<<" "<<(*iter)->get_name().getString()<<" ";
      cout<<endl;
      cout<<"Outliner::Transform::generateFunction() -----Found "<<liveOuts.size()<<" live out variables..:";
      for (std::set<SgInitializedName*>::const_iterator iter = liveOuts.begin();
          iter!=liveOuts.end(); iter++)
        cout<<" "<<(*iter)->get_name().getString()<<" ";
      cout<<endl; 
    }
  }

  // Insert outlined function.
  // grab target scope first
  SgGlobal* glob_scope = const_cast<SgGlobal *> (TransformationSupport::getGlobalScope (s));

  if (Outliner::useNewFile)  // change scope to the one within the new source file
  {
    glob_scope = new_file->get_globalScope();
  }

  // generate the function and its prototypes if necessary
   if(Outliner::enable_debug) cout << "In Outliner::Transform::outlineBlock() function name to build: func_name_str = " <<func_name_str.c_str()<< endl;
   
  std::set<SgInitializedName*> restoreVars;

  /**
   *
   */
  calculateVariableRestorationSet (syms, readOnlyVars,liveOuts,restoreVars);

  if(Outliner::enable_debug) {
	cout << "syms ";
	dumpVars(syms);
	cout << "pdSyms ";
	dumpVars(pdSyms);	
	cout << "liveIns ";
	dumpInitdNames(liveIns);
	cout << "liveOuts ";
	dumpInitdNames(liveOuts);
	cout << "restoreVars ";
	dumpInitdNames(restoreVars);
  }

  SgFunctionDeclaration* func = generateFunction (s, func_name_str, syms, pdSyms, restoreVars, NULL, glob_scope);
  ROSE_ASSERT (func != NULL);
  ROSE_ASSERT(glob_scope->lookup_function_symbol(func->get_name()));

  // DQ (2/26/2009): At this point "s" has been reduced to an empty block.
  ROSE_ASSERT(s->get_statements().empty() == true);

  // Retest this...
  ROSE_ASSERT(func->get_definition()->get_body()->get_parent() == func->get_definition());

  //-----------Step 3. Insert the outlined function -------------
  // DQ (2/16/2009): Added (with Liao) the target block which the outlined function will replace.
  // Insert the function and its prototype as necessary  
  ROSE_ASSERT(glob_scope->lookup_function_symbol(func->get_name()));
  insert (func, glob_scope, s); //Outliner::insert() 
  ROSE_ASSERT(glob_scope->lookup_function_symbol(func->get_name()));
  //
  // Retest this...
  ROSE_ASSERT(func->get_definition()->get_body()->get_parent() == func->get_definition());

//-----------Step 4. Replace the outlining target with a function call-------------

  // Generate a call to the outlined function.
  SgScopeStatement * p_scope = s->get_scope();
  ROSE_ASSERT(p_scope);

  SgStatement *func_call = NULL;
  func_call = generateCall (func, syms, readOnlyVars, "", p_scope);

  ROSE_ASSERT (func_call != NULL);

  if(Outliner::enable_debug) cout << "In Outliner::Transform::outlineBlock() generateCall done" << endl;
  // Retest this...
  ROSE_ASSERT(func->get_definition()->get_body()->get_parent() == func->get_definition());

  // What is this doing (what happens to "s")
  //  cout<<"Debug before replacement(s, func_call), s is\n "<< s<<endl; 
  //     SageInterface::insertStatementAfter(s,func_call);
  SageInterface::replaceStatement(s,func_call);

  ROSE_ASSERT(s != NULL);
  ROSE_ASSERT(s->get_statements().empty() == true);

  if(Outliner::enable_debug) cout << "In Outliner::Transform::outlineBlock() replaceStatement done" << endl;

  // Retest this...
  ROSE_ASSERT(func->get_definition()->get_body()->get_parent() == func->get_definition());

  // Restore preprocessing information.
  ASTtools::moveInsidePreprocInfo (s, func->get_definition ()->get_body ());
  ASTtools::pastePreprocInfoFront (ppi_before, func_call);
  ASTtools::pastePreprocInfoBack  (ppi_after, func_call);

  SageInterface::fixVariableReferences(p_scope);

  if(Outliner::enable_debug) cout << "In Outliner::Transform::outlineBlock() fixVariableReferences done" << endl;

  //-----------handle dependent declarations, headers if new file is generated-------------
  if (new_file)
  {
    SageInterface::fixVariableReferences(new_file);
    // SgProject * project2= new_file->get_project();
    // AstTests::runAllTests(project2);// turn it off for now
    // project2->unparse();
  }

  // Retest this...
  ROSE_ASSERT(func->get_definition()->get_body()->get_parent() == func->get_definition());
#if 0
  printf ("After resetting the parent: func->get_definition() = %p func->get_definition()->get_body()->get_parent() = %p \n",func->get_definition(),func->get_definition()->get_body()->get_parent());
#endif

  ROSE_ASSERT(s != NULL);
  ROSE_ASSERT(s->get_statements().empty() == true);

  if (useNewFile == true && ! SageInterface::is_Fortran_language())
  {
    // DQ (2/6/2009): I need to write this function to support the
    // insertion of the function into the specified scope.  If the
    // file associated with the scope is marked as compiler generated 
    // (or as a transformation) then the declarations referenced in the 
    // function must be copied as well (those not in include files)
    // and the include files must be copies also. If the SgFile
    // is not compiler generated (or a transformation) then we just
    // append the function to the scope (trivial case).

    // I am passing in the target_func so that I can get the location 
    // in the file from which we want to generate a matching context.
    // It would be better if this were the location of the new function call
    // to the outlined function (since dependent declaration in the function
    // containing the outlined code (loop nest, for example) might contain
    // relevant typedefs which have to be created in the new file (or the 
    // outlined function as a special case).

#if 1
    ROSE_ASSERT(func->get_firstNondefiningDeclaration() != NULL);
    ROSE_ASSERT(TransformationSupport::getSourceFile(func) == TransformationSupport::getSourceFile(func->get_firstNondefiningDeclaration()));
    ROSE_ASSERT(TransformationSupport::getSourceFile(func->get_scope()) == TransformationSupport::getSourceFile(func->get_firstNondefiningDeclaration()));
#if 0
    printf ("******************************************************************** \n");
    printf ("Now calling SageInterface::appendStatementWithDependentDeclaration() \n");
    printf ("******************************************************************** \n");
#endif
    // If the outline function will be placed into it's own file then we need to reconstruct any dependent statements (and #include CPP directives).
    SgFunctionDeclaration* func_orig = const_cast<SgFunctionDeclaration *> (SageInterface::getEnclosingFunctionDeclaration (s));

    SageInterface::appendStatementWithDependentDeclaration(func,glob_scope,func_orig,exclude_headers);
  
    if(Outliner::enable_debug) cout << "In Outliner::Transform::outlineBlock() appendStatementWithDependentDeclaration done" << endl;

    // printf ("DONE: Now calling SageInterface::appendStatementWithDependentDeclaration() \n");
#else
    printf ("Skipping call to SageInterface::appendStatementWithDependentDeclaration() (testing only)\n");
#endif
  }

  cout << "[OutlinerUIUC][outlineBlock] ending..." << endl;
  return Result (func, func_call, new_file);

}



/* =====================================================================
 *  Main routine to outline a single statement immediately following
 *  an outline directive (pragma).
 */

Outliner::Result
Outliner::UIUC::outline (SgPragmaDeclaration* decl)
{
	cout << "[OutlinerUIUC] outline (SgPragmaDeclaration)" << endl;
	SgStatement* s = processPragma (decl);
	if (!s)
		return Result ();
	//cout<<"Debug: OutlinerUIUC::outline(), outlining target is:"<<s<<endl;
	// Generate outlined function, removing 's' from the tree.
	string name;
	if(newFileName.size() == 0) {
		name = generateFuncName (s);
	} else {
		stringstream ss;
		ss << newFileIndex;
		name = Outliner::UIUC::newFileName + "_" + ss.str();+
		newFileIndex++;
	}

//	if(Outliner::enable_debug) 
	cout << "[Outliner:UIUC:outline] file name used " << name << endl; 

	Result result = Outliner::UIUC::outline (s, name);
	if (!preproc_only_)
	{
		ROSE_ASSERT (result.isValid());

		// DQ (2/26/2009): If this was sucessful, then delete the input block? (this may be a problem for moreTest1.c).
		// SageInterface::deleteAST(s);

		// Remove pragma
		ASTtools::moveBeforePreprocInfo (decl, result.call_);
		ASTtools::moveAfterPreprocInfo (decl, result.call_);

#if 1
		//This will search the parent for the location of decl, but this is not found
#ifndef _MSC_VER
		LowLevelRewrite::remove (decl);
#else
		ROSE_ASSERT(false);
#endif
#else
		// DQ (2/24/2009): Added more direct concept of remove.
		// We just want a more direct and simpler concept of remove (remove the AST, 
		//because potential dangling pointers have been taken care of).
		SageInterface::deleteAST(decl);
#endif
	}
	return result;
}

size_t Outliner::UIUC::outlineAll(SgProject * project) 
{
	cout << "[OutlinerUIUC] UOW it's gonna work" << endl;

	size_t num_outlined = 0;
	TargetList_t targets;
	//generic abstract handle based target selection
	if (Outliner::handles.size()>0)
	{
		cout<< "[OutlinerUIUC] handles are not accepted anymore only pragmas." << endl;
	} //TODO do we want to have non-exclusive abstract handle options?
	else  if (SageInterface::is_Fortran_language ()) 
	{ // Search for the special source comments for Fortran input
		if (collectFortranTarget(project, targets))
		{
			for (TargetList_t::iterator i = targets.begin ();
					i != targets.end (); ++i)
				if (outline(*i).isValid())
					++num_outlined;
		}
	} 
	else // Search for pragmas for C/C++
	{
		PragmaList_t pragmas;
		if (collectPragmas (project, pragmas))
		{
			for (PragmaList_t::iterator i = pragmas.begin (); i != pragmas.end (); ++i)
			{
				if (outline (*i).isValid ())
				{
					++num_outlined;
				}
			}

			// DQ (2/24/2009): Now remove the pragma from the original source code.
			// Any dangling pointers have already been taken care of.
			// Liao (4/14/2009):delete only if it is not preprocessing only
			if (!preproc_only_) 
			{
				for (PragmaList_t::iterator i = pragmas.begin (); i != pragmas.end (); ++i)
				{
					SageInterface::deleteAST(*i);
				}
			}
		}
	}
	return num_outlined;
}


void Outliner::UIUC::commandLineProcessing(std::vector<std::string> &argvList)
{
  if (CommandlineProcessing::isOption (argvList,"-rose:outline:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for outlined functions..."<<endl;
    enable_debug= true;
  }
 
  if (CommandlineProcessing::isOption (argvList,"-rose:outline:","temp_variable",true))
  {
    if (enable_debug)
      cout<<"Enabling using temp variables to reduce pointer dereferencing for outlined functions..."<<endl;
    temp_variable = true;
  }

  if (CommandlineProcessing::isOption (argvList,"-rose:outline:","enable_liveness",true))
  {
  	if (temp_variable)
  	{
      		enable_liveness = true;
	    	if (enable_debug)
      			cout<<"Enabling using liveness analysis to reduce restoring statements..."<<endl;
  	} else {
		cerr << "WARNING! In order to enable liveness is necessary to enable temp_variable first." << endl;
	}
  }

  Outliner::UIUC::newFileIndex = 0; 
  if (CommandlineProcessing::isOptionWithParameter(argvList,"-rose:outline:","newFileName", newFileName, true)) {
	cout <<  "New file generated with name " << newFileName << endl;	
  }

  useNewFile= true;
  if (enable_debug) cout<<"Enabling new source file for outlined functions..."<<endl;

  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC Outliner-specific options"<<endl;
    cout<<"Usage: outline [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
    cout<<"\t-rose:outline:temp_variable                    use temp variables to reduce pointer dereferencing for the variables to be passed"<<endl;
    cout<<"\t-rose:outline:enable_liveness                  use liveness analysis to reduce restoring statements if temp_variable is turned on"<<endl;
  //  cout<<"\t-rose:outline:new_file                         use a new source file for the generated outlined function"<<endl;
    cout<<"\t-rose:outline:enable_debug                     run outliner in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }

}
