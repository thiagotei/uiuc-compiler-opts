

#ifndef ROSE_OUTLINER_UIUC
#define ROSE_OUTLINER_UIUC

#include <cstdlib> 
#include <sage3basic.h>
#include <Outliner.hh>
#include <boost/algorithm/string/trim.hpp>
#include <ASTtools.hh>
#include <Cxx_Grammar.h>
#include <PreprocessingInfo.hh>
//#include <TransformUIUC.hh>
#include <sageBuilder.h>
#include <OmpAttribute.h>
#include <TraversalUIUC.hh>

using namespace SageBuilder;

class SgProject;
class SgFunctionDeclaration;
class SgStatement;
class SgPragmaDeclaration;


namespace Outliner
{

	namespace UIUC
	{
		std::string newFileName;
		int newFileIndex;
		size_t	outlineAll(SgProject * project);
		Outliner::Result outline(SgPragmaDeclaration* decl);	
		Outliner::Result outline(SgStatement* s);		
		Outliner::Result outline(SgStatement* s, const std::string& func_name);		
		Outliner::Result outlineBlock(SgBasicBlock* s, const std::string& func_name_str);
		Outliner::Result OLDoutlineBlock(SgBasicBlock* s, const std::string& func_name_str);
		SgSourceFile*  	 generateNewSourceFile(SgBasicBlock* s, const std::string& file_name); 
		SgFile*	buildFile(const std::string& inputFileName, const std::string& outputFileName, SgProject* project);
		void commandLineProcessing(std::vector<std::string> &argvList);
		

	}
	

}


#endif
