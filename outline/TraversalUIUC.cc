#include <TraversalUIUC.hh>

visitorTraversal::visitorTraversal(){
}

void visitorTraversal::visit(SgNode *n){
	cout << "Visiting " << n->class_name() << " " << SageInterface::get_name(n) << " " <<(void *)n <<" ." << endl;
}


void visitorTraversal::atTraversalEnd(){
	printf("Traversal ends here!\n");
}
