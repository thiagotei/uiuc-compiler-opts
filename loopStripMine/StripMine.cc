#include "StripMine.hh"


int main(int argc, char ** argv)
{
	vector<string> argvList(argv, argv+argc);
	ICE::StripMineUIUC::commandLineProcessing(argvList);	
	
	SgProject * proj = frontend(argvList);
	ROSE_ASSERT(proj);

	cout << "[ICE/StripMineUIUC]..." << endl;
	size_t count = ICE::StripMineUIUC::stripmineAll(proj);

	//TODO specify the output name.
	proj->unparse();
	cout << "[ICE/StripMineUIUC] end" << endl;

	
	
	return 0;
}




ICE::Result ICE::StripMineUIUC::stripmine(SgPragmaDeclaration* prag) 
{
//	SI::dumpInfo(s);
	vector<int> pragmainfo;
	SgStatement* s = AnnotParser::processPragmaInfo (prag, PRAGMA_NAME, pragmainfo);
	if(!s) return Result(false);	

	// Expects two infos to do the stripmine.
	if(pragmainfo.size() != 2) {
     		cerr<<"Error in loopStripMine, not proper info on the pragma."<<endl;
		return Result(false);
	}

	if(enable_debug) {
		cout << "pragma info: ";
		for(vector<int>::iterator it = pragmainfo.begin(); it != pragmainfo.end(); ++it){
			cout << *it << " ";
		}	
		cout << endl;
	}
	
	return stripmine(s, pragmainfo[0], pragmainfo[1]);
}

ICE::Result ICE::StripMineUIUC::stripmineFortran(SgStatement * s, vector<int>& pragmainfo) {

	if(pragmainfo.size() != 2) {
     		cerr<<"Error in loopStripMine, not proper info on the pragma."<<endl;
		return Result(false);
	}

	return stripmine(s, pragmainfo[0], pragmainfo[1]);
}


ICE::Result ICE::StripMineUIUC::stripmine(SgStatement* s, size_t targetLevel, size_t tileSize) 
{
	bool r;
//	SI::dumpInfo(s);

	if (SgForStatement * for_loop = isSgForStatement(s)) {
		cout << "[ICE/StripMineUIUC/stripmine2] For loop" << endl;
		loopStripMine(for_loop, targetLevel, tileSize);		
		r = true;
	} else if (SgFortranDo * do_loop = isSgFortranDo(s)) {
		cout << "[ICE/StripMineUIUC/stripmine2] Do loop" << endl;
		loopStripMine(do_loop, targetLevel, tileSize);
		r = true;
	} else {
		cerr << "[ICE/StripMineUIUC/stripmine2] ERROR! SgStatement not a For neither a Do loop!" << endl;	
		r = false;
	}
	
	return Result(r);
}

size_t ICE::StripMineUIUC::stripmineAll(SgProject * project) 
{

	size_t num_tiled = 0;	
	TargetList_t targets;
        std::vector< std::vector<int> > pragmasinfo;
        //Rose_STL_Container< std::vector<int> > pragmasinfo;
	if (SI::is_Fortran_language()) 
	{
		if (AnnotParser::collectFortranTarget(project, targets, PRAGMA_NAME, pragmasinfo))
		{
			std::vector< std::vector<int> >::iterator j = pragmasinfo.begin();
			std::vector< std::vector<int> >::iterator j_end = pragmasinfo.end();
			//Rose_STL_Container< std::vector<int> >::iterator j = pragmasinfo.begin();
			//Rose_STL_Container< std::vector<int> >::iterator j_end = pragmasinfo.end();
			for(TargetList_t::iterator i = targets.begin(); 
                            i != targets.end() && j != j_end; ++i, ++j) 
				if(stripmineFortran(*i, *j).isValid())
					++num_tiled;
		}

	} else // Search for pragmas for C/C++
	{ 
		PragmaList_t pragmas;
		if(AnnotParser::collectPragmas(project, pragmas, PRAGMA_NAME))
		{
			for (PragmaList_t::iterator i = pragmas.begin(); i != pragmas.end(); ++i)
				if(stripmine(*i).isValid())
					++num_tiled;

		
		}
	}		

	return num_tiled;
}

bool ICE::StripMineUIUC::loopStripMine(SgForStatement* loopNest, size_t targetLevel, size_t tileSize)
{
	return SIUI::loopBlocking(loopNest, targetLevel, tileSize, targetLevel -1);
}


//! Tile the n-level (starting from 1) of a perfectly nested loop nest using stripmine size s
/* Translation
 Before:
  for (i = 0; i < 100; i++)
    for (j = 0; j < 100; j++)
      for (k = 0; k < 100; k++)
        c[i][j]= c[i][j]+a[i][k]*b[k][j];

  After stripmine i loop nest's level 3 (k-loop) with size 5, it becomes

// added a new controlling loop at the previous level
  int _lt_var_k;
    for (i = 0; i < 100; i++)
      for (j = 0; j < 100; j++)
       for (_lt_var_k = 0; _lt_var_k <= 99; _lt_var_k += 1 * 5) {
        // rewritten loop header , normalized also
        for (k = _lt_var_k; k <= (99 < (_lt_var_k + 5 - 1))?99 : (_lt_var_k + 5 - 1); k += 1) {
          c[i][j] = c[i][j] + a[i][k] * b[k][j];
        }
  }
// finally run constant folding

 */

bool ICE::StripMineUIUC::loopStripMine(SgFortranDo* loopNest, size_t targetLevel, size_t tileSize)
{
	return SIUI::loopBlocking(loopNest, targetLevel, tileSize, targetLevel - 1);
}


void ICE::StripMineUIUC::commandLineProcessing(std::vector<std::string> &argvList) {

  if (CommandlineProcessing::isOption (argvList,"-rose:stripmine:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for loopStripMine functions..."<<endl;
    enable_debug= true;
  }

/*  if (CommandlineProcessing::isOptionWithParameter(argvList,"-rose:stripmine:","target_level", target_level, true))
  {	
    cout<<"Target level is " << target_level << endl;	
  }

  if (CommandlineProcessing::isOptionWithParameter (argvList,"-rose:stripmine:","tile_size", tile_size, true))
  {	
    cout<<"Tile size is " << tile_size << endl;	
  }
*/
  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC StripMine-specific options"<<endl;
    cout<<"Usage: stripmine [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
  //  cout<<"\t-rose:stripmine:target_level                   Target loop n-level starting from 1."<<endl;
  //  cout<<"\t-rose:stripmine:tile_size                      Tile Size of the targeted loop level."<<endl;
    cout<<"\t-rose:stripmine:enable_debug                   run stripmine in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }
}

