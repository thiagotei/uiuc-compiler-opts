#ifndef STRIPMINE_UIUC
#define STRIPMINE_UIUC

#include <cstdlib>
#include <sage3basic.h>
#include <Cxx_Grammar.h>
#include <sageBuilder.h>
#include <Rose/StringUtility.h>
#include <string>
#include <vector>
#include <AnnotParser.hh>
#include <Result.hh>
#include <SageInterfaceUIUC.hh>

using namespace std;

namespace SB = SageBuilder;
namespace SI = SageInterface;
namespace SIUI = ICE::SageInterfaceUIUC;

namespace ICE
{
	namespace StripMineUIUC
	{

		bool enable_debug = false;
		static const std::string PRAGMA_NAME("uiuc_stripmine");
 		void commandLineProcessing(std::vector<std::string> &argvList); 
 		size_t stripmineAll(SgProject * project);
		Result stripmine(SgPragmaDeclaration* prag);
		Result stripmineFortran(SgStatement * s, std::vector<int>& pragmainfo);
		Result stripmine(SgStatement * s, size_t targetLevel, size_t tileSize);
		bool loopStripMine(SgFortranDo* loopNest, size_t targetLevel, size_t tileSize);
		bool loopStripMine(SgForStatement* loopNest, size_t targetLevel, size_t tileSize);

	}
}
#endif
