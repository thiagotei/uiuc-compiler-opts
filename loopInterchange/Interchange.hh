
#ifndef INTERCHANGE_UIUC
#define INTERCHANGE_UIUC

#include <cstdlib>
#include <sage3basic.h>
#include <Cxx_Grammar.h>
#include <sageBuilder.h>
#include <string>
#include <vector>
#include <AnnotParser.hh>
#include <Result.hh>
#include <Misc.hh>

using namespace std;

namespace SI = SageInterface;

namespace ICE
{
	namespace InterchangeUIUC
	{
		bool enable_debug = false;
		static const std::string PRAGMA_NAME("uiuc_interchange");
		//Delimiter to the new order string in the annotation  
		static const std::string PRAGMA_DELIMITER(",");

 		void commandLineProcessing(std::vector<std::string> &argvList); 
		Result interchange(SgPragmaDeclaration* prag);
		//Result interchange(SgStatement * s);
		template <typename T> Result interchange(SgStatement* s,  size_t depth, T neworder);
		Result interchangeFortran(SgStatement * s, vector<std::string>& pragmainfo);
		size_t interchangeAll(SgProject * project);
		bool loopInterchange(SgFortranDo* loop, size_t depth, size_t lexicoOrder);
		bool loopInterchange(SgForStatement* loop, size_t depth, size_t lexicoOrder);
		bool loopInterchange(SgFortranDo* loop, size_t depth, const std::vector<size_t> &changedOrder);
		bool loopInterchange(SgForStatement* loop, size_t depth, const std::vector<size_t> &changedOrder);

		template <typename N> int checkNewOrder(std::vector<N> neworder); 
	}
}	

#endif
