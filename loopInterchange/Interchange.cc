#include "Interchange.hh"


int main(int argc, char ** argv)
{
	vector<string> argvList(argv, argv+argc);
	ICE::InterchangeUIUC::commandLineProcessing(argvList);	
	
	SgProject * proj = frontend(argvList);
	ROSE_ASSERT(proj);

	cout << "[ICE/InterchangeUIUC]..." << endl;
	size_t count = ICE::InterchangeUIUC::interchangeAll(proj);

	//TODO specify the output name.
	proj->unparse();
	cout << "[ICE/InterchangeUIUC] end" << endl;

	return 0;
}

size_t ICE::InterchangeUIUC::interchangeAll(SgProject * project) 
{
	size_t num_tiled = 0;	
	TargetList_t targets;
	std::vector< std::vector<string> > pragmasinfo;
	if (SageInterface::is_Fortran_language()) 
	{
		if (AnnotParser::collectFortranTarget(project, targets, PRAGMA_NAME, pragmasinfo))
		{
			std::vector< std::vector<string> >::iterator j = pragmasinfo.begin();
                        std::vector< std::vector<string> >::iterator j_end = pragmasinfo.end();	
			for(TargetList_t::iterator i = targets.begin();  
				i != targets.end() && j != j_end; ++i, ++j)
				if(interchangeFortran(*i, *j).isValid())
					++num_tiled;
		}

	} else // Search for pragmas for C/C++
	{ 
		PragmaList_t pragmas;
		if(AnnotParser::collectPragmas(project, pragmas, PRAGMA_NAME))
		{
			for (PragmaList_t::iterator i = pragmas.begin(); i != pragmas.end(); ++i)
				if(interchange(*i).isValid())
					++num_tiled;

		}
	}		
	return num_tiled;
}

ICE::Result ICE::InterchangeUIUC::interchange(SgPragmaDeclaration* prag) 
{
//	SageInterface::dumpInfo(s);
	vector<string> pragmainfo;
	SgStatement* s = AnnotParser::processPragmaInfo (prag, PRAGMA_NAME, pragmainfo);
	if(!s) return Result(false);	

	// Expects two infos to do the tiling.
	if(pragmainfo.size() != 1) {
     		cerr<<"Error in loopInterchange, not proper info on the pragma."<<endl;
		return Result(false);
	}

	if(enable_debug) {
		cout << "pragma info: ";
		for(vector<string>::iterator it = pragmainfo.begin(); it != pragmainfo.end(); ++it){
			cout << *it << " ";
		}	
		cout << endl;
	}

	std::vector<size_t> neworder = parseStringToNum<size_t>(pragmainfo[0], PRAGMA_DELIMITER);

	return interchange(s, neworder.size(), neworder);
}

ICE::Result ICE::InterchangeUIUC::interchangeFortran(SgStatement * s, vector<string>& pragmainfo) 
{

        if(pragmainfo.size() != 1) {
                cerr<<"Error in loopInterchange, not proper info on the pragma."<<endl;
                return Result(false);
        }

	std::vector<size_t> neworder = parseStringToNum<size_t>(pragmainfo[0], PRAGMA_DELIMITER);
	if(enable_debug)
	{
		cout << "[ICE/InterchangeUIUC/interchangeFortran] neworder: ";
		for(std::vector<size_t>::iterator it= neworder.begin(); it != neworder.end(); ++it) 
		{
			std::cout << *it << " "; 
		}		
		std::cout << endl;
	}

        return interchange(s, neworder.size(), neworder);
}


template <typename T>
ICE::Result ICE::InterchangeUIUC::interchange(SgStatement* s,  size_t depth, T neworder) 
{
	bool r;
//	SageInterface::dumpInfo(s);

	if (SgForStatement * for_loop = isSgForStatement(s)) {
		cout << "[ICE/InterchangeUIUC/interchange] For loop" << endl;
		//SageInterface::loopInterchange(for_loop, depth, neworder);		
		r = loopInterchange(for_loop, depth, neworder);		
	} else if (SgFortranDo * do_loop = isSgFortranDo(s)) {
		cout << "[ICE/InterchangeUIUC/interchange] Do loop" << endl;
		r = loopInterchange(do_loop, depth, neworder);
	} else {
		cerr << "[ICE/InterchangeUIUC/interchange] ERROR! SgStatement not a For neither a Do loop!" << endl;	
		r = false;
	}
	
	return Result(r);
}

bool ICE::InterchangeUIUC::loopInterchange(SgFortranDo* loop, size_t depth, size_t lexicoOrder) 
{
  if (lexicoOrder == 0) // allow 0 to mean no interchange at all
    return true;
  ROSE_ASSERT(lexicoOrder<myfactorial(depth));

  // convert the lexicographical number to a permutation order array permutation[depth]
  std::vector<size_t> changedOrder = getPermutationOrder (depth, lexicoOrder);
  if (enable_debug) {
	cout << "[loopInterchange] new order for "<<lexicoOrder <<" : ";
        for (std::vector<size_t> ::iterator i = changedOrder.begin(); i!= changedOrder.end(); i++)
	{
		cout << *i << " ";		
	}
	cout << endl;
  }

  return loopInterchange(loop, depth, changedOrder); 
}

bool ICE::InterchangeUIUC::loopInterchange(SgFortranDo* loop, size_t depth, const std::vector<size_t> &changedOrder) 
{
  // parameter verification
  ROSE_ASSERT(loop != NULL);
  //must have at least two levels
  ROSE_ASSERT (depth >1);
  ROSE_ASSERT (changedOrder.size()>1);

  // Check the neworder sequence 
  if(checkNewOrder(changedOrder))
  {
	  std::cerr << "ERROR!! The new order provided is incorrect." << std::endl;
	  return false;
  }

  //TODO need to verify the input loop has n perfectly-nested children loops inside
  // save the loop nest's headers: init, test, and increment  
  std::vector<SgFortranDo* > loopNest = SageInterface::querySubTree<SgFortranDo>(loop,V_SgFortranDo);
  ROSE_ASSERT(loopNest.size()>=depth);
  std::vector<std::vector<SgNode*> > loopHeads;
  for (std::vector<SgFortranDo* > ::iterator i = loopNest.begin(); i!= loopNest.end(); i++)
  {
    SgFortranDo* cur_loop = *i;
    std::vector<SgNode*> head;
    head.push_back(cur_loop->get_initialization());
    head.push_back(cur_loop->get_bound());
    head.push_back(cur_loop->get_increment());
    loopHeads.push_back(head);
  }

  // rewrite the loop nest to reflect the permutation
  // set the header to the new header based on the permutation array
  for (size_t i=0; i<depth; i++)
  {
    // only rewrite if necessary
    if (i != changedOrder[i])
    {
      SgFortranDo* cur_loop = loopNest[i];
      std::vector<SgNode*> newhead = loopHeads[changedOrder[i]];

      SgExpression* init = isSgExpression(newhead[0]);
      //ROSE_ASSERT(init != NULL) // could be NULL?
      ROSE_ASSERT(init != cur_loop->get_initialization());
      cur_loop->set_initialization(init);
      if (init)
      {
        init->set_parent(cur_loop);
        SageInterface::setSourcePositionForTransformation(init);
      }

      SgExpression* bound = isSgExpression(newhead[1]);
      cur_loop->set_bound(bound);
      if (bound)
      {
        bound->set_parent(cur_loop);
        SageInterface::setSourcePositionForTransformation(bound);
      }

      SgExpression* incr = isSgExpression(newhead[2]);
      cur_loop->set_increment(incr);
      if (incr)
      {
        incr->set_parent(cur_loop);
        SageInterface::setSourcePositionForTransformation(incr);
      }
    }
  }
  return true;
}

//! Interchange/Permutate a n-level perfectly-nested loop rooted at 'loop' using a lexicographical order number within [0,depth!)
bool ICE::InterchangeUIUC::loopInterchange(SgForStatement* loop, size_t depth, size_t lexicoOrder)
{
  if (lexicoOrder == 0) // allow 0 to mean no interchange at all
    return true;
  ROSE_ASSERT(lexicoOrder<myfactorial(depth));
  // convert the lexicographical number to a permutation order array permutation[depth]
  std::vector<size_t> changedOrder = getPermutationOrder (depth, lexicoOrder);

  return loopInterchange(loop, depth, changedOrder); 
}

//! Interchange/Permutate a n-level perfectly-nested loop rooted at 'loop' using a lexicographical order number within [0,depth!)
bool ICE::InterchangeUIUC::loopInterchange(SgForStatement* loop, size_t depth, const std::vector<size_t> &changedOrder)
{
 // parameter verification
  ROSE_ASSERT(loop != NULL);
  //must have at least two levels
  ROSE_ASSERT (depth >1);
  ROSE_ASSERT (changedOrder.size()>1);

  // Check the neworder sequence 
  if(checkNewOrder(changedOrder))
  {
	  std::cerr << "ERROR!! The new order provided is incorrect." << std::endl;
	  return false;
  }

  //TODO need to verify the input loop has n perfectly-nested children loops inside
  // save the loop nest's headers: init, test, and increment
  std::vector<SgForStatement* > loopNest = SageInterface::querySubTree<SgForStatement>(loop,V_SgForStatement);
  ROSE_ASSERT(loopNest.size()>=depth);
  std::vector<std::vector<SgNode*> > loopHeads;
  for (std::vector<SgForStatement* > ::iterator i = loopNest.begin(); i!= loopNest.end(); i++)
  {
    SgForStatement* cur_loop = *i;
    std::vector<SgNode*> head;
    head.push_back(cur_loop->get_for_init_stmt());
    head.push_back(cur_loop->get_test());
    head.push_back(cur_loop->get_increment());
    loopHeads.push_back(head);
  }

  // rewrite the loop nest to reflect the permutation
  // set the header to the new header based on the permutation array
  for (size_t i=0; i<depth; i++)
  {
    // only rewrite if necessary
    if (i != changedOrder[i])
    {
      SgForStatement* cur_loop = loopNest[i];
      std::vector<SgNode*> newhead = loopHeads[changedOrder[i]];

      SgForInitStatement* init = isSgForInitStatement(newhead[0]);
      //ROSE_ASSERT(init != NULL) // could be NULL?
      ROSE_ASSERT(init != cur_loop->get_for_init_stmt());
      cur_loop->set_for_init_stmt(init);
      if (init)
      {
        init->set_parent(cur_loop);
	SI::setSourcePositionForTransformation(init);
      }

      SgStatement* test = isSgStatement(newhead[1]);
      cur_loop->set_test(test);
      if (test)
      {
        test->set_parent(cur_loop);
	SI::setSourcePositionForTransformation(test);
      }

      SgExpression* incr = isSgExpression(newhead[2]);
      cur_loop->set_increment(incr);
      if (incr)
      {
        incr->set_parent(cur_loop);
	SI::setSourcePositionForTransformation(incr);
      }
    }
  }
  return true;
}


void ICE::InterchangeUIUC::commandLineProcessing(std::vector<std::string> &argvList) {

  if (CommandlineProcessing::isOption (argvList,"-rose:interchange:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for interchange functions..."<<endl;
    enable_debug= true;
  }

/*  if (CommandlineProcessing::isOptionWithParameter(argvList,"-rose:tiling:","target_level", target_level, true))
  {	
    cout<<"Target level is " << target_level << endl;	
  }

  if (CommandlineProcessing::isOptionWithParameter (argvList,"-rose:tiling:","tile_size", tile_size, true))
  {	
    cout<<"Tile size is " << tile_size << endl;	
  }
*/

  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC Interchange-specific options"<<endl;
    cout<<"Usage: loopInterchange [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
    cout<<"\t-rose:interchange:enable_debug                   run interchange in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }
}

/*
 * Receive a vector of numbers to be used as new order,
 * and checks if they comprised all the loops in the depth.
 * For instance, 2,0,1 are valid but 2,0,4 is not.
 * Return 0 if everything is correct, 1 otherwise. 
 */
template <typename N>
int ICE::InterchangeUIUC::checkNewOrder(std::vector<N> neworder)
{
	std::sort(neworder.begin(), neworder.end());
	N inc = 0;
	int ret = 0;
	for(typename std::vector< N >::iterator it = neworder.begin(); it != neworder.end(); ++it) {
		if (*it != inc)
		{
			ret = 1;
			break;
		}	
		++inc;
	}

	return ret;
}

template int ICE::InterchangeUIUC::checkNewOrder<size_t> (std::vector<size_t> neworder);

template ICE::Result ICE::InterchangeUIUC::interchange < const std::vector<size_t>& > (SgStatement* s, size_t depth, const std::vector<size_t> &neworder);

template ICE::Result ICE::InterchangeUIUC::interchange < size_t > (SgStatement* s, size_t depth, size_t neworder);

