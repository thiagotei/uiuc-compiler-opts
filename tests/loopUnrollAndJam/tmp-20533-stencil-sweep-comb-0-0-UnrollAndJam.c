#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void sweep(int bx, int by, double *anew, const double *aold, double *heatPtr)
{
  int i;
  int j;
  double heat;
  heat = 0.0;
  #pragma uiuc_unrollandjam 1 4
  for (int j = 1; j < (by + 1); ++j)
  {
    for (int i = 1; i < (bx + 1); ++i)
    {
      anew[(j * (bx + 2)) + i] = (aold[(j * (bx + 2)) + i] / 2.0) + (((((aold[(j * (bx + 2)) + (i - 1)] + aold[(j * (bx + 2)) + (i + 1)]) + aold[((j - 1) * (bx + 2)) + i]) + aold[((j + 1) * (bx + 2)) + i]) / 4.0) / 2.0);
      heat += anew[(j * (bx + 2)) + i];
    }

  }

  *heatPtr = heat;
}

int main(int argc, char **argv)
{
  int n;
  int i;
  int k;
  int ntest = 10;
  double *aold;
  double *anew;
  double temp;
  clock_t t0;
  clock_t t1;
  double t;
  double rate;
  double tmin;
  if (argc != 2)
  {
    fprintf(stderr, "usage: %s n\n", argv[0]);
    exit(1);
  }

  n = atoi(argv[1]);
  aold = (double *) malloc((n * n) * (sizeof(double)));
  anew = (double *) malloc((n * n) * (sizeof(double)));
  for (i = 0; i < (n * n); i++)
  {
    aold[i] = i;
    anew[i] = -1.0;
  }

  tmin = 1.e30;
  for (k = 0; k < ntest; k++)
  {
    t0 = clock();
    sweep(n - 2, n - 2, anew, aold, &temp);
    sweep(n - 2, n - 2, aold, anew, &temp);
    t1 = clock();
    t = (t1 - t0) * 1.e-6;
    if (t < tmin)
      tmin = t;

  }

  rate = (((2 * 6) * (n - 2)) * (n - 2)) / tmin;
  printf("time = %.2e, rate = %.2e MFLOPS\n", tmin, rate * 1.e-6);
  free(aold);
  free(anew);
  return 0;
}

