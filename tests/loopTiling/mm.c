#include <stdio.h>
 
int main()
{
  int m = 10, p = 10, q = 10, c, d, k, sum = 0;
  int first[10][10], second[10][10], multiply[10][10];
 
#pragma uiuc_tiling 3 10 
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          sum = sum + first[c][k]*second[k][d];
        }
	printf("%d\t", sum); 
        multiply[c][d] = sum;
        sum = 0;
      }
    }
 
    printf("Product of entered matrices:-\n");
#pragma rose_outline 
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++)
        printf("%d\t", multiply[c][d]);
 
      printf("\n");
    }
 
  return 0;
}
