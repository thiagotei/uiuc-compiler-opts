#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
extern int i;
typedef struct {
int row_dim;
int col_dim;
double *data;
double **cols;}matrix;
typedef matrix *Matrix;
enum CUTOFF_NONE_enum {CUTOFF_NONE=0,CUTOFF_SIMPLE=1,CUTOFF_ENERGY=2,CUTOFF_STRETCH=3,CUTOFF_CLASSES=4} ;
typedef enum CUTOFF_NONE_enum CutOffMode;
typedef struct {
int numSubjects;
int numPixels;
int basisDim;
Matrix values;
Matrix basis;
Matrix mean;
int useLDA;
CutOffMode cutOffMode;
double cutOff;
int dropNVectors;}Subspace;
extern Subspace * restrict s;
extern double deltaSumOfSquares;
extern  restrict Matrix deltaCentered;

void loop()
{
  int __i_0__ = i;
  
#pragma uiuc_loretiling 1 8
  for (__i_0__ = 0; __i_0__ <= s -> numPixels - 1; __i_0__ += 1) {
    deltaSumOfSquares += deltaCentered -> cols[0][__i_0__] * deltaCentered -> cols[0][__i_0__];
  }
  
  i = __i_0__;
}
