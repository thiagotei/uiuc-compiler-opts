
typedef struct {
	int num_zones;
	int num_groups;
	int group0;
	int num_directions;
	double * ell;
	double * psi;
	double * phi;
} Subdomain;

typedef struct {
	int total_num_moments;
	Subdomain * subdomains;
	int subdomains_size;
}GridData;

void LTimes(GridData * grid_data) {
  // Outer parameters
  const int nidx = grid_data->total_num_moments;

  // Clear phi
//  for(int ds = 0;ds < grid_data->num_zone_sets;++ ds){
//    grid_data->phi[ds]->clear(0.0);
//  }

 // Loop over Subdomains
  int num_subdomains = grid_data->subdomains_size;
  for (int sdom_id = 0; sdom_id < num_subdomains; ++ sdom_id){
    Subdomain * sdom = &(grid_data->subdomains[sdom_id]);

    // Get dimensioning
    const int num_zones = sdom->num_zones;
    const int num_local_groups = sdom->num_groups;
    const int group0 = sdom->group0;
    const int num_local_directions = sdom->num_directions;

    // Get pointers
    double * ell = sdom->ell;
    double * psi = sdom->psi;
    double       * phi = sdom->phi;

#ifdef KRIPKE_USE_OPENMP
#pragma omp parallel for
#endif
#pragma uiuc_licm
    for(int g = 0;g < num_local_groups; ++ g){
      for(int nm_offset = 0;nm_offset < nidx;++nm_offset){
         for(int d = 0; d < num_local_directions; d++) {
            for(int z = 0;z < num_zones; ++ z){

               double       * phi_final = phi + (group0+g)*num_zones*nidx + nm_offset*num_zones;
               double const * ell_final = ell + nm_offset*num_local_directions;
               double const * psi_final = psi + g*num_zones*num_local_directions + d*num_zones;

               //(*sdom.phi)(group0+g,nm_offset,z) += (*sdom.ell)(nm_offset,d,0) * (*sdom.psi)(g,d,z);
               phi_final[z] += ell_final[d] * psi_final[z];
            }
      }
    }
   }
  } // Subdomain
}


