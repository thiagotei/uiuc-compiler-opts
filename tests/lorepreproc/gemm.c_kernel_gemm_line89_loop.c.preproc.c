#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
extern int i;
extern int ni;
extern int j;
extern int nj;
extern double C[1000 + 0][1100 + 0];
extern double beta;
extern int k;
extern int nk;
extern double alpha;
extern double A[1000 + 0][1200 + 0];
extern double B[1200 + 0][1100 + 0];

void loop()
{
  int __i_0__ = i;
  int __j_1__ = j;
  int __k_2__ = k;
  
#pragma uiuc_loreunrollandjam 2 3
  for (__i_0__ = 0; __i_0__ < ni; __i_0__++) {
    for (__j_1__ = 0; __j_1__ < nj; __j_1__++) {
      C[__i_0__][__j_1__] *= beta;
    }
    for (__k_2__ = 0; __k_2__ < nk; __k_2__++) {
      for (__j_1__ = 0; __j_1__ < nj; __j_1__++) {
        C[__i_0__][__j_1__] += alpha * A[__i_0__][__k_2__] * B[__k_2__][__j_1__];
      }
    }
  }
  
  i = __i_0__;
  j = __j_1__;
  k = __k_2__;
}
