#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
extern int i;
extern double * __restrict__ window;

void loop()
{
  int __i_0__ = i;
  
#pragma uiuc_loredepavailable
  for (__i_0__ = 0; __i_0__ <= 1023; __i_0__ += 1) {
    window[__i_0__] = 0.5 * (1 - cos(2.0 * 3.14159265358979 * (__i_0__ - 0.5) / 1024));
  }
  
  i = __i_0__;
}
