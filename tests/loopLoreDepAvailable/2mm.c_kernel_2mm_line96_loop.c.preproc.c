#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
extern int i;
extern int ni;
extern int j;
extern int nl;
extern double D[800 + 0][1200 + 0];
extern double beta;
extern int k;
extern int nj;
extern double tmp[800 + 0][900 + 0];
extern double C[900 + 0][1200 + 0];

void loop()
{
  int __i_0__ = i;
  int __j_1__ = j;
  int __k_2__ = k;
  
#pragma uiuc_loredepavailable
  for (__i_0__ = 0; __i_0__ <= ni - 1; __i_0__ += 1) {
    for (__j_1__ = 0; __j_1__ <= nl - 1; __j_1__ += 1) {
      D[__i_0__][__j_1__] *= beta;
      for (__k_2__ = 0; __k_2__ <= nj - 1; __k_2__ += 1) {
        D[__i_0__][__j_1__] += tmp[__i_0__][__k_2__] * C[__k_2__][__j_1__];
      }
    }
  }
  
  i = __i_0__;
  j = __j_1__;
  k = __k_2__;
}
