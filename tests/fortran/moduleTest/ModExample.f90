program module_example     
use ModConstants      
implicit none     


   integer, parameter :: rowsA = 10, colsA = 10, rowsB = 10, colsB = 10
   integer, parameter :: rowsC = 10, colsC = 10
   REAL, DIMENSION(rowsA,colsA) :: A !Matrix A
   REAL, DIMENSION(rowsB,colsB) :: B !Matrix B
   REAL, DIMENSION(rowsA,colsC) :: C !Matrix C

   real :: x, ePowerx, area, radius 
   INTEGER :: i, j, k !Counters

   x = 2.0
   radius = 7.0
   ePowerx = e ** x
   area = pi * radius**2     
   
   call show_consts() 
   
   print*, "e raised to the power of 2.0 = ", ePowerx
   print*, "Area of a circle with radius 7.0 = ", area  
   
!$  uiuc_unroll 3 5
    DO i = 1, rowsA, 1
            DO j = 1, colsB, 1
                    DO k = 1, colsA, 1 
                            C(i,j) = C(i,j) + A(i,k)*B(k,j)
                    END DO
            END DO
    END DO

end program module_example
