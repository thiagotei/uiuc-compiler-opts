#include <stdio.h>
 
int main()
{
  int m = 16, p = 16, q = 16, c, d, k;
  int first[16][16], second[16][16], multiply[16][16];
  int x = 0;
 
#pragma uiuc_unroll 3 4 
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          x = x + 1;
          multiply[c][d] += first[c][k]*second[k][d];
        }
       }
     }
      
   
 
    printf("Product of entered matrices:-\n");
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++)
        printf("%d\t", multiply[c][d]);
 
      printf("\n");
    }
 
  return 0;
}
