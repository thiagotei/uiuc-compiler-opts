#ifndef CONFIG_HH
#define CONFIG_HH

namespace restructurer {

const bool verify_output = false;
const int verbose=0;

const int num_transformations = 5;

extern bool write_to_db;

const std::string transformations[num_transformations] = {
    "interchange",
    "tiling",
    "unrolling",
    "distribution",
    "unrolljam"
};


};


#endif
