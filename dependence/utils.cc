#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdio>

#include <uuid/uuid.h>

#include "rose.h"

#include <AstInterface_ROSE.h>

#include "utils.hh"
#include "config.hh"


using namespace std;
using namespace restructurer;

void restructurer::printBanner(string s) {
    cout << endl << "---------- " << s << " ----------" << endl << endl;
}

string restructurer::exec_cmd(const string &cmd, bool verbose) {
    
    if (verbose) {
        cout << "Execute " << cmd << endl;
    }

    FILE *pipe = popen(cmd.c_str(), "r");
    if (!pipe) {
        return "";
    }
    char buffer[128];
    string result;
    while (!feof(pipe)) {
        if (fgets(buffer, 128, pipe) != NULL) {
            result += buffer;
        }
    }

    pclose(pipe);
    return result;
}

void restructurer::compile(const string &src, const string &target) {
    string cmd("gcc " + src + " -o " + target + " -std=c99 -lcrypto -lssl");
    exec_cmd(cmd);
}

bool restructurer::generate_output(const string &ref_out, int file_idx) {
    
    SgProject* project = SageInterface::getProject();

    //printBanner("Generate Output Source File");

    SgFile &file = project->get_file(0);
    string file_name = file.get_sourceFileNameWithoutPath();

    if (file_name.find(".preproc.c") != string::npos) {
        file_name = file_name.substr(0, file_name.size() - 10);
    }

    stringstream ss;

    ss << file_name << "." << file_idx << ".c";

    string out_file_name(ss.str());
    string src_dir(file.getSourceDirectory());
    string out_dir = src_dir + "/" + file_name + "_mutations";

    exec_cmd("mkdir -p " + out_dir);

    file.set_unparse_output_filename(out_file_name);

    string file_path = src_dir + "/" + out_file_name;
    string bin_path = file_path + ".out";

    project->unparse();

    exec_cmd("mv " + out_file_name + " " + src_dir);

    //printBanner("Compile Output Source File");


    //printBanner("Compare Output Against Reference");

    if (verify_output) {

        compile(file_path, bin_path);

        string output = exec_cmd(bin_path);
        cout << "Output:" << endl << output << endl;

        if (output == ref_out) {

            exec_cmd("mv " + file_path + " " + out_dir);
            exec_cmd("mv " + bin_path + " " + out_dir);

            cout << "Match" << endl;
            file_idx++;

            return true;
        } else {
            cout << "Mismatch, discard" << endl;
            //exec_cmd("rm " + file_path + " " + bin_path);

            return false;
        }

    } else {

        exec_cmd("mv " + file_path + " " + out_dir);
        cout << "Written to mutation " << file_idx << endl;

        return true;

    }


}

void restructurer::string_replace(string& subject, const string& search,
                          const string& replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
}

string restructurer::uuid_gen() {

    uuid_t id;
    uuid_generate(id);
    char buff[36];
    memset(buff, 0, 36);
    uuid_unparse(id, buff);

    string ret(buff);
    return ret;
}











