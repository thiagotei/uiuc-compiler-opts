#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cstdio>
#include <string>
#include <sstream>

#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>

#include "rose.h"

#include <AstInterface_ROSE.h>

/*
#include <polyopt/PolyOpt.hpp>
#include <polyopt/ScopExtractor.hpp>
#include <polyopt/SageNodeAnnotation.hpp>
*/
#include <PolyOpt.hpp>
#include <ScopExtractor.hpp>
#include <SageNodeAnnotation.hpp>

#include "utils.hh"
#include "dependence.hh"


using namespace std;
using namespace restructurer;

const char *restructurer::Dependence::dep_type_str_[4] = {"RAW", "WAR", "WAW", "RAR"};
const char *restructurer::Dependence::dir_type_str_[4] = {">", "<", "=", "*"};


restructurer::Dependence::Dependence(CandlDependence *cdep) {

    CandlStatement* src = cdep->source;
    CandlStatement* dst = cdep->target;

    scoplib_statement_p scoplib_src = (scoplib_statement_p) (src->ref);
    scoplib_statement_p scoplib_dst = (scoplib_statement_p) (dst->ref);

    source_stmt_ = (SgExprStatement *) (scoplib_src->body);
    target_stmt_ = (SgExprStatement *) (scoplib_dst->body);

    source_id_ = src->label;
    target_id_ = dst->label;

    depth_ = cdep->depth;

    CandlMatrix *src_matrix;
    CandlMatrix *dst_matrix;

    vector<SgNode *> src_ref;
    vector<SgNode *> dst_ref;

    ScopStatementAnnotation* annot_src = 
        (ScopStatementAnnotation*) (source_stmt_->getAttribute("ScopStmt"));

    ScopStatementAnnotation* annot_dst = 
        (ScopStatementAnnotation*) (target_stmt_->getAttribute("ScopStmt"));

    switch (cdep->type) {
    case CANDL_RAW:
        type_ = RAW;
        src_matrix = src->written;
        dst_matrix = dst->read;
        source_is_read_ = false;
        target_is_read_ = true;
        src_ref = annot_src->writeAffineRefs;
        dst_ref = annot_dst->readAffineRefs;
        break;
    case CANDL_WAR:
        type_ = WAR;
        src_matrix = src->read;
        dst_matrix = dst->written;
        source_is_read_ = true;
        target_is_read_ = false;
        src_ref = annot_src->readAffineRefs;
        dst_ref = annot_dst->writeAffineRefs;
        break;
    case CANDL_WAW:
        type_ = WAW;
        src_matrix = src->written;
        dst_matrix = dst->written;
        source_is_read_ = false;
        target_is_read_ = false;
        src_ref = annot_src->writeAffineRefs;
        dst_ref = annot_dst->writeAffineRefs;
        break;
    case CANDL_RAR:
        type_ = RAR;
        src_matrix = src->read;
        dst_matrix = dst->read;
        source_is_read_ = true;
        target_is_read_ = true;
        src_ref = annot_src->readAffineRefs;
        dst_ref = annot_dst->readAffineRefs;
        break;
    default:
        ROSE_ASSERT(0);
        break;
    }

    source_ref_id_ = findRef(src_ref, src_matrix, cdep->ref_source);
    target_ref_id_ = findRef(dst_ref, dst_matrix, cdep->ref_target);

    source_ref_ = src_ref[source_ref_id_];
    target_ref_ = dst_ref[target_ref_id_];

    CandlDDV *cddv = candl_ddv_create_from_dep(cdep, 1, min(src->depth, dst->depth));

    for (int i = 0; i < cddv->length; ++i) {

        DirType dir;
        int dist = 0;

        switch (cddv->data[i].type) {
        case candl_dv_eq:
            dir = EQUAL;
            break;
        case candl_dv_plus:
            dir = PLUS;
            break;
        case candl_dv_minus:
            dir = MINUS;
            break;
        case candl_dv_star:
            dir = STAR;
            break;
        case candl_dv_scalar:
            dist = cddv->data[i].value;
            if (cddv->data[i].value > 0) {
                dir = PLUS;
            } else if (cddv->data[i].value < 0) {
                dir = MINUS;
            } else {
                ROSE_ASSERT(0);
            }
            break;
        default:
            ROSE_ASSERT(0);
            break;
        }

        dir_vec_.push_back(dir);
        dist_vec_.push_back(dist);
    }

    candl_ddv_free(cddv);

}

bool restructurer::Dependence::operator == (const Dependence &other) const {

    if (source_id_ != other.source_id_) {
        return false;
    }

    if (target_id_ != other.target_id_) {
        return false;
    }

    if (source_ref_id_ != other.source_ref_id_) {
        return false;
    }

    if (target_ref_id_ != other.target_ref_id_) {
        return false;
    }

    if (type_ != other.type_) {
        return false;
    }

    return true;
}

bool restructurer::Dependence::operator < (const Dependence &other) const {

    typedef boost::tuple<int, int, int, int, int> dep_tuple_t;

    dep_tuple_t a(source_id_, target_id_, source_ref_id_, target_ref_id_, type_);
    dep_tuple_t b(other.source_id_, other.target_id_, other.source_ref_id_, other.target_ref_id_, other.type_);

    return (a < b);
}

int restructurer::Dependence::findRef(vector<SgNode *> &refs, CandlMatrix *ref_matrix, int idx) {

    int count = 0;
    for (int i = 0; i < idx; ++i) {
        if (ref_matrix->p[i][0] != 0) {
            count++;
        }
    }

    return count;

}

void restructurer::DependenceGraph::UpdateDepGraph(const Dependence &dep) {

    id_pair_t src(dep.source_id_, dep.source_ref_id_);
    id_pair_t dst(dep.target_id_, dep.target_ref_id_);

    if (ref_dep_graph_.find(src) == ref_dep_graph_.end()) {
        ref_dep_graph_[src] = id_set_t();
    }
    if (ref_dep_graph_.find(dst) == ref_dep_graph_.end()) {
        ref_dep_graph_[dst] = id_set_t();
    }

    ref_dep_graph_[src].insert(dst);
    ref_dep_graph_[dst].insert(src);

    stmt_dep_graph_[dep.source_id_].insert(dep.target_id_);
    stmt_dep_graph_[dep.target_id_].insert(dep.source_id_);

}

void restructurer::DependenceGraph::UpdateStatementSet(const Dependence &dep) {

    stmt_set_t::iterator itr = stmt_set_.find(dep.source_id_);

    if (dep.source_is_read_) {
        itr->addR(Reference(dep.source_ref_id_, dep.source_ref_));
    } else {
        itr->addW(Reference(dep.source_ref_id_, dep.source_ref_));
    }

    itr = stmt_set_.insert(Statement(dep.target_id_, dep.target_stmt_)).first;

    if (dep.target_is_read_) {
        itr->addR(Reference(dep.target_ref_id_, dep.target_ref_));
    } else {
        itr->addW(Reference(dep.target_ref_id_, dep.target_ref_));
    }


}

void restructurer::DependenceGraph::calculateDependence() {

    SgProject *project = SageInterface::getProject();
    ROSE_ASSERT(project);

    PolyRoseOptions polyoptions;
//    polyoptions.setQuiet(true);
    polyoptions.setQuiet(false);
    ScopExtractor extractor(project, polyoptions);
    vector<scoplib_scop_p> scops = extractor.getScoplibs();

    stmt_set_.clear();
    stmt_dep_graph_.clear();
    ref_dep_graph_.clear();

    stmt_to_id_map_.clear();
    dep_set_.clear();

    if (scops.size() == 0) {
	cerr << "[DepGraph][calculateDependence] Not found any scops." << endl;
        root_ = NULL;
        return;

    } else if (scops.size() > 1) {
        cerr << "WARNING: Input file contains multiple SCoP. We only process the first found one." << endl;

        for (vector<scoplib_scop_p>::iterator itr = scops.begin() + 1; itr != scops.end(); ++itr) {
            scoplib_scop_shallow_free(*itr);
        }
    }

    scoplib_scop_p scop = *(scops.begin());

    root_ = (SgNode *) (scop->usr);
        
    CandlOptions *coptions = candl_options_malloc();
    //coptions->scalar_expansion = 1;
    CandlProgram *cprogram = candl_program_convert_scop(scop, NULL);


    CandlDependence *cdeps = candl_dependence(cprogram, coptions);

    for (int i = 0; i < cprogram->nb_statements; ++i) {

        CandlStatement *cstmt = cprogram->statement[i];
        scoplib_statement_p sstmt = (scoplib_statement_p) (cstmt->ref);

        SgExprStatement *stmt = (SgExprStatement *) (sstmt->body);

        stmt_set_.insert(Statement(i, stmt));
        stmt_to_id_map_[stmt] = i;
        stmt_dep_graph_[i] = int_set_t();

    }



    //candl_dependence_pprint(stdout, cdeps);

    for (CandlDependence *ptr = cdeps; ptr; ptr = ptr->next) {
        
        Dependence dep(ptr);
        dep_set_.insert(dep);

        UpdateDepGraph(dep);
        UpdateStatementSet(dep);

        /*cout << dep << endl;

        candl_matrix_print(stdout, ptr->source->written);
        candl_matrix_print(stdout, ptr->source->read);

        candl_matrix_print(stdout, ptr->target->written);
        candl_matrix_print(stdout, ptr->target->read);*/
    }
    
    candl_program_free(cprogram);
    candl_dependence_free(cdeps);
    candl_options_free(coptions);
    scoplib_scop_shallow_free(scop);

}

string restructurer::DependenceGraph::tab(unsigned int cnt) {

    string str;
    for (unsigned int i = 0; i < cnt; ++i) {
        str += "\t";
    }

    return str;
}

void restructurer::DependenceGraph::outputDot(ostream &os) {

    os << "digraph G {" << endl << endl;

    int statement_cnt = 0;
    int node_cnt = 0;

    stringstream ss;
    ss << tab(1) << "legend [shape = none label = \"\\" << endl;

    for (stmt_set_t::const_iterator s_itr = stmt_set_.begin(); s_itr != stmt_set_.end(); ++s_itr) {
        
        os << tab(1) << "subgraph cluster" << statement_cnt << " {" << endl;
        os << tab(2) << "label = \"S" << statement_cnt << "\";" << endl;

        ss << tab(1) << "S" << statement_cnt << ": " << s_itr->stmt_->unparseToString() << "\\" << endl;

        for (ref_set_t::const_iterator w_itr = s_itr->w_set_.begin(); w_itr != s_itr->w_set_.end(); ++w_itr) {

            os << tab(2) << "s" << statement_cnt << "w" << w_itr->id_ << " [label = \"w" << w_itr->id_ << "\"];" << endl;

            ss << tab(2) << "\\l     - w" << w_itr->id_ << ": " << w_itr->ref_->unparseToString() << "\\" << endl;
        }

        for (ref_set_t::const_iterator r_itr = s_itr->r_set_.begin(); r_itr != s_itr->r_set_.end(); ++r_itr) {

            os << tab(2) << "s" << statement_cnt << "r" << r_itr->id_ << " [label = \"r" << r_itr->id_ << "\"];" << endl;

            ss << tab(2) << "\\l     - r" << r_itr->id_ << ": " << r_itr->ref_->unparseToString() << "\\" << endl;
        }

        os << tab(1) << "}" << endl << endl;

        ss << tab(2) << "\\l\\l\\" << endl; 

        statement_cnt++;
    }

    ss << tab(1) << "\"];";

    for (dep_set_t::const_iterator d_itr = dep_set_.begin(); d_itr != dep_set_.end(); ++d_itr) {

        os << tab(1) << "s" << d_itr->source_id_ << (d_itr->source_is_read_ ? "r" : "w") << d_itr->source_ref_id_;
        os << " -> s" << d_itr->target_id_ << (d_itr->target_is_read_ ? "r" : "w") << d_itr->target_ref_id_;

        os << " [label = \"" << Dependence::dep_type_str_[d_itr->type_] << "\\n[ ";

        for (vector<Dependence::DirType>::const_iterator v_itr = d_itr->dir_vec_.begin();
            v_itr != d_itr->dir_vec_.end(); ++v_itr) {
            os << Dependence::dir_type_str_[*v_itr] << " ";
        }

        os << "]\"];" << endl;
    }

    os << endl << ss.str() << endl << endl << "}" << endl << endl;

}

vector<DependenceGraph::int_set_t> restructurer::DependenceGraph::getDisjointStatmentSets() const {

    stmt_dep_graph_t stmt_dep_graph = stmt_dep_graph_;
    vector<int_set_t> disjoint_sets;

    while (stmt_dep_graph.size()) {
        
        int_set_t connected;

        stmt_dep_graph_t::iterator itr = stmt_dep_graph.begin();
        connected.insert(itr->first);

        queue<int> work_queue;
        work_queue.push(itr->first);

        while (work_queue.size()) {

            int stmt = work_queue.front();
            work_queue.pop();

            int_set_t dep_stmts = stmt_dep_graph[stmt];
            stmt_dep_graph.erase(stmt);

            for (int_set_t::iterator s_itr = dep_stmts.begin(); s_itr != dep_stmts.end(); ++s_itr) {

                if (connected.find(*s_itr) == connected.end()) {

                    work_queue.push(*s_itr);
                    connected.insert(*s_itr);
                }
            }

        }

        disjoint_sets.push_back(connected);
    }

    return disjoint_sets;
}

int restructurer::DependenceGraph::getStatementId(SgNode *stmt) const {

    id_map_t::const_iterator finder = stmt_to_id_map_.find(stmt);
    if (finder == stmt_to_id_map_.end()) {
        return -1;
    }

    return finder->second;

}

SgExprStatement *restructurer::DependenceGraph::getStatementById(int id) const {

    stmt_set_t::iterator finder = stmt_set_.find(id);
    if (finder == stmt_set_.end()) {
        //cout << "Stmt ID " << id << " not found" << endl;
        return NULL;
    }

    //cout << "Stmt ID " << id << ": " << finder->stmt_->unparseToString() << endl;
    return finder->stmt_;

}

bool restructurer::DependenceGraph::isPermutationLegal(const vector<size_t> &order) const {
    for (dep_set_t::const_iterator itr = dep_set_.begin(); itr != dep_set_.end(); ++itr) {

        if (order.size() != itr->dir_vec_.size()) {
            return false;
        }

        /*for (int i = order.size() - 1; i >= 0; --i) {
            cout << Dependence::dir_type_str_[itr->dir_vec_[order.size() - 1 - order[i]]] << " ";
        }
        cout << endl;*/

        for (int i = 0; i < order.size(); ++i) {
            bool legal = false;
            switch (itr->dir_vec_[order[i]]) {
            case Dependence::MINUS: legal = true; break;
            case Dependence::PLUS:
            case Dependence::STAR: return false;
            default: break;
            }

            if (legal) {
                break;
            }
        }

    }
    return true;
}


bool restructurer::DependenceGraph::isUnrollAndJamLegal(int level, int factor) const {

    for (dep_set_t::const_iterator itr = dep_set_.begin(); itr != dep_set_.end(); ++itr) {

        if (level >= itr->dir_vec_.size()) {
            return false;
        }

        // assuming the original dependence holds, interchange would never go wrong with these conditions
        if (itr->dir_vec_[level] == Dependence::STAR || itr->dir_vec_[level] == Dependence::PLUS) {
            continue;
        }
        switch (itr->dir_vec_.back()) {
            case Dependence::MINUS: break;
            case Dependence::PLUS: {
                if (-itr->dist_vec_[level] < factor) {
                    return false;
                }
                break;
            }
            case Dependence::STAR: return false;
            default: break;      
        }

    }

    return true;
}

bool restructurer::DependenceGraph::isTilingLegal(int level) const {

    for (dep_set_t::const_iterator itr = dep_set_.begin(); itr != dep_set_.end(); ++itr) {

        if (level >= itr->dir_vec_.size()) {
            return false;
        }

        switch (itr->dir_vec_[level]) {
            case Dependence::EQUAL:
            case Dependence::MINUS: continue;
            default: return false;      
        }

    }

    return true;
}

void restructurer::DependenceGraph::printDepSet(ostream &os, const dep_set_t &dep_set) const {
    for (dep_set_t::const_iterator itr = dep_set.begin(); itr != dep_set.end(); ++itr) {
        os << *itr << endl;
    }
}

void restructurer::DependenceGraph::printDepSet(ostream &os) const {
    printDepSet(os, dep_set_);
}


ostream& operator << (ostream &os, const Dependence &dep) {

    os << "s" << dep.source_id_ << "[" << (dep.source_is_read_ ? "r" : "w") << dep.source_ref_id_ << "]" << " -> ";
    os << "s" << dep.target_id_ << "[" << (dep.target_is_read_ ? "r" : "w") << dep.target_ref_id_ << "]";

    os << " depth = " << dep.depth_;

    os << " " << Dependence::dep_type_str_[dep.type_] << " [ ";

    for (vector<Dependence::DirType>::const_iterator itr = dep.dir_vec_.begin();
        itr != dep.dir_vec_.end(); ++itr) {
        os << Dependence::dir_type_str_[*itr] << " ";
    }

    os << "][";

    for (vector<int>::const_iterator itr = dep.dist_vec_.begin();
        itr != dep.dist_vec_.end(); ++itr) {
        if (*itr) {
            os << *itr << " ";
        } else {
            os << "? ";
        }
    }

    os << "]";

    os << endl << "\tstatement:\t" << dep.source_stmt_->unparseToString() << " -> "
        << dep.target_stmt_->unparseToString();
    os << endl << "\treference:\t" << dep.source_ref_->unparseToString() << " -> "
        << dep.target_ref_->unparseToString();

    return os;
}

void restructurer::DependenceGraph::printStmtDepSet(std::ostream &os) {

    for (stmt_dep_graph_t::iterator m_itr = stmt_dep_graph_.begin(); m_itr != stmt_dep_graph_.end(); ++m_itr) {
        os << "s" << m_itr->first << ": ";

        const int_set_t &stmt_set = m_itr->second;
        for (int_set_t::const_iterator s_itr = stmt_set.begin(); s_itr != stmt_set.end(); ++s_itr) {
            os << "s" << *s_itr << " ";
        }

        os << endl;

    }


}














