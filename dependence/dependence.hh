#ifndef DEPENDENCE_HH
#define DEPENDENCE_HH

#include <candl/candl.h>
#include <candl/ddv.h>
#include <scoplib/scop.h>

#include <set>

namespace restructurer {

struct Dependence {

    static const char *dep_type_str_[4];
    static const char *dir_type_str_[4];

    typedef enum {
        RAW = 0,
        WAR,
        WAW,
        RAR
    } DepType;

    typedef enum {
        PLUS = 0,
        MINUS,
        EQUAL,
        STAR
    } DirType;

    int source_id_;
    int target_id_;

    SgExprStatement *source_stmt_;
    SgExprStatement *target_stmt_;

    int source_ref_id_;
    int target_ref_id_;

    SgNode *source_ref_;
    SgNode *target_ref_;

    bool source_is_read_;
    bool target_is_read_;

    int depth_;
    DepType type_;

    std::vector<DirType> dir_vec_;
    std::vector<int> dist_vec_;

    Dependence(CandlDependence *cdep);

    bool operator == (const Dependence &other) const;
    bool operator < (const Dependence &other) const;

protected:

    int findRef(std::vector<SgNode *> &refs, CandlMatrix *ref_matrix, int idx);

};

typedef std::set<Dependence> dep_set_t;

class DependenceGraph {

protected:

    SgNode *root_;
    dep_set_t dep_set_;

    typedef std::map<int, SgStatement *> statement_map_t;
    statement_map_t statement_map_;

    struct Reference {

        const int id_;
        SgNode * const ref_;

        Reference(int id, SgNode *ref) : id_(id), ref_(ref) {}

        bool operator < (const Reference &other) const {
            return id_ < other.id_;
        }

    };

    typedef std::set<Reference> ref_set_t;

    struct Statement {

        const int id_;

        SgExprStatement * const stmt_;

        mutable ref_set_t w_set_;
        mutable ref_set_t r_set_;

        Statement(int id, SgExprStatement *stmt = NULL) : id_(id), stmt_(stmt) {}

        void addW(const Reference &ref) const {
            w_set_.insert(ref);
        }

        void addR(const Reference &ref) const {
            r_set_.insert(ref);
        }

        bool operator < (const Statement &other) const {
            return id_ < other.id_;
        }
    };

    typedef std::map<SgNode *, int> id_map_t;
    id_map_t stmt_to_id_map_;

    typedef std::pair<int, int> id_pair_t;
    typedef std::set<id_pair_t> id_set_t;

    typedef std::map<id_pair_t, id_set_t> ref_dep_graph_t;
    ref_dep_graph_t ref_dep_graph_;

    typedef std::set<int> int_set_t;
    typedef std::map<int, int_set_t> stmt_dep_graph_t;
    stmt_dep_graph_t stmt_dep_graph_;

    typedef std::set<Statement> stmt_set_t;
    stmt_set_t stmt_set_;

    void UpdateDepGraph(const Dependence &dep);
    void UpdateStatementSet(const Dependence &dep);
    std::string tab(unsigned int cnt);

public:

    void calculateDependence();

    DependenceGraph() : root_(NULL) {
        calculateDependence();
    }

    void outputDot(std::ostream &os);

    SgNode *getRoot() const {
        return root_;
    }

    dep_set_t getDepSet() const {
        return dep_set_;
    }

    std::vector<int_set_t> getDisjointStatmentSets() const;

    int getStatementId(SgNode *stmt) const;
    SgExprStatement *getStatementById(int id) const;

    bool isPermutationLegal(const std::vector<size_t> &order) const;

    bool isUnrollAndJamLegal(int level, int factor) const;

    bool isTilingLegal(int level) const;

    void printDepSet(std::ostream &os, const dep_set_t &dep_set) const;
    void printDepSet(std::ostream &os) const;

    void printStmtDepSet(std::ostream &os);

};



};

std::ostream& operator << (std::ostream &os, const restructurer::Dependence &dep);

#endif
