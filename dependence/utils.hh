#ifndef UTILS_HH
#define UTILS_HH

namespace restructurer {

void printBanner(std::string s);

std::string exec_cmd(const std::string &cmd, bool verbose = false);

void compile(const std::string &src, const std::string &target);

bool generate_output(const std::string &ref_out, int file_idx);

void string_replace(std::string& subject, const std::string& search,
                          const std::string& replace);

std::string uuid_gen();

};

#endif