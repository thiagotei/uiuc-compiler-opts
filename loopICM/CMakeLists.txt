

set(
licm_src
LoopICM.cc
LoopICM.hh
)

include_directories(../util)
include_directories(../sageInterface)
include_directories(${ROSE_INC_PATH} ${Boost_INCLUDE_DIR})
add_executable(loopICM ${licm_src})
#add_library(loopInterchange SHARED ${interchange_src})
target_sources(loopICM
	PUBLIC
	../sageInterface/SageInterfaceUIUC.cc)
target_link_libraries(loopICM util)
target_link_libraries(loopICM rose)
target_link_libraries(loopICM ${Boost_LIBRARIES})
#set_target_properties(loopICM  PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE)
if(QUADM_LIB)
	target_link_libraries(loopICM quadmath)
endif()
install(TARGETS loopICM
	ARCHIVE DESTINATION lib
	LIBRARY DESTINATION lib
	RUNTIME DESTINATION bin)
