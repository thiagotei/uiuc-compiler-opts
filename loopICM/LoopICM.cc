#include "LoopICM.hh"


int main(int argc, char ** argv)
{
	vector<string> argvList(argv, argv+argc);
	ICE::LoopICMUIUC::commandLineProcessing(argvList);	
	
	//SgProject * proj = frontend(argvList);
	ICE::LoopICMUIUC::project = frontend(argvList);
	ROSE_ASSERT(ICE::LoopICMUIUC::project);

	cout << "[ICE/LoopICMUIUC]..." << endl;
	size_t count = ICE::LoopICMUIUC::licmAll(ICE::LoopICMUIUC::project);

	//TODO specify the output name.
//	if (count > 0) {
		ICE::LoopICMUIUC::project->unparse();
//	}
	cout << "[ICE/LoopICMUIUC] end" << endl;

	return 0;
}

size_t ICE::LoopICMUIUC::licmAll(SgProject * project) 
{
	size_t num_tiled = 0;	
	TargetList_t targets;

	bool debug = false;

	std::vector< std::vector<string> > pragmasinfo;
	if (SageInterface::is_Fortran_language()) 
	{
		if (AnnotParser::collectFortranTarget(project, targets, PRAGMA_NAME, pragmasinfo))
		{
			std::vector< std::vector<string> >::iterator j = pragmasinfo.begin();
                        std::vector< std::vector<string> >::iterator j_end = pragmasinfo.end();	
			for(TargetList_t::iterator i = targets.begin();  
				i != targets.end() && j != j_end; ++i, ++j)
				if(licmFortran(*i, *j).isValid())
					++num_tiled;
		}

	} else // Search for pragmas for C/C++
	{ 
		PragmaList_t pragmas;
		if(AnnotParser::collectPragmas(project, pragmas, PRAGMA_NAME))
		{
			for (PragmaList_t::iterator i = pragmas.begin(); i != pragmas.end(); ++i)
				if(licm(*i).isValid())
					++num_tiled;

		}
	}		
	return num_tiled;
}

ICE::Result ICE::LoopICMUIUC::licmFortran(SgStatement* s, vector<string>& pragmainfo)
{
	return licm(s);
}

ICE::Result ICE::LoopICMUIUC::licm(SgPragmaDeclaration* prag) 
{
	vector<string> pragmainfo;
	SgStatement* s = AnnotParser::processPragmaInfo (prag, PRAGMA_NAME, pragmainfo);
	if(!s) return Result(false);	

	return licm(s);
}

ICE::Result ICE::LoopICMUIUC::licm(SgStatement* s) 
{
	bool r;
//	SageInterface::dumpInfo(s);

	if (SgForStatement * for_loop = isSgForStatement(s)) {
		cout << "[ICE/LoopICMUIUC/licm] For loop" << endl;
		//SageInterface::loopICM(for_loop, depth, neworder);		
		int stmtsmoved = 0;
		int moved = 0;
		//int file_idx = 0;
		//do {
			defuse = new DefUseAnalysis(project);
			defuse->run(false);
		 	moved = licm(for_loop);		
			stmtsmoved += moved;
			cout << "Moved "<< moved << " statments. Total "<< stmtsmoved <<endl;
			/*SgFile &file = project->get_file(0);
			string file_name = file.get_sourceFileNameWithoutPath();
			stringstream ss;
			ss << "tmp_"<< file_name << "." << file_idx<< ".tmp";
			string out_file_name(ss.str());
			file.set_unparse_output_filename(out_file_name);
			if(moved > 0) {
				cout << "La vai unparse" << endl << flush;
				project->unparse();
			}
			++file_idx;*/
			delete defuse;
		//} while(moved != 0);

		if (stmtsmoved > 0) {
			r = true;
		} else {
			r = false;
		}
	} else if (SgFortranDo * do_loop = isSgFortranDo(s)) {
		cout << "[ICE/LoopICMUIUC/licm] Do loop" << endl;
		r = licm(do_loop);
	} else {
		cerr << "[ICE/LoopICMUIUC/licm] ERROR! SgStatement not a For neither a Do loop!" << endl;	
		r = false;
	}
	
	return Result(r);
}

bool ICE::LoopICMUIUC::licm(SgFortranDo* loop) 
{
	cout << "Not implemented yet!" << endl;
	return false;
}

int ICE::LoopICMUIUC::licm(SgForStatement* loop)
{
	
	// Find loops
	// Find assignments in the loop bodys
	// que the def use list for each of the RHS on those assingnments
	// get higher the statment could go
	// change position of the stmt.
	
	int stmtsmoved = 0;
	NodeQuerySynthesizedAttributeType loopsList = NodeQuery::querySubTree(loop, V_SgForStatement);
	NodeQuerySynthesizedAttributeType::const_iterator it_loops = loopsList.begin();
	for(; it_loops != loopsList.end(); ++it_loops) {
		if(enable_debug){cout << "loop" << endl;}
		SgForStatement * loop = isSgForStatement(*it_loops);
		if(!loop) { continue;}

		SgStatement * loopbody = loop->get_loop_body();
		if(!loopbody) { continue;}
		if(enable_debug){cout << "loopbody" << endl;}

		//TODO implement for bodies that are not basic block
		SgBasicBlock * bodyblock = isSgBasicBlock(loopbody);
		if(!bodyblock) { continue;}

		if(enable_debug){cout << "bodyblock" << endl;}
		// Get the assignments in a loop body.
//		NodeQuerySynthesizedAttributeType assoplist = NodeQuery::querySubTree(bodyblock, V_SgAssignOp);
//		NodeQuerySynthesizedAttributeType::const_iterator it_assops = assoplist.begin();
		SgStatementPtrList& stmts = bodyblock->get_statements();
		vector<SgStatement* > rmstmts;

		for(SgStatementPtrList::const_iterator st_it = stmts.begin(); st_it != stmts.end(); st_it++) {
			if(enable_debug){cout << endl << "### type of stmt " << (*st_it)->class_name() << endl;}
			if((*st_it)->variantT() == V_SgVariableDeclaration || (*st_it)->variantT() == V_SgExprStatement ) {

				NodeQuerySynthesizedAttributeType varreflist = NodeQuery::querySubTree(*st_it, V_SgVarRefExp);
				NodeQuerySynthesizedAttributeType::const_iterator it_varref = varreflist.begin();
				vector<SgNode*> alldefs;
				for(; it_varref != varreflist.end(); ++it_varref) {
					SgVarRefExp * varRef = isSgVarRefExp(*it_varref);
					SgInitializedName* initName = isSgInitializedName(varRef->get_symbol()->get_declaration());
					std::string name = initName->get_qualified_name().str();
					SgNode * varRefParent = varRef->get_parent();

					if(enable_debug) {
						std::cout<<"---------------------------------------------"<<std::endl;
						std::cout << "--- Var " << varRef->unparseToString() << std::endl;
						std::cout << "parent:  " << varRefParent->class_name() << std::endl;
					}
					if(isSgDotExp(varRefParent)) {
						// TODO be conservative and only check teh most LHS of a SgDotExp.
						cout << "WARNING! this is dangerous! Skipping the struct fields" << endl;
						continue;
					}
					vector<SgNode* > vec = defuse->getDefFor(varRef, initName);
					if(vec.size() > 0) {
						size_t j =0;
						if(enable_debug) {
							std::cout << vec.size() << " definition entry/entries for " << varRef->unparseToString() <<  
								" @ line " << varRef->get_file_info()->get_line()<<":"<<varRef->get_file_info()->get_col() 
							<< std::endl;
							cout<<vec[j]->class_name()<<" "<<vec[j]<<endl;
						}
						SgStatement * def_stmt = SageInterface::getEnclosingStatement(vec[j]);
						ROSE_ASSERT(def_stmt);
						if(enable_debug) {
							cout<<def_stmt->unparseToString()<<"  @ line "<<def_stmt->get_file_info()->get_line()
								<<":"<<def_stmt->get_file_info()->get_col() <<endl;
						}

						SgNode * commonancestor = SIUI::leastCommonAncestor(vec[j], (*st_it));
						if(!commonancestor){ cerr<< "Error looking for common ancestor!"<<endl; return false;}
						if(enable_debug){cout << endl << "least common ancestor " << commonancestor->class_name() << endl << commonancestor->unparseToString() << endl;}
			
						alldefs.push_back(commonancestor);

					} else {
						cerr << "Aborting LICM for current loop nest! "<< varRef->unparseToString() << " @ line " 
							<< varRef->get_file_info()->get_line() <<":"<<
						     varRef->get_file_info()->get_col() << " could not be found at least one definition!" <<endl;
						return false;
					}
				}
				
				if (alldefs.size() > 0) {
					if(enable_debug){cout << "Starting loop is ancestor: len " << alldefs.size() << endl;}
					SgNode * commonancestor = alldefs[0];
					for (size_t j =1; j<alldefs.size(); j++) {
						if(enable_debug){cout << "_______________" << endl;}
						if(SI::isAncestor(commonancestor, alldefs[j])) {
							commonancestor = alldefs[j];
							if(enable_debug){cout << endl <<"Cur ancestor " << commonancestor->unparseToString() << endl;}
						} else {
							if(enable_debug){cout << endl << "Nothing change" << endl;}
						}
						if(enable_debug){cout << endl << "current ancestor " << commonancestor->class_name() << endl << commonancestor->unparseToString() << endl;}
					}
					if(enable_debug) {
						cout << endl << "Common ancestor " << commonancestor->class_name() << endl << commonancestor->unparseToString() << endl;
						cout << "&&&&&&&&" << endl;
					}
					if (commonancestor) {
						if(isSgForStatement(commonancestor)) {
							SgForStatement * fordest = isSgForStatement(commonancestor);
							SgBasicBlock * bodydest = isSgBasicBlock(fordest->get_loop_body());
							if(!bodydest) {cerr << "Error, expecting BasicBlock as loop body!" <<endl; return false;}
							
							if(bodyblock == bodydest) { cout << "Body dest is the same!" << endl; continue;}

							SgStatement* new_stmt = SI::copyStatement(*st_it);
							SI::insertStatementBeforeFirstNonDeclaration(new_stmt, bodydest, true);
							rmstmts.push_back(*st_it);
							cout << "Moved the stmt up! " << endl;
						}
					}
				} else {
					cerr << "No defs found for the RHS!" << endl;
				}
			}
		}
		vector<SgStatement*>::iterator it_rm= rmstmts.begin();
		for(; it_rm != rmstmts.end(); ++it_rm) {
			++stmtsmoved;
			SI::removeStatement(*it_rm);
			if(enable_debug){cout << "Removed stmt" << endl;}
		}
	}

	return stmtsmoved;
}

void ICE::LoopICMUIUC::commandLineProcessing(std::vector<std::string> &argvList) {

  if (CommandlineProcessing::isOption (argvList,"-rose:licm:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for licm functions..."<<endl;
    enable_debug= true;
  }

  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC LICM-specific options"<<endl;
    cout<<"Usage: loopICM [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
    cout<<"\t-rose:licm:enable_debug                   run licm in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }
}

