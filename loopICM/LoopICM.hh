
#ifndef INTERCHANGE_UIUC
#define INTERCHANGE_UIUC

#include <cstdlib>
#include <sage3basic.h>
#include <Cxx_Grammar.h>
#include <sageBuilder.h>
#include <string>
#include <vector>
#include <AnnotParser.hh>
#include <Result.hh>
#include <Misc.hh>
#include <rose.h>
#include <DefUseAnalysis.h>
#include <SageInterfaceUIUC.hh>

using namespace std;

namespace SI = SageInterface;
namespace SB = SageBuilder;
namespace SIUI = ICE::SageInterfaceUIUC;

namespace ICE
{
	namespace LoopICMUIUC
	{
		bool enable_debug = false;
		DFAnalysis *defuse;
		SgProject * project;
		static const std::string PRAGMA_NAME("uiuc_licm");
		//Delimiter to the new order string in the annotation  
		static const std::string PRAGMA_DELIMITER(",");

 		void commandLineProcessing(std::vector<std::string> &argvList); 
		size_t licmAll(SgProject * project);
		Result licmFortran(SgStatement * s, vector<std::string>& pragmainfo);
		Result licm(SgPragmaDeclaration* prag);
		Result licm(SgStatement* prag);
		bool licm(SgFortranDo* loop);
		int licm(SgForStatement* loop);

	}
}	

#endif
