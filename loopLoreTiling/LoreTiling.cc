#include "LoreTiling.hh"


int main(int argc, char ** argv)
{
	vector<string> argvList(argv, argv+argc);
	ICE::LoreTilingUIUC::commandLineProcessing(argvList);	
	
	SgProject * proj = frontend(argvList);
	ROSE_ASSERT(proj);

	cout << "[ICE/LoreTilingUIUC]..." << endl;
	int res = ICE::LoreTilingUIUC::tilingAll(proj);

	//TODO specify the output name.
	proj->unparse();
	cout << "[ICE/LoreTilingUIUC] end" << endl;

	return res;
}

int ICE::LoreTilingUIUC::tilingAll(SgProject * project) 
{
	size_t num_tiled = 0;	
	int res = 0;
	// Search for pragmas for C/C++
	PragmaList_t pragmas;
	if(AnnotParser::collectPragmas(project, pragmas, PRAGMA_NAME))
	{
		for (PragmaList_t::iterator i = pragmas.begin(); i != pragmas.end(); ++i) {
			ICE::Result resopt = tiling(*i);
			if(resopt.isValid()) {
				++num_tiled;
			} else {
				res = resopt.getValue();
				break;
			}
		}
	}
	return res;
}

ICE::Result ICE::LoreTilingUIUC::tiling(SgPragmaDeclaration* prag) 
{
//	SageInterface::dumpInfo(s);
	vector<int> pragmainfo;
	SgStatement* s = AnnotParser::processPragmaInfo (prag, PRAGMA_NAME, pragmainfo);
	if(!s) return Result(1);	

	// Expects two infos to do the tiling.
	if(pragmainfo.size() != 2) {
     		cerr<<"Error in loopLoreTiling, not proper info on the pragma."<<endl;
		return Result(1);
	}

	if(enable_debug) {
		cout << "pragma info: ";
		for(vector<int>::iterator it = pragmainfo.begin(); it != pragmainfo.end(); ++it){
			cout << *it << " ";
		}	
		cout << endl;
	}
	size_t targetLevel = pragmainfo[0];
	size_t factor = pragmainfo[1];

	SgPragmaDeclaration *newpragbefor, *newpragmafter;
	SIUI::addScopPragma(prag, newpragbefor, newpragmafter, isSgForStatement(s));
	cout << "Added Scop pragmas" << endl;
	
	DependenceGraph orig_dep_graph;
	orig_dep_graph.printDepSet(cout);

	dep_set_t orig_dep_set = orig_dep_graph.getDepSet();

	SgBasicBlock *root = isSgBasicBlock(orig_dep_graph.getRoot());
	SIUI::undoScopPragma(prag, newpragbefor, newpragmafter);
	if (root == NULL) {
		cout << "Tiling ABORTED! No proper SCoP is found in the input file." << endl;
		return Result(1);
	}

	bool legal = orig_dep_graph.isTilingLegal(targetLevel - 1);
	cout << "Legality: " << legal << endl;
	if(legal) {
		cout << "Legal!" << endl;
		return tiling(s, targetLevel, factor);
	} else {
		cout <<  "NOT legal!" << endl;
		return Result(2);
	}
}

//Tile the n-level (starting from 1)
ICE::Result ICE::LoreTilingUIUC::tiling(SgStatement* s, size_t targetLevel, size_t factor)
{
	bool r;
//	SageInterface::dumpInfo(s);

	if (SgForStatement * for_loop = isSgForStatement(s)) {
		cout << "[ICE/LoreTilingUIUC/tiling] For loop " << targetLevel  << " " << factor << endl;
		r = SI::loopTiling(for_loop, targetLevel, factor);
	} else {
		cerr << "[ICE/LoreTilingUIUC/tiling] ERROR! SgStatement not a For neither a Do loop!" << endl;	
		r = false;
	}
	int ret = 1;
	if (r) { ret = 0;}
	else {ret = 1;}
	
	return Result(ret);
}

void ICE::LoreTilingUIUC::commandLineProcessing(std::vector<std::string> &argvList) {

  if (CommandlineProcessing::isOption (argvList,"-rose:loretiling:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for tiling functions..."<<endl;
    enable_debug= true;
  }

/*  if (CommandlineProcessing::isOptionWithParameter(argvList,"-rose:tiling:","target_level", target_level, true))
  {	
    cout<<"Target level is " << target_level << endl;	
  }

  if (CommandlineProcessing::isOptionWithParameter (argvList,"-rose:tiling:","tile_size", tile_size, true))
  {	
    cout<<"Tile size is " << tile_size << endl;	
  }
*/

  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC LoreTiling-specific options"<<endl;
    cout<<"Usage: loopLoreTiling [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
    cout<<"\t-rose:loretiling:enable_debug                   run tiling in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }
}

