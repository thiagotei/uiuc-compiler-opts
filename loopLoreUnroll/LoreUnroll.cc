#include "LoreUnroll.hh"


int main(int argc, char ** argv)
{
	vector<string> argvList(argv, argv+argc);
	ICE::LoreUnrollUIUC::commandLineProcessing(argvList);	
	
	SgProject * proj = frontend(argvList);
	ROSE_ASSERT(proj);

	cout << "[ICE/LoreUnrollUIUC]..." << endl;
	int res = ICE::LoreUnrollUIUC::unrollAll(proj);

	//TODO specify the output name.
	proj->unparse();
	cout << "[ICE/LoreUnrollUIUC] end" << endl;

	return res;
}

int ICE::LoreUnrollUIUC::unrollAll(SgProject * project) 
{
	size_t num_tiled = 0;	
	int res = 0;
	// Search for pragmas for C/C++
	PragmaList_t pragmas;
	if(AnnotParser::collectPragmas(project, pragmas, PRAGMA_NAME))
	{
		for (PragmaList_t::iterator i = pragmas.begin(); i != pragmas.end(); ++i) {
			ICE::Result resopt = unroll(*i);
			if(resopt.isValid()) {
				++num_tiled;
			} else {
				res = resopt.getValue();
				break;
			}
		}
	}
	return res;
}

ICE::Result ICE::LoreUnrollUIUC::unroll(SgPragmaDeclaration* prag) 
{
//	SageInterface::dumpInfo(s);
	vector<int> pragmainfo;
	SgStatement* s = AnnotParser::processPragmaInfo (prag, PRAGMA_NAME, pragmainfo);
	if(!s) return Result(1);	

	// Expects two infos to do the tiling.
	if(pragmainfo.size() != 1) {
     		cerr<<"Error in loopLoreUnroll, not proper info on the pragma."<<endl;
		return Result(1);
	}

	if(enable_debug) {
		cout << "pragma info: ";
		for(vector<int>::iterator it = pragmainfo.begin(); it != pragmainfo.end(); ++it){
			cout << *it << " ";
		}	
		cout << endl;
	}

	return unroll(s, pragmainfo[0]);
}

ICE::Result ICE::LoreUnrollUIUC::unroll(SgStatement* s,  size_t factor)
{
	bool r;
//	SageInterface::dumpInfo(s);

	if (SgForStatement * for_loop = isSgForStatement(s)) {
		cout << "[ICE/LoreUnrollUIUC/unroll] For loop " << factor << endl;
		r = SI::loopUnrolling(for_loop, factor);
	} else {
		cerr << "[ICE/LoreUnrollUIUC/unroll] ERROR! SgStatement not a For neither a Do loop!" << endl;	
		r = false;
	}
	int ret = 1;
	if (r) { ret = 0;}
	else {ret = 1;}

	return Result(ret);
}

void ICE::LoreUnrollUIUC::commandLineProcessing(std::vector<std::string> &argvList) {

  if (CommandlineProcessing::isOption (argvList,"-rose:loreunroll:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for unroll functions..."<<endl;
    enable_debug= true;
  }

/*  if (CommandlineProcessing::isOptionWithParameter(argvList,"-rose:tiling:","target_level", target_level, true))
  {	
    cout<<"Target level is " << target_level << endl;	
  }

  if (CommandlineProcessing::isOptionWithParameter (argvList,"-rose:tiling:","tile_size", tile_size, true))
  {	
    cout<<"Tile size is " << tile_size << endl;	
  }
*/

  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC LoreUnroll-specific options"<<endl;
    cout<<"Usage: loopLoreUnroll [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
    cout<<"\t-rose:loreunroll:enable_debug                   run unroll in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }
}

