
#ifndef INTERCHANGE_UIUC
#define INTERCHANGE_UIUC

#include <cstdlib>
#include <sage3basic.h>
#include <Cxx_Grammar.h>
#include <sageBuilder.h>
#include <string>
#include <vector>
#include <AnnotParser.hh>
#include <Result.hh>
#include <Misc.hh>
#include <SageInterfaceUIUC.hh>

using namespace std;

namespace SI = SageInterface;
namespace SIUI = ICE::SageInterfaceUIUC;

namespace ICE
{
	namespace LoreUnrollUIUC
	{
		bool enable_debug = false;
		static const std::string PRAGMA_NAME("uiuc_loreunroll");
		//Delimiter to the new order string in the annotation  
		static const std::string PRAGMA_DELIMITER(",");

 		void commandLineProcessing(std::vector<std::string> &argvList); 
		Result unroll(SgPragmaDeclaration* prag);
		Result unroll(SgStatement* s,  size_t factor);
		int unrollAll(SgProject * project);
	}
}	

#endif
