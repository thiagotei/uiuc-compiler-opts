#include "SageInterfaceUIUC.hh"

SgFortranDo* ICE::SageInterfaceUIUC::buildFortranDo(
        SgExpression* initialization,
        SgExpression* bound,
        SgExpression* increment,
        SgBasicBlock* loop_body,
        bool hasEndStatement)
{
     //SgForStatement * result = new SgForStatement(test,increment, loop_body);
     SgFortranDo* result = new SgFortranDo(initialization, bound, increment, loop_body);
     ROSE_ASSERT(result);

  // DQ (11/28/2010): Added specification of case insensitivity for Fortran.
     if (SB::symbol_table_case_insensitive_semantics == true)
          result->setCaseInsensitive(true);

     SI::setOneSourcePositionForTransformation(result);

     if (initialization)
          initialization->set_parent(result);
     if (bound)
          bound->set_parent(result);
     if (loop_body)
          loop_body->set_parent(result);
     if (increment)
          increment->set_parent(result);

     result->set_has_end_statement(hasEndStatement);
     return result;
}

/*
Parameters:
     loopNest - nest where the loop to be blocked is.
     targetLevel - which loop in the nest to be blocked [1,N]
     blockSize - size of the blocking operation
     controlLoopPos - level in the nest where the new loop should be added.[0,N-1] 

 */

bool ICE::SageInterfaceUIUC::loopBlocking(SgForStatement* loopNest, size_t targetLevel, size_t blockSize, size_t controlLoopPos)
{
  ROSE_ASSERT(loopNest != NULL);
  ROSE_ASSERT(targetLevel >0);
  ROSE_ASSERT(controlLoopPos < targetLevel);
 // ROSE_ASSERT(blockSize>0);// 1 is allowed
 // skip tiling if tiling size is 0 (no tiling), we allow 0 to get a reference value for the original code being tuned
 // 1 (no need to tile)
  if (blockSize<=1)
    return true;
  // Locate the target loop at level n
  std::vector<SgForStatement* > loops= SI::querySubTree<SgForStatement>(loopNest,V_SgForStatement);
  ROSE_ASSERT(loops.size()>=targetLevel);
  SgForStatement* target_loop = loops[targetLevel -1]; // adjust to numbering starting from 0
  SgForStatement* target_control_loop = loops[controlLoopPos];

  // normalize the target loop first
  if (!SI::forLoopNormalization(target_loop));
  {// the return value is not reliable
//    cerr<<"Error in SI::loopTiling(): target loop cannot be normalized."<<endl;
//    dumpInfo(target_loop);
//    return false;
  }
   // grab the target loop's essential header information
   SgInitializedName* ivar = NULL;
   SgExpression* lb = NULL;
   SgExpression* ub = NULL;
   SgExpression* step = NULL;
   if (!SI::isCanonicalForLoop(target_loop, &ivar, &lb, &ub, &step, NULL))
   {
     cerr<<"Error in SI::loopTiling(): target loop is not canonical."<<endl;
     SI::dumpInfo(target_loop);
     return false;
   }
   ROSE_ASSERT(ivar&& lb && ub && step);

  // Add a controlling loop around the top loop nest
  // Ensure the parent can hold more than one children
  SgLocatedNode* parent = NULL; //SI::ensureBasicBlockAsParent(loopNest)
  if (SI::isBodyStatement(loopNest)) // if it is a single body statement (Already a for statement, not a basic block)
   parent = SI::makeSingleStatementBodyToBlock (loopNest);
  else
    parent = isSgLocatedNode(loopNest ->get_parent());

  ROSE_ASSERT(parent!= NULL);

  SgScopeStatement* scope = loopNest->get_scope();

  // Now we can prepend a controlling loop index variable: __lt_var_originalIndex
  string ivar2_name = findAvailVarName("_lt_var_"+ivar->get_name().getString()+"_", scope);

  SgVariableDeclaration* loop_index_decl = SB::buildVariableDeclaration
  (ivar2_name, SB::buildIntType(),NULL, scope);
  
    //Insert it before the previous statement
  SI::insertStatementBefore(SI::getPreviousStatement(loopNest), loop_index_decl);

   // init statement of the loop header, copy the lower bound
   SgStatement* init_stmt = SB::buildAssignStatement(SB::buildVarRefExp(ivar2_name,scope), SI::copyExpression(lb));
   //two cases <= or >= for a normalized loop
   SgExprStatement* cond_stmt = NULL;
   SgExpression* orig_test = target_loop->get_test_expr();
   if (isSgBinaryOp(orig_test))
   {
     if (isSgLessOrEqualOp(orig_test))
       cond_stmt = SB::buildExprStatement(SB::buildLessOrEqualOp(SB::buildVarRefExp(ivar2_name,scope),SI::copyExpression(ub)));
     else if (isSgGreaterOrEqualOp(orig_test))
     {
       cond_stmt = SB::buildExprStatement(SB::buildGreaterOrEqualOp(SB::buildVarRefExp(ivar2_name,scope),SI::copyExpression(ub)));
       }
     else
     {
       cerr<<"Error: illegal condition operator for a canonical loop"<<endl;
       SI::dumpInfo(orig_test);
       ROSE_ASSERT(false);
     }
   }
   else
   {
     cerr<<"Error: illegal condition expression for a canonical loop"<<endl;
     SI::dumpInfo(orig_test);
     ROSE_ASSERT(false);
   }
   ROSE_ASSERT(cond_stmt != NULL);

   // build loop incremental  I
   // expression var+=up*tilesize or var-=upper * tilesize
   SgExpression* incr_exp = NULL;
   SgExpression* orig_incr_exp = target_loop->get_increment();
   if( isSgPlusAssignOp(orig_incr_exp))
   {
     incr_exp = SB::buildPlusAssignOp(SB::buildVarRefExp(ivar2_name,scope), SB::buildMultiplyOp(SI::copyExpression(step), SB::buildIntVal(blockSize)));
   }
    else if (isSgMinusAssignOp(orig_incr_exp))
    {
      incr_exp = SB::buildMinusAssignOp(SB::buildVarRefExp(ivar2_name,scope), SB::buildMultiplyOp(SI::copyExpression(step), SB::buildIntVal(blockSize)));
    }
    else
    {
      cerr<<"Error: illegal increment expression for a canonical loop"<<endl;
      SI::dumpInfo(orig_incr_exp);
      ROSE_ASSERT(false);
    }
    SgForStatement* control_loop = SB::buildForStatement(init_stmt, cond_stmt,incr_exp, SB::buildBasicBlock());

  //Inserting before the loop created.
  SI::insertStatementBefore(target_control_loop, control_loop);
  // move loopNest into the control loop
  SI::removeStatement(target_control_loop);
  SI::appendStatement(target_control_loop,isSgBasicBlock(control_loop->get_loop_body()));

  // rewrite the lower (i=lb), upper bounds (i<=/>= ub) of the target loop
  SgAssignOp* assign_op  = isSgAssignOp(lb->get_parent());
  ROSE_ASSERT(assign_op);
  assign_op->set_rhs_operand(SB::buildVarRefExp(ivar2_name,scope));
    // ub< var_i+blockSize-1? ub:var_i+blockSize-1
  SgBinaryOp* bin_op = isSgBinaryOp(ub->get_parent());
  ROSE_ASSERT(bin_op);
  SgExpression* ub2 = SB::buildSubtractOp(SB::buildAddOp(SB::buildVarRefExp(ivar2_name,scope), SB::buildIntVal(blockSize)), SB::buildIntVal(1));
  SgExpression* test_exp = SB::buildLessThanOp(SI::copyExpression(ub),ub2);
  test_exp->set_need_paren(true);
  ub->set_need_paren(true);
  ub2->set_need_paren(true);
  SgConditionalExp * triple_exp = SB::buildConditionalExp(test_exp,SI::copyExpression(ub), SI::copyExpression(ub2));
  bin_op->set_rhs_operand(triple_exp);
  // constant folding
  // folding entire loop may decrease the accuracy of floating point calculation
  // we fold loop control expressions only
  //constantFolding(control_loop->get_scope());
  SI::constantFolding(control_loop->get_test());
  SI::constantFolding(control_loop->get_increment());
  return true;
}
//! Block the n-level (starting from 1) of a perfectly nested loop nest using stripmine size s and put the control level in postion p:
/* Translation
 Before:
  for (i = 0; i < 100; i++)
    for (j = 0; j < 100; j++)
      for (k = 0; k < 100; k++)
        c[i][j]= c[i][j]+a[i][k]*b[k][j];

  After stripmine i loop nest's level 3 (k-loop) with size 5, it becomes

// added a new controlling loop at the previous level
  int _lt_var_k;
    for (i = 0; i < 100; i++)
      for (j = 0; j < 100; j++)
       for (_lt_var_k = 0; _lt_var_k <= 99; _lt_var_k += 1 * 5) {
        // rewritten loop header , normalized also
        for (k = _lt_var_k; k <= (99 < (_lt_var_k + 5 - 1))?99 : (_lt_var_k + 5 - 1); k += 1) {
          c[i][j] = c[i][j] + a[i][k] * b[k][j];
        }
  }
// finally run constant folding

Parameters:
     loopNest - nest where the loop to be blocked is.
     targetLevel - which loop in the nest to be blocked [1,N]
     blockSize - size of the blocking operation
     controlLoopPos - level in the nest where the new loop should be added.[0,N-1] 

 */
bool ICE::SageInterfaceUIUC::loopBlocking(SgFortranDo* loopNest, size_t targetLevel, size_t blockSize, size_t controlLoopPos)
{
  ROSE_ASSERT(loopNest != NULL);
  ROSE_ASSERT(targetLevel >0);
  ROSE_ASSERT(controlLoopPos < targetLevel);
 // ROSE_ASSERT(blockSize>0);// 1 is allowed
 // skip stripmine if stripmine size is 0 (no stripmine), we allow 0 to get a reference value for the original code being tuned
 // 1 (no need to tile)
  if (blockSize<=1)
    return true;
  // Locate the target loop at level n
  std::vector<SgFortranDo *> loops= SI::querySubTree<SgFortranDo>(loopNest,V_SgFortranDo);
  ROSE_ASSERT(loops.size()>=targetLevel);
  SgFortranDo* target_loop = loops[targetLevel -1]; // adjust to numbering starting from 0
  SgFortranDo* target_control_loop = loops[controlLoopPos];

  // normalize the target loop first
  if (!SI::doLoopNormalization(target_loop));
  {// the return value is not reliable
//    cerr<<"Error in SI::loopStripMine(): target loop cannot be normalized."<<endl;
//    dumpInfo(target_loop);
//    return false;
  }
   // grab the target loop's essential header information
   SgInitializedName* ivar = NULL;
   SgExpression* lb = NULL;
   SgExpression* ub = NULL;
   SgExpression* step = NULL;
   if (!SI::isCanonicalDoLoop(target_loop, &ivar, &lb, &ub, &step, NULL, NULL, NULL))
   {
     cerr<<"Error in SI::loopStripMine(): target loop is not canonical."<<endl;
     SI::dumpInfo(target_loop);
     return false;
   }
   ROSE_ASSERT(ivar&& lb && ub && step);

  // Add a controlling loop around the top loop nest
  // Ensure the parent can hold more than one children
  SgLocatedNode* parent = NULL; //SI::ensureBasicBlockAsParent(loopNest)
  if (SI::isBodyStatement(loopNest)) // if it is a single body statement (Already a for statement, not a basic block)
   parent = SI::makeSingleStatementBodyToBlock (loopNest);
  else
    parent = isSgLocatedNode(loopNest ->get_parent());

  ROSE_ASSERT(parent!= NULL);

  SgScopeStatement* scope = loopNest->get_scope();

  SgFunctionDefinition* funcDef = SI::getEnclosingFunctionDefinition(loopNest);
  ROSE_ASSERT(funcDef!=NULL);
  SgBasicBlock* funcBody = funcDef->get_body();
  ROSE_ASSERT(funcBody!=NULL);

  // Now we can prepend a controlling loop index variable: __lt_var_originalIndex
  string ivar2_name = findAvailVarName("lt_var_"+ivar->get_name().getString()+"_", funcBody);
  
  SgVariableDeclaration* ndecl = SB::buildVariableDeclaration(ivar2_name,ivar->get_type(), NULL, funcBody);

  //It needs to be after the Implicit None statement.
  // if there is a SgImplicitStatement, put it afterwards.
  SgStatement * insert_target =  SI::findLastDeclarationStatement(funcBody->get_scope(), false);
  if (insert_target) {
     insert_target = SI::getNextStatement(insert_target); 
  } else {
     insert_target = SI::getFirstStatement(funcBody->get_scope());
  }
  SI::insertStatementBefore(insert_target, ndecl);

// Creating lower bound and upper bound using a formula for stripmine.

  int tile_offset = 0;

// lower bound
  SgExprListExp * lb_cl_arg_list = SB::buildExprListExp();
  SI::appendExpression(lb_cl_arg_list,
                  SB::buildDivideOp(
	//Cast the operation to real, otherwise the FLOOR wont compile	
	            SB::buildFunctionCallExp("REAL", SgTypeFloat::createType(),
		      SB::buildExprListExp(
                        SB::buildSubtractOp(
                          SI::copyExpression(lb),
                          SB::buildIntVal(tile_offset))),
		      scope), 
                    SB::buildIntVal(blockSize))); 

  SgExpression* lb_cl = SB::buildAddOp(
                          SB::buildMultiplyOp(
                            SB::buildFunctionCallExp("FLOOR", SgTypeInt::createType(),lb_cl_arg_list, scope ),                            SB::buildIntVal(blockSize)), 
                          SB::buildIntVal(tile_offset)); 

   SgExpression* initialization =  SB::buildAssignOp(SB::buildVarRefExp(ivar2_name,scope), lb_cl);

// upper bound

  SgExprListExp * ub_cl_arg_list = SB::buildExprListExp();

//  ub_cl_arg_list->append_expression(
  SI::appendExpression(ub_cl_arg_list,
                    SB::buildDivideOp(
		      SB::buildFunctionCallExp("REAL", SgTypeFloat::createType(),    
			SB::buildExprListExp(
                          SB::buildSubtractOp(
                            SI::copyExpression(ub),
                            SB::buildIntVal(tile_offset))), 
			scope),
                        SB::buildIntVal(blockSize))); 
  SgExpression* bound = SB::buildAddOp(
                          SB::buildMultiplyOp(
                            SB::buildFunctionCallExp("FLOOR", SgTypeInt::createType(),ub_cl_arg_list, scope ),                            SB::buildIntVal(blockSize)), 
                          SB::buildIntVal(tile_offset)); 
////

   // SB::build loop incremental  I
   // expression var+=up*tilesize or var-=upper * tilesize
   SgExpression* incr_exp = NULL;
   SgExpression* orig_incr_exp = target_loop->get_increment();

   incr_exp = SB::buildIntVal(blockSize);

  SgFortranDo* control_loop = buildFortranDo(initialization, bound,incr_exp, SB::buildBasicBlock());
  //REMOVE SI::insertStatementBefore(loopNest, control_loop);

  //Inserting before the loop created.
  SI::insertStatementBefore(target_control_loop, control_loop);
  // move loopNest into the control loop
  SI::removeStatement(target_control_loop);
  SI::appendStatement(target_control_loop,isSgBasicBlock(control_loop->get_body()));


  // rewrite the lower (i=lb), upper bounds (i<=/>= ub) of the target loop
  SgAssignOp* assign_op  = isSgAssignOp(lb->get_parent());
  ROSE_ASSERT(assign_op);
  
  SgExprListExp* lb_e_list = SB::buildExprListExp();
  SI::appendExpression(lb_e_list, SB::buildVarRefExp(ivar2_name,scope) );
  SI::appendExpression(lb_e_list, SI::copyExpression(assign_op->get_rhs_operand()) );
  assign_op->set_rhs_operand(
                            SB::buildFunctionCallExp("MAX", SgTypeInt::createType(),lb_e_list , scope )); 

  SgExprListExp* ub_e_list = SB::buildExprListExp();
  SI::appendExpression(ub_e_list, SI::copyExpression(SI::copyExpression(ub)) );
  SI::appendExpression(ub_e_list,
               SB::buildSubtractOp(
                  SB::buildAddOp(   
                    SB::buildVarRefExp(ivar2_name,scope),
                    SB::buildIntVal(blockSize)),
                  SB::buildIntVal(1)));
   target_loop->set_bound(SB::buildFunctionCallExp("MIN", SgTypeInt::createType(), ub_e_list , scope )); 
   SI::deepDelete(ub);

  // constant folding
  // folding entire loop may decrease the accuracy of floating point calculation
  // we fold loop control expressions only
  //constantFolding(control_loop->get_scope());
  SI::constantFolding(control_loop->get_bound());
  SI::constantFolding(control_loop->get_increment());
  return true;
}

bool ICE::SageInterfaceUIUC::jamming(SgBasicBlock *& body, SgBasicBlock * orig_body, SgSymbol *varsym,  
        size_t unroll_factor, const bool enable_debug) 
{
     // copy body to loop body, this should be a better choice
     // to avoid redefinition of variables after unrollandjaming (new scope is introduced to avoid this)
     //
     // Thiago 10/19/2017 Jam algorithm.
     // Assuming that we only add the SgExprStatments unrolled to BasicBlocks. 
     // Go through the basic blocks. And compare the two ASTs. Ast one  is a copy of the original (this is case is a copy of the fringe loop)
     // but with the variables changed accordingly to the unroll factor.
     // Ast two is the one that is being added the new statements copies. They must have 
     // the same number of basic blocks. As both are traversed the new statements are added to Ast 2. 
     //
     typedef Rose_STL_Container<SgNode *> NodeList_t;
     NodeList_t raw_list_fring = NodeQuery::querySubTree (body, V_SgBasicBlock);
     NodeList_t raw_list_orig = NodeQuery::querySubTree (orig_body, V_SgBasicBlock);
     //cout << "raw_list fring " << raw_list_fring.size() << " raw_list_orig "  << raw_list_fring.size() << endl;
     for (NodeList_t::iterator it = raw_list_fring.begin(),  it_o = raw_list_orig.begin();
             it != raw_list_fring.end() && it_o != raw_list_orig.end(); ++it , ++it_o)
     {

         SgBasicBlock * bb = isSgBasicBlock(*it);
         SgBasicBlock * bb_o = isSgBasicBlock(*it_o);
         if(enable_debug) {
            cout << "== factor " << unroll_factor << " " << (*it)->class_name() << " " << (*it_o)->class_name() << endl;
         }
         //SgStatementPtrList & stmts = bb->get_statements();    
         const SgStatementPtrList & stmts = bb->generateStatementList();//this generates a copy of the list and do not change from/to any change on the AST. If I add statement to the scope it does show up on this list.
         const SgStatementPtrList & stmts_out = bb_o->generateStatementList();    
/*
         for(SgStatementPtrList::const_iterator st_it = stmts.begin(); st_it != stmts.end(); st_it++) {
                cout << " ast in " << *st_it << " "<< (*st_it)->class_name() << " " << endl;
         }
             
         for(SgStatementPtrList::const_iterator st_it = stmts_out.begin(); st_it != stmts_out.end(); st_it++) {
                cout << " ast out " << *st_it << " "<< (*st_it)->class_name() << " " << endl;
         }
        SgStatement * last_out = bb_o->lastStatement();
        cout << " last out " << last_out << " "<< last_out->class_name() << " " << endl;
*/
         SgStatementPtrList::const_iterator st_it_out = stmts_out.begin(); 

         for(SgStatementPtrList::const_iterator st_it = stmts.begin(); st_it != stmts.end(); st_it++) {

             if(st_it_out == stmts_out.end())
             {     		
                cerr<<"Error in loopUnrollAndJaming, the corresponding stmt on the dest AST does not exist. Something is weird!"<<endl;
		return false;
             }

             //try find reference to loop ind in the statment. If not find not reproduce it.
             bool found_ivar = foundVar((*st_it), varsym );

             if(isSgExprStatement(*st_it) && found_ivar) {
                //cout << " copy out " << *st_it << " "<< (*st_it)->class_name() << " " << endl;
                 st_it_out += unroll_factor;
                 SI::removeStatement(isSgStatement(*st_it));
                 SI::insertStatementAfter(*(st_it_out-1), *st_it);
             }
             else 
             {
                 st_it_out++; 
             }
             if(enable_debug) {
                    cout << (*st_it)->class_name()  << " end list" << endl;
             }
         }
     }

     SI::deepDelete(body);
     return true;
}

bool ICE::SageInterfaceUIUC::addScopPragma(SgPragmaDeclaration* prag, SgPragmaDeclaration*& newpragbefor, 
		SgPragmaDeclaration*& newpragmafter, SgForStatement * target_loop)
{
	// Create a new pragma scop
	//
	newpragbefor = SI::deepCopy<SgPragmaDeclaration>(prag);
	SI::deepDelete(newpragbefor->get_pragma());
	SgPragma * newpr = new SgPragma("scop");
	newpragbefor->set_pragma(newpr);
	SI::replaceStatement(prag,  newpragbefor);

	// Create a new pragma endscop
	newpragmafter = SB::buildPragmaDeclaration("endscop", prag->get_scope());
	SI::insertStatementAfter(target_loop, newpragmafter);
	
	return true;
}

bool ICE::SageInterfaceUIUC::undoScopPragma(SgPragmaDeclaration* prag, SgPragmaDeclaration*& newpragbefor, 
		SgPragmaDeclaration*& newpragmafter)
{
	SI::replaceStatement(newpragbefor, prag);
	SI::removeStatement(newpragmafter);
	SI::deepDelete(newpragbefor);
	SI::deepDelete(newpragmafter);
	return true;
}

//used by unroll and unrollandjam for C/C++
bool ICE::SageInterfaceUIUC::loopUnrollAndJaming(SgForStatement* target_loop, 
        size_t unrollandjaming_factor, 
        SgPragmaDeclaration * prag,
        const bool jam,
        bool& needFringe, const bool enable_debug)
{
  //Handle 0 and 1, which means no unrolling at all
  if (unrollandjaming_factor <= 1)
    return true;
  // normalize the target loop first
  if (!SI::forLoopNormalization(target_loop));
  {// the return value is not reliable
    //    cerr<<"Error in SageInterface::loopUnrolling(): target loop cannot be normalized."<<endl;
    //    dumpInfo(target_loop);
    //    return false;
  }
  // grab the target loop's essential header information
  SgInitializedName* ivar = NULL;
  SgExpression* lb = NULL;
  SgExpression* ub = NULL;
  SgExpression* step = NULL;
  SgStatement* orig_body = NULL;
  if (!SI::isCanonicalForLoop(target_loop, &ivar, &lb, &ub, &step, &orig_body))
  {
    cerr<<"Error in SageInterface::loopUnrolling(): target loop is not canonical."<<endl;
    SI::dumpInfo(target_loop);
    return false;
  }
  ROSE_ASSERT(ivar&& lb && ub && step);
  ROSE_ASSERT(isSgBasicBlock(orig_body));

   //TODO if the parent of the target loop is pragma

   // generate the fringe loop
   //bool needFringe = true;
   SgForStatement* fringe_loop = SI::deepCopy<SgForStatement>(target_loop);
   SgBinaryOp* ub_bin_op = isSgBinaryOp(ub->get_parent());
   ROSE_ASSERT(ub_bin_op);
   SgScopeStatement* scope = target_loop->get_scope();
   ROSE_ASSERT(scope != NULL);

   if(needFringe) {
       SI::insertStatementAfter(target_loop,fringe_loop);
       SI::removeStatement(fringe_loop->get_for_init_stmt());
       fringe_loop->set_for_init_stmt(NULL);

       // _lu_iter_count = (ub-lb+1)%step ==0?(ub-lb+1)/step: (ub-lb+1)/step+1;
       SgExpression* raw_range_exp = SB::buildSubtractOp( SB::buildAddOp(SI::copyExpression(ub),SB::buildIntVal(1)),
               SI::copyExpression(lb));
       raw_range_exp->set_need_paren(true);
       SgExpression* range_d_step_exp = SB::buildDivideOp(raw_range_exp,SI::copyExpression(step));//(ub-lb+1)/step
       SgExpression* condition_1 = SB::buildEqualityOp(SB::buildModOp(SI::copyExpression(raw_range_exp),SI::copyExpression(step)),SB::buildIntVal(0)); //(ub-lb+1)%step ==0

       SgExpression* iter_count_exp = SB::buildConditionalExp(condition_1,range_d_step_exp, SB::buildAddOp(SI::copyExpression(range_d_step_exp),SB::buildIntVal(1)));
       // fringe = iteration_count%unrollandjam_factor==0 ? 0:unrollandjam_factor*step
       SgExpression* condition_2 = SB::buildEqualityOp(SB::buildModOp(iter_count_exp, SB::buildIntVal(unrollandjaming_factor)), SB::buildIntVal(0));
       SgExpression* initor = SB::buildConditionalExp(condition_2, SB::buildIntVal(0), SB::buildMultiplyOp(SB::buildIntVal(unrollandjaming_factor),SI::copyExpression(step)));

       SgFunctionDefinition* funcDef = SI::getEnclosingFunctionDefinition(target_loop);
       ROSE_ASSERT(funcDef);
       SgBasicBlock* funcBody = funcDef->get_body();
       ROSE_ASSERT(funcBody);

       string fringe_name = findAvailVarName("_lu_fringe_"+ivar->get_name().getString()+"_", funcBody);
       SgVariableDeclaration* fringe_decl = SB::buildVariableDeclaration(fringe_name, SB::buildIntType(), NULL, scope);
       SI::insertStatementBeforeFirstNonDeclaration(fringe_decl, funcBody->get_scope());

        SgExprStatement* fringe_assign = SB::buildAssignStatement(
			       SB::buildVarRefExp(fringe_name, scope),
                               //SB::buildAssignInitializer(initor)
                               initor
        		       );

       // compile-time evaluate to see if index is a constant of value 0
       // if so, the iteration count can be divided even by the unrollandjaming factor
       // and no fringe loop is needed
       // WE have to fold on its parent node to get a possible constant since
       // constant folding only folds children nodes, not the current node to a constant
       //ConstantFolding::constantFoldingOptimization(fringe_decl,false);
       ConstantFolding::constantFoldingOptimization(fringe_assign,false);
       SgAssignOp* fold_assign = isSgAssignOp(fringe_assign->get_expression());
       if (fold_assign)
           if (isSgIntVal(fold_assign->get_rhs_operand()))
               if (isSgIntVal(fold_assign->get_rhs_operand())->get_value() == 0){
                   needFringe = false;
                   SI::removeStatement(fringe_decl);
                   if(enable_debug)
                   {
                       cout << "ConstantFolding removed the need for a fringe." << endl;
                   }
               }
       if(needFringe) //check it again after constant folding
       {
         // rewrite loop header ub --> ub -fringe; step --> step *unrollandjaming_factor
        ub_bin_op->set_rhs_operand(SB::buildSubtractOp(SI::copyExpression(ub),SB::buildVarRefExp(fringe_name,scope)));
        SI::attachComment(fringe_decl, "iter_count = (ub-lb+1)%step ==0?(ub-lb+1)/step: (ub-lb+1)/step+1;");
        SI::attachComment(fringe_decl, "fringe = iter_count%unrollandjam_factor==0 ? 0:unrollandjam_factor*step");


        if (prag != NULL) {
                SI::insertStatementBefore(prag, fringe_assign);
        } else {
                SI::insertStatementBefore(target_loop, fringe_assign);
        }
                      
       }
   }   
   else //no fringe
   {
     ub_bin_op->set_rhs_operand(SI::copyExpression(ub));
//     SI::removeStatement(fringe_decl);
   }

   SgBinaryOp* step_bin_op = isSgBinaryOp(step->get_parent());
   ROSE_ASSERT(step_bin_op != NULL);
   step_bin_op->set_rhs_operand(SB::buildMultiplyOp(SI::copyExpression(step),SB::buildIntVal(unrollandjaming_factor)));

   bool isPlus = false;
   if (isSgPlusAssignOp(step_bin_op))
     isPlus = true;
    else if (isSgMinusAssignOp(step_bin_op))
      isPlus = false;
    else
    {
      cerr<<"Error in SageInterface::loopUnrolling(): illegal incremental exp of a canonical loop"<<endl;
      SI::dumpInfo(step_bin_op);
      ROSE_ASSERT(false);
    }

   // copy loop body factor -1 times, and replace reference to ivar  with ivar +/- step*[1 to factor-1]
   for (size_t i =1; i<unrollandjaming_factor; i++)
   {
     SgBasicBlock* body = isSgBasicBlock(SI::deepCopy(fringe_loop->get_loop_body())); // normalized loop has a BB body
     ROSE_ASSERT(body);
     std::vector<SgVarRefExp*> refs = SI::querySubTree<SgVarRefExp> (body, V_SgVarRefExp);
     for (std::vector<SgVarRefExp*>::iterator iter = refs.begin(); iter !=refs.end(); iter++)
     {
       SgVarRefExp* refexp = *iter;
       if (refexp->get_symbol()==ivar->get_symbol_from_symbol_table())
       {
         // replace reference to ivar with ivar +/- step*i
         SgExpression* new_exp = NULL;
         //build replacement  expression for every appearance 
         if (isPlus) //ivar +/- step * i
         new_exp = SB::buildAddOp(SB::buildVarRefExp(ivar,scope),SB::buildMultiplyOp(SI::copyExpression(step),SB::buildIntVal(i)));
         else
         new_exp = SB::buildSubtractOp(SB::buildVarRefExp(ivar,scope),SB::buildMultiplyOp(SI::copyExpression(step),SB::buildIntVal(i)));

         // replace it with the right one
         SI::replaceExpression(refexp, new_exp);
       }
     }
     // copy body to loop body, this should be a better choice
     // to avoid redefinition of variables after unrolling (new scope is introduced to avoid this)
     if(jam) {//used for 

        jamming(body, isSgBasicBlock(orig_body), ivar->get_symbol_from_symbol_table(), i, enable_debug); 

     } else {

         const SgStatementPtrList & stmts = body->generateStatementList();//this generates a copy of the list and do not change from/to any change on the AST. If I add statement to the scope it does show up on this list.
         for(SgStatementPtrList::const_iterator st_it = stmts.begin(); st_it != stmts.end(); st_it++) {
             //cout << i << " type of stmt " << (*st_it)->class_name() << endl;
               
             bool found_ivar = foundVar((*st_it), ivar->get_symbol_from_symbol_table());
             //try find reference to loop ind in the statment. If not find not reproduce it.
/*             std::vector<SgVarRefExp*> refs = SI::querySubTree<SgVarRefExp> ((*st_it), V_SgVarRefExp);
             for (std::vector<SgVarRefExp*>::iterator iter = refs.begin(); iter !=refs.end(); iter++)
             {
                 SgVarRefExp* refexp = *iter;
                 if (refexp->get_symbol()==ivar->get_symbol_from_symbol_table()) {
                       found_ivar = true;
                        cout << i<<" Found var in the stmt" << endl;
                       break;
                 }
             }
*/
             if(found_ivar) {
                 SgStatement * last_out = isSgScopeStatement(orig_body)->lastStatement();
                 ROSE_ASSERT(last_out);
                 SI::removeStatement(isSgStatement(*st_it));
                 SI::insertStatementAfter(last_out, *st_it);
             }
         }
     }
   }

   // remove the fringe loop if not needed finally
   // it is used to buffering the original loop body before in either cases
   if (!needFringe)
     SI::removeStatement(fringe_loop);

   // constant folding for the transformed AST
   ConstantFolding::constantFoldingOptimization(scope,false);
   //ConstantFolding::constantFoldingOptimization(getProject(),false);

  return true;
}

bool ICE::SageInterfaceUIUC::foundVar(SgNode * node, SgSymbol * varsym)
{
    //try find reference to loop ind in the statment. If not find not reproduce it.
    std::vector<SgVarRefExp*> refs = SI::querySubTree<SgVarRefExp> (node, V_SgVarRefExp);
    for (std::vector<SgVarRefExp*>::iterator iter = refs.begin(); iter !=refs.end(); iter++)
    {
        SgVarRefExp* refexp = *iter;
        if (refexp->get_symbol()==varsym) {
            cout <<" Found var in the stmt" << endl;
            return true;
        }
    }
    return false;
}

//Try find a name that is not a variabel already existing in parent scopes.
string ICE::SageInterfaceUIUC::findAvailVarName(const string& var_base_name, SgScopeStatement * cscope)
{
    bool found_same_name = true;
    string final_name = var_base_name;

//    int tmp_counter = ++SI::gensym_counter;

    while(found_same_name)
    {
        final_name = var_base_name + Rose::StringUtility::numberToString(++SI::gensym_counter);
        if(SI::lookupVariableSymbolInParentScopes(SgName(final_name), cscope) == NULL)
        {
            // Found no variable with same name in parent scopes.
            found_same_name = false;
            if (SgProject::get_verbose() >= 1)
            {
                    cout << "[SUIC/findAvailVarName] "<<final_name<<" IS available." <<endl;
            }
        } else {
            if (SgProject::get_verbose() >= 1)
            {
                    cout << "[SUIC/findAvailVarName] "<<final_name<<" NOT available." <<endl;
            }
        }
    }
    
    return final_name;
}

int ICE::SageInterfaceUIUC::getForLoopTripCount(SgForStatement *loop) {

    SgVariableSymbol *for_ierator;
    SgExpression * lower_bound;
    SgExpression * upper_bound;
    SgExpression * stride;
    bool success = SageInterface::getForLoopInformations(loop, for_ierator, lower_bound, upper_bound, stride);
    if (success && for_ierator && lower_bound && upper_bound && stride) {
        if (!for_ierator->get_type()->stripType()->isIntegerType()
            || !lower_bound->get_type()->stripType()->isIntegerType()
            || !upper_bound->get_type()->stripType()->isIntegerType()
            || !stride->get_type()->stripType()->isIntegerType()
            ) {
            return -1;
        }

        // evaluateConstIntegerExpression has bug. it sometimes returns 0 when the result is not
        SageInterface::const_int_expr_t lb = SageInterface::evaluateConstIntegerExpression(lower_bound);
        SageInterface::const_int_expr_t ub = SageInterface::evaluateConstIntegerExpression(upper_bound);
        SageInterface::const_int_expr_t st = SageInterface::evaluateConstIntegerExpression(stride);

        if (lb.hasValue_ && ub.hasValue_ && st.hasValue_) {

            int trip = ceil(static_cast<float>(static_cast<int>(ub.value_) + 1 - static_cast<int>(lb.value_))/static_cast<int>(st.value_));
            //cout << (int) lb.value_ << " " << (int) ub.value_ << " " << (int) st.value_ << " " << trip << endl;

            return trip;

        }

        return -1;

    }

    return -1;
}

SgNode* ICE::SageInterfaceUIUC::leastCommonAncestor(SgNode* a, SgNode* b) {
	// Find the closest node which is an ancestor of both a and b
	vector<SgNode*> ancestorsOfA;
	for (SgNode* p = a; p; p = p->get_parent()) ancestorsOfA.push_back(p);
	while (b) {
		vector<SgNode*>::const_iterator i = std::find(ancestorsOfA.begin(), ancestorsOfA.end(), b);
		if (i != ancestorsOfA.end()) return *i;
		b = b->get_parent();
	}
	return NULL;
}

