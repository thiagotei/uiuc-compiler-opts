#ifndef SAGEINTERFACE_UIUC
#define SAGEINTERFACE_UIUC

#include <sageBuilder.h>
#include <sage3basic.h>
#include <iostream>
#include <constantFolding.h>
#include <string>

using namespace std;

namespace SB = SageBuilder;
namespace SI = SageInterface;

namespace ICE
{
	namespace SageInterfaceUIUC
	{
		SgFortranDo* buildFortranDo(SgExpression* initialization, SgExpression* bound, SgExpression* increment, SgBasicBlock* loop_body, bool hasEndStatement=true);
		bool loopBlocking(SgForStatement* loopNest, size_t targetLevel, size_t blockSize, size_t controlLoopPos);
		bool loopBlocking(SgFortranDo* loopNest, size_t targetLevel, size_t blockSize, size_t controlLoopPos);
                string findAvailVarName(const string& var_base_name, SgScopeStatement * cscope);
                bool loopUnrollAndJaming(SgForStatement* target_loop, size_t unrollandjaming_factor, SgPragmaDeclaration * prag, const bool jam, bool& needFringe, const bool enable_debug);
                bool jamming(SgBasicBlock *& body, SgBasicBlock * orig_body, SgSymbol * varsym, size_t unroll_factor, const bool enable_debug);
                bool foundVar(SgNode * node, SgSymbol * varsym);
		bool addScopPragma(SgPragmaDeclaration* prag, SgPragmaDeclaration*& newpragbefor,
					SgPragmaDeclaration*& newpragmafter, SgForStatement * loop);
		bool undoScopPragma(SgPragmaDeclaration* prag, SgPragmaDeclaration*& newpragbefor,
					SgPragmaDeclaration*& newpragmafter);
		int getForLoopTripCount(SgForStatement *loop);
		SgNode* leastCommonAncestor(SgNode* a, SgNode* b);
	}
}


#endif

