
#ifndef INTERCHANGE_UIUC
#define INTERCHANGE_UIUC

#include <cstdlib>
#include <sage3basic.h>
#include <Cxx_Grammar.h>
#include <sageBuilder.h>
#include <string>
#include <vector>
#include <AnnotParser.hh>
#include <Result.hh>
#include <Misc.hh>
#include <dependence.hh>
#include <SageInterfaceUIUC.hh>

using namespace std;
using namespace restructurer;

namespace SI = SageInterface;
namespace SIUI = ICE::SageInterfaceUIUC;

namespace ICE
{
	namespace LoreDepAvailable 
	{
		bool enable_debug = false;
		static const std::string PRAGMA_NAME("uiuc_loredepavailable");
		//Delimiter to the new order string in the annotation  
		static const std::string PRAGMA_DELIMITER(",");

 		void commandLineProcessing(std::vector<std::string> &argvList); 
		Result depavailable(SgPragmaDeclaration* prag);
		int depavailableAll(SgProject * project);
	}
}	

#endif
