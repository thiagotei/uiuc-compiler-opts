#include "LoreDepAvailable.hh"


int main(int argc, char ** argv)
{
	vector<string> argvList(argv, argv+argc);
	ICE::LoreDepAvailable::commandLineProcessing(argvList);	
	
	SgProject * proj = frontend(argvList);
	ROSE_ASSERT(proj);

	cout << "[ICE/LoreDepAvailable]..." << endl;
	int res = ICE::LoreDepAvailable::depavailableAll(proj);

	//proj->unparse();
	cout << "[ICE/LoreDepAvailable] end, Res " << res  << endl;

	return res;
}

int ICE::LoreDepAvailable::depavailableAll(SgProject * project) 
{
	size_t num_tiled = 0;
	int res = 0;
	// Search for pragmas for C/C++
	PragmaList_t pragmas;
	if(AnnotParser::collectPragmas(project, pragmas, PRAGMA_NAME))
	{
		for (PragmaList_t::iterator i = pragmas.begin(); i != pragmas.end(); ++i) {
			ICE::Result resopt = depavailable(*i);
			if(resopt.isValid()) {
				++num_tiled;
			} else {
				res = resopt.getValue();
				break;
			}
		}
	}
	return res;
}

//Tile the n-level (starting from 1)
ICE::Result ICE::LoreDepAvailable::depavailable(SgPragmaDeclaration* prag) 
{
//	SageInterface::dumpInfo(s);
	vector<int> pragmainfo;
	SgStatement* s = AnnotParser::processPragmaInfo (prag, PRAGMA_NAME, pragmainfo);
	if(!s) return Result(1);	

	if(enable_debug) {
		cout << "pragma info: ";
		for(vector<int>::iterator it = pragmainfo.begin(); it != pragmainfo.end(); ++it){
			cout << *it << " ";
		}	
		cout << endl;
	}

	SgPragmaDeclaration *newpragbefor, *newpragmafter;
	SIUI::addScopPragma(prag, newpragbefor, newpragmafter, isSgForStatement(s));
	cout << "Added Scop pragmas" << endl;
	
	DependenceGraph orig_dep_graph;
	orig_dep_graph.printDepSet(cout);

	dep_set_t orig_dep_set = orig_dep_graph.getDepSet();

	SgBasicBlock *root = isSgBasicBlock(orig_dep_graph.getRoot());
	SIUI::undoScopPragma(prag, newpragbefor, newpragmafter);
	if (root == NULL) {
		cout << "No proper SCoP is found in the input file." << endl;
		return Result(1);
	} else {
		return Result(0);
	}

}


void ICE::LoreDepAvailable::commandLineProcessing(std::vector<std::string> &argvList) {

  if (CommandlineProcessing::isOption (argvList,"-rose:loredepavailable:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for depavailable functions..."<<endl;
    enable_debug= true;
  }

/*  if (CommandlineProcessing::isOptionWithParameter(argvList,"-rose:depavailable:","target_level", target_level, true))
  {	
    cout<<"Target level is " << target_level << endl;	
  }

  if (CommandlineProcessing::isOptionWithParameter (argvList,"-rose:depavailable:","tile_size", tile_size, true))
  {	
    cout<<"Tile size is " << tile_size << endl;	
  }
*/

  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC LoreUnrollAndJam-specific options"<<endl;
    cout<<"Usage: LoreDepAvailable [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
    cout<<"\t-rose:loredepavailable:enable_debug                   run in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }
}

