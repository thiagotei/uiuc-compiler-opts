#include "LoreInterchange.hh"


int main(int argc, char ** argv)
{
	vector<string> argvList(argv, argv+argc);
	ICE::LoreInterchangeUIUC::commandLineProcessing(argvList);	
	
	SgProject * proj = frontend(argvList);
	ROSE_ASSERT(proj);

	cout << "[ICE/LoreInterchangeUIUC]..." << endl;
	int res = ICE::LoreInterchangeUIUC::interchangeAll(proj);

	//TODO specify the output name.
	proj->unparse();
	cout << "[ICE/LoreInterchangeUIUC] end" << endl;

	return res;
}

int ICE::LoreInterchangeUIUC::interchangeAll(SgProject * project) 
{
	size_t num_tiled = 0;	
	int res = 0;
	// Search for pragmas for C/C++
	PragmaList_t pragmas;
	if(AnnotParser::collectPragmas(project, pragmas, PRAGMA_NAME))
	{
		for (PragmaList_t::iterator i = pragmas.begin(); i != pragmas.end(); ++i) {
			ICE::Result resopt = interchange(*i);
			if(resopt.isValid()) {
				++num_tiled;
			} else {
				res = resopt.getValue();
				break;
			}
		}

	}
	return res;
}

ICE::Result ICE::LoreInterchangeUIUC::interchange(SgPragmaDeclaration* prag) 
{
//	SageInterface::dumpInfo(s);
	vector<string> pragmainfo;
	SgStatement* s = AnnotParser::processPragmaInfo (prag, PRAGMA_NAME, pragmainfo);
	if(!s) return Result(1);	

	// Expects two infos to do the tiling.
	if(pragmainfo.size() != 1) {
     		cerr<<"Error in loopLoreInterchange, not proper info on the pragma."<<endl;
		return Result(1);
	}

	if(enable_debug) {
		cout << "pragma info: ";
		for(vector<string>::iterator it = pragmainfo.begin(); it != pragmainfo.end(); ++it){
			cout << *it << " ";
		}	
		cout << endl;
	}

	std::vector<size_t> neworder = parseStringToNum<size_t>(pragmainfo[0], PRAGMA_DELIMITER);
	size_t neworder_lexirank = getLexicoRank(neworder);;
	cout << "Lexico rank " << neworder_lexirank << endl;

	//Needs to add the scop pragma around the loop
	SgPragmaDeclaration *newpragbefor, *newpragmafter;
	SIUI::addScopPragma(prag, newpragbefor, newpragmafter, isSgForStatement(s));
	cout << "Added Scop pragmas" << endl;
	
	DependenceGraph orig_dep_graph;
	orig_dep_graph.printDepSet(cout);

	dep_set_t orig_dep_set = orig_dep_graph.getDepSet();

	SgBasicBlock *root = isSgBasicBlock(orig_dep_graph.getRoot());
	SIUI::undoScopPragma(prag, newpragbefor, newpragmafter);
	if (root == NULL) {
		cout << "Interchange ABORTED! No proper SCoP is found in the input file." << endl;
		return Result(1);
	}

	bool legal = orig_dep_graph.isPermutationLegal(neworder);
	cout << "Legality: " << legal << endl;
	if(legal) {
		cout << "Legal!" << endl;
		return interchange(s, neworder.size(), neworder_lexirank);
	} else {
		cout <<  "NOT legal!" << endl;
		return Result(2);
	}
}

template <typename T>
ICE::Result ICE::LoreInterchangeUIUC::interchange(SgStatement* s,  size_t depth, T neworder) 
{
	bool r;
//	SageInterface::dumpInfo(s);

	if (SgForStatement * for_loop = isSgForStatement(s)) {
		cout << "[ICE/LoreInterchangeUIUC/interchange] For loop" << endl;
		r = SI::loopInterchange(for_loop, depth, neworder);		
		//r = loopLoreInterchange(for_loop, depth, neworder);		
	} else {
		cerr << "[ICE/LoreInterchangeUIUC/interchange] ERROR! SgStatement not a For neither a Do loop!" << endl;	
		r = false;
	}
	int ret = 1;
	if (r) { ret = 0;}
	else {ret = 1;}

	return Result(ret);
}

void ICE::LoreInterchangeUIUC::commandLineProcessing(std::vector<std::string> &argvList) {

  if (CommandlineProcessing::isOption (argvList,"-rose:interchange:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for interchange functions..."<<endl;
    enable_debug= true;
  }

/*  if (CommandlineProcessing::isOptionWithParameter(argvList,"-rose:tiling:","target_level", target_level, true))
  {	
    cout<<"Target level is " << target_level << endl;	
  }

  if (CommandlineProcessing::isOptionWithParameter (argvList,"-rose:tiling:","tile_size", tile_size, true))
  {	
    cout<<"Tile size is " << tile_size << endl;	
  }
*/

  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC LoreInterchange-specific options"<<endl;
    cout<<"Usage: loopLoreInterchange [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
    cout<<"\t-rose:depinterchange:enable_debug                   run interchange in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }
}

/*
 * Receive a vector of numbers to be used as new order,
 * and checks if they comprised all the loops in the depth.
 * For instance, 2,0,1 are valid but 2,0,4 is not.
 * Return 0 if everything is correct, 1 otherwise. 
 */
template <typename N>
int ICE::LoreInterchangeUIUC::checkNewOrder(std::vector<N> neworder)
{
	std::sort(neworder.begin(), neworder.end());
	N inc = 0;
	int ret = 0;
	for(typename std::vector< N >::iterator it = neworder.begin(); it != neworder.end(); ++it) {
		if (*it != inc)
		{
			ret = 1;
			break;
		}	
		++inc;
	}

	return ret;
}

template int ICE::LoreInterchangeUIUC::checkNewOrder<size_t> (std::vector<size_t> neworder);

//template ICE::Result ICE::LoreInterchangeUIUC::interchange < const std::vector<size_t>& > (SgStatement* s, size_t depth, const std::vector<size_t> &neworder);

template ICE::Result ICE::LoreInterchangeUIUC::interchange < size_t > (SgStatement* s, size_t depth, size_t neworder);

