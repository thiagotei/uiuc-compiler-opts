#ifndef Misc_UIUC
#define Misc_UIUC

#include <cstdlib>
#include <cstdio>
#include <vector> 
#include <sstream>
#include <iostream>


namespace ICE
{

	size_t myfactorial(size_t n);
	size_t getLexicoRank(std::vector<size_t> input);
	std::vector<size_t> getPermutationOrder( size_t n, size_t lexicoOrder);
	template<typename N> std::vector<N> parseStringToNum(const std::string &str, const std::string &delimeter); 
	template<typename T> T stringToNumber(const std::string &text); 
}


#endif
