#include "AnnotParser.hh"

// =====================================================================

/*
 *  \brief Collects all outlining pragmas.
 *
 * This routine scans the given project for all outlining pragmas, and
 * returns them in the order in which they should be processed.
 *
 * The ordering is important because neither preorder nor postorder
 * tree traversals yield the desired bottomup processing for outlining
 * pragmas. To see why, consider the following code example:
 *
 * \code
 * #pragma rose_outline
 * {
 *   #pragma rose_outline
 *   statement1;
 * }
 * \endcode
 *
 * The corresponding AST is:
 *
 *   SgBasicBlock1
 *     /      \
 *    /        \
 * SgPragma1  SgBasicBlock2
 *              /      \
 *          SgPragma2  SgStatement1
 *
 * The standard traversal orders are:
 *
 * - Preorder: bb1, pragma1, bb2, pragma2, stmt1
 * - Postorder: pragma1, pragma2, stmt1,bb2, bb1
 *
 * In both cases, pragma1 is always visited before pragma2.
 *
 * The routine obtains a "correct" ordering by using the default
 * preorder AST query and then reversing the results.  In this we, we
 * obtain the ordering:
 *
 * - stmt1, pragma2, bb2,pragma1, bb1
 */
size_t
ICE::AnnotParser::collectPragmas (SgProject* proj, PragmaList_t& pragmas) 
{
  typedef Rose_STL_Container<SgNode *> NodeList_t;
  NodeList_t raw_list = NodeQuery::querySubTree (proj, V_SgPragmaDeclaration);
  size_t count = 0;
  for (NodeList_t::reverse_iterator i = raw_list.rbegin ();
       i != raw_list.rend (); ++i)
    {
      SgPragmaDeclaration* decl = isSgPragmaDeclaration (*i);
      if (processPragma (decl))
        {
          pragmas.push_back (decl);
          ++count;
        }
    }
  return count;
}

size_t
ICE::AnnotParser::collectPragmas (SgProject* proj, PragmaList_t& pragmas, const std::string& PRAGMA_NAME) 
{
  typedef Rose_STL_Container<SgNode *> NodeList_t;
  NodeList_t raw_list = NodeQuery::querySubTree (proj, V_SgPragmaDeclaration);
  size_t count = 0;
  for (NodeList_t::reverse_iterator i = raw_list.rbegin ();
       i != raw_list.rend (); ++i)
    {
      SgPragmaDeclaration* decl = isSgPragmaDeclaration (*i);
      if (processPragma (decl, PRAGMA_NAME))
        {
          pragmas.push_back (decl);
          ++count;
        }
    }
  return count;
}



template <typename T>
size_t ICE::AnnotParser::collectFortranTarget (SgProject* proj, TargetList_t& targets, const std::string& PRAGMA_NAME, std::vector< std::vector< T > > & pragmasinfo) 
{
  typedef Rose_STL_Container<SgNode *> NodeList_t;
  NodeList_t raw_list = NodeQuery::querySubTree (proj, V_SgStatement);
  size_t count = 0;
  for (NodeList_t::reverse_iterator i = raw_list.rbegin ();
      i != raw_list.rend (); ++i)
  {
    std::vector<T> pragmainfo;
    SgStatement* decl = isSgStatement(*i);
    if (processFortranComment(decl, PRAGMA_NAME, pragmainfo))
    {
      targets.push_back (decl);
      pragmasinfo.push_back(pragmainfo);
      ++count;
    }
  }
  return count;
}

SgStatement *
ICE::AnnotParser::processFortranComment(SgLocatedNode* node, const std::string& PRAGMA_NAME, std::vector<int>& pragmainfo)
{
	vector<string> pragmainfo_str;
	SgStatement* res = processFortranComment(node, PRAGMA_NAME, pragmainfo_str);

	for(vector<string>::iterator it = pragmainfo_str.begin(); it != pragmainfo_str.end(); it++){
		pragmainfo.push_back(stringToNumber<int>(*it));			
	}

	return res;
}

/*
 *  \brief Check whether the specified comment is an outlining
 *  directive for Fortran: one of the following three cases:
 *    !$rose_outline
 *    c$rose_outline
 *    *$rose_outline
 *  This routine checks whether the specified source comment an outlining
 *  directive, and if so, returns the statement that should be
 *  outlined. Returns NULL if the comment is not an outlining directive
 *  or no such statement exists.
 *  Liao, 12/19/2008
 */
SgStatement *
ICE::AnnotParser::processFortranComment(SgLocatedNode* node, const std::string& PRAGMA_NAME, std::vector<string>& pragmainfo)
{
  SgStatement* target = NULL;
  ROSE_ASSERT(node);
  AttachedPreprocessingInfoType *comments =
    node->getAttachedPreprocessingInfo ();
  if (comments==NULL)
    return 0;
  AttachedPreprocessingInfoType::iterator i;
  std::vector< PreprocessingInfo* > removeList;
  for (i = comments->begin (); i != comments->end (); i++)
  {
    if ((*i)->getTypeOfDirective() == PreprocessingInfo::FortranStyleComment)
    {
      string commentString = (*i)->getString();
/*      boost::algorithm::trim(commentString);
      if (   (commentString == "!$"+PRAGMA_NAME)
          || (commentString == "c$"+PRAGMA_NAME)
          || (commentString == "*$"+PRAGMA_NAME))
      {
        target = isSgStatement(node);
        if (target==NULL)
        {
          cerr<<"Unhandled case when a Fortran statement is attached to a non-statement node!!"<<endl;
          ROSE_ASSERT(false);
        }
        removeList.push_back(*i);
      }
*/
      boost::char_separator<char> sep(" ");
      tokenizer tok(commentString, sep);
 
      bool found_pragma = false;
      string tok_1 = *(tok.begin());
#ifdef DEBUG	
      cout << "Fortran comment: " ;     
      for(tokenizer::iterator it = tok.begin(); it != tok.end(); ++it) {
 	  cout << *it << " ";
      }
      cout << endl << "+++" << endl;
#endif
      // if there is an space between the comment indicator and name
      if(  tok_1 == "!$"
         ||tok_1 == "c$"
         ||tok_1 == "*$" ) { 
            
	   tokenizer::iterator itA = ++tok.begin();
	   if (*itA == PRAGMA_NAME) {
   	      for(tokenizer::iterator it = ++itA; it != tok.end(); ++it) {
	         //cout << *it << " blah" << endl;
	         pragmainfo.push_back(*it);
      	      }

              found_pragma = true;
           }
      } else if (  tok_1 == "!$"+PRAGMA_NAME
                 ||tok_1 == "c$"+PRAGMA_NAME
                 ||tok_1 == "*$"+PRAGMA_NAME) {
	   for(tokenizer::iterator it = ++tok.begin(); it != tok.end(); ++it) {
	        //cout << *it << " blah" << endl;
	        pragmainfo.push_back(*it);
      	    }
            found_pragma = true;
      }

      if (found_pragma) {
	 cout << "     Found pragma! " << endl;
         target = isSgStatement(node);
         if (target==NULL)
         {
            cerr<<"Unhandled case when a Fortran statement is attached to a non-statement node!!"<<endl;
            ROSE_ASSERT(false);
         }
         //removeList.push_back(*i);
      }

    } // end if Fortran comment
  } // end for-loop

  // remove those special comments
/*  for (std::vector<PreprocessingInfo* >::iterator j = removeList.begin();
       j!=removeList.end();j++)
  {
    comments->erase(find(comments->begin(), comments->end(),*j));
*/
 // DQ (2/27/2009): Don't mix free with C++ code (malloc/free for C and new/delete for C++)
 // free memory also
 // free(*j);
/*    delete(*j);
  }
*/
  return target; // const_cast<SgStatement *> (target);
}

/*
 *  \brief Check whether the specified pragma is an outlining
 *  directive.
 *
 *  This routine checks whether the specified pragma is an outlining
 *  directive, and if so, returns the statement that should be
 *  outlined. Returns NULL if the pragma is not an outlining directive
 *  or no such statement exists.
 */
SgStatement *
ICE::AnnotParser::processPragma (SgPragmaDeclaration* decl) 
{
  if (!decl || !decl->get_pragma ())
    return 0;

  string pragmaString = decl->get_pragma ()->get_pragma ();
  if (pragmaString != PRAGMA_OUTLINE) // Not an outlining pragma.
    return 0;

  // Get statement to outline
  return const_cast<SgStatement *> (SageInterface::getNextStatement(decl));
  //return const_cast<SgStatement *> (ASTtools::findNextStatement (decl));
}


SgStatement *
ICE::AnnotParser::processPragma (SgPragmaDeclaration* decl, const std::string& PRAGMA_NAME) 
{
  if (!decl || !decl->get_pragma ())
    return 0;

  string pragmaString = decl->get_pragma ()->get_pragma ();
  boost::char_separator<char> sep(" ");
  tokenizer tok(pragmaString, sep);
/*
  for(tokenizer::iterator it = tok.begin(); it != tok.end(); ++it ) {
	cout << *it << endl;
  }
 */
  if (*(tok.begin()) != PRAGMA_NAME) // Not an outlining pragma.
    return 0;

  //cout << "[processPragma] pragmaString FOUND!" << pragmaString << endl;

  // Get statement to outline
  return const_cast<SgStatement *> (SageInterface::getNextStatement(decl));
  //return const_cast<SgStatement *> (ASTtools::findNextStatement (decl));
}

/*
 * Converts pragma's info to integer. 
 *
 */ 
SgStatement *
ICE::AnnotParser::processPragmaInfo (SgPragmaDeclaration* decl, const std::string& PRAGMA_NAME, std::vector<int>& pragmainfo) 
{
	vector<string> pragmainfo_str; 
	SgStatement* res = processPragmaInfo(decl,PRAGMA_NAME, pragmainfo_str);

	for(vector<string>::iterator it = pragmainfo_str.begin(); it != pragmainfo_str.end(); ++it) {
		pragmainfo.push_back(stringToNumber<int>(*it));
	}

	return res;
}

/*
 * Return pragma info in an array of strings.
 *
 */ 
SgStatement *
ICE::AnnotParser::processPragmaInfo (SgPragmaDeclaration* decl, const std::string& PRAGMA_NAME, std::vector<string>& pragmainfo) 
{
  if (!decl || !decl->get_pragma ())
    return 0;

  string pragmaString = decl->get_pragma ()->get_pragma ();
  boost::char_separator<char> sep(" ");
  tokenizer tok(pragmaString, sep);

  if (*(tok.begin()) != PRAGMA_NAME) // Not an outlining pragma.
    return 0;

  //cout << "[processPragmaInfo] pragmaString FOUND! " << pragmaString << endl;

  for(tokenizer::iterator it = ++tok.begin(); it != tok.end(); ++it) {
	//cout << *it << " blah" << endl;
	pragmainfo.push_back(*it);
  }

  // Get statement to outline
  return const_cast<SgStatement *> (SageInterface::getNextStatement(decl));
  //return const_cast<SgStatement *> (ASTtools::findNextStatement (decl));
}


template size_t ICE::AnnotParser::collectFortranTarget<int>(SgProject * proj, TargetList_t & targets, const std::string& PRAGMA_NAME, std::vector< std::vector<int> > & pragmainfo);

template size_t ICE::AnnotParser::collectFortranTarget<string>(SgProject * proj, TargetList_t & targets, const std::string& PRAGMA_NAME, std::vector< std::vector<string> > & pragmainfo);


