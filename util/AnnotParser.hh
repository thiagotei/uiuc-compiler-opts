
#ifndef AnnotParser_UIUC
#define AnnotParser_UIUC

#include <sage3basic.h>
#include <Cxx_Grammar.h> 
#include <boost/algorithm/string/trim.hpp>
#include <boost/tokenizer.hpp>
#include "Misc.hh"

using namespace std;

namespace ICE
{
	static const std::string PRAGMA_OUTLINE ("rose_outline");
	//! Stores a list of valid outlining pragma directives.
	typedef Rose_STL_Container<SgPragmaDeclaration *> PragmaList_t;
	//! Stores a list of valid outlining targets, used only for Fortran for now
	typedef Rose_STL_Container<SgStatement*> TargetList_t;

	class AnnotParser {
		private:

		public:
		AnnotParser() {}
		~AnnotParser() {}		
		AnnotParser(const AnnotParser&);
		
		/*Process Fortran pragmas*/				
		template<typename T> static size_t collectFortranTarget (SgProject * proj, TargetList_t & targets, const std::string& PRAGMA_NAME, std::vector< std::vector<T> > & pragmainfo) ;	
		static SgStatement * processFortranComment(SgLocatedNode* node, const std::string& PRAGMA_NAME, std::vector<string>& pragmainfo) ;
		static SgStatement * processFortranComment(SgLocatedNode* node, const std::string& PRAGMA_NAME, std::vector<int>& pragmainfo) ;

		/*Process C/C++ pragmas*/
		static size_t collectPragmas (SgProject* proj, PragmaList_t& pragmas) ;
		static size_t collectPragmas (SgProject* proj, PragmaList_t& pragmas, const std::string& PRAGMA_NAME) ;
		static SgStatement * processPragma (SgPragmaDeclaration* decl) ;
		static SgStatement * processPragma (SgPragmaDeclaration* decl, const std::string& PRAGMA_NAME);
		static SgStatement * processPragmaInfo (SgPragmaDeclaration* decl, const std::string& PRAGMA_NAME, std::vector<string>& pragmainfo);
		static SgStatement * processPragmaInfo (SgPragmaDeclaration* decl, const std::string& PRAGMA_NAME, std::vector<int>& pragmainfo);

		typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	};
}
#endif
