#include "Misc.hh"

// Liao, 6/15/2009
//! A helper function to calculate n!
//! See also, Combinatorics::factorial(), which also checks for overflow.
size_t ICE::myfactorial (size_t n)
{
  size_t result=1;
  for (size_t i=2; i<=n; i++)
    result*=i;
  return result;
}

size_t ICE::getLexicoRank(std::vector<size_t> input)
{
	size_t pos = input.size();
	size_t check = 0;
	for(size_t ind =0; ind < pos; ++ind) {
		if(input[ind] != check) {break;}
		++check;
	}
	//If the vector elements are in ascendent order the rank is 0.
	if (check == pos){return 0;}

	size_t final_pos = 0;
	for(std::vector<size_t>::iterator it = input.begin(); it != input.end(); ++it) {
		int min_v = pos-1 < *it ? pos-1 : *it;
		final_pos += min_v * myfactorial(pos-1);
		--pos;
	}
	return final_pos;
}

std::vector<size_t> ICE::getPermutationOrder( size_t n, size_t lexicoOrder)
{
  size_t k = lexicoOrder;
  std::vector<size_t> s(n);
  // initialize the permutation vector
  for (size_t i=0; i<n; i++)
    s[i]=i;

  //compute (n- 1)!
  size_t factorial = myfactorial(n-1);
  //check if the number is not in the range of [0, n! - 1]
  if (k/n>=factorial)
  {
    printf("Error: in getPermutationOrder(), lexicoOrder is larger than n!-1\n");
    exit(1);
  }
  // Algorithm:
  //check each element of the array, excluding the right most one.
  //the goal is to find the right element for each s[j] from 0 to n-2
  // method: each position is associated a factorial number
  //    s[0] -> (n-1)!
  //    s[1] -> (n-2)! ...
  // the input number k is divided by the factorial at each position (6, 3, 2, 1 for size =4)
  //   so only big enough k can have non-zero value after division
  //   0 value means no change to the position for the current iteration
  // The non-zero value is further modular by the number of the right hand elements of the current element.
  //     (mode on 4, 3, 2 to get offset 1-2-3, 1-2, 1 from the current position 0, 1, 2)
  //  choose one of them to be moved to the current position,
  //  shift elements between the current and the moved element to the right direction for one position
  for (size_t j=0; j<n-1; j++)
  {
    //calculates the next cell from the cells left
    //(the cells in the range [j, s.length - 1])
    int tempj = (k/factorial) % (n - j);
    //Temporarily saves the value of the cell needed
    // to add to the permutation this time
    int temps = s[j+tempj];
    //shift all elements to "cover" the "missing" cell
    //shift them to the right
    for (size_t i=j+tempj; i>j; i--)
    {
      s[i] = s[i-1]; //shift the chain right
    }
    // put the chosen cell in the correct spot
    s[j]= temps;
    // updates the factorial
    factorial = factorial /(n-(j+1));
  }
  return s;
}

/*
 * Parse a string with delimiter given and convert it to number.
 */
template <typename N>
std::vector<N> ICE::parseStringToNum(const std::string &str, const std::string &delimiter ) 
{
	std::vector<N> nums;
	size_t last = 0; size_t next = 0; 
	while ((next = str.find(delimiter, last)) != std::string::npos) { 
		//std::cout << str.substr(last, next-last);
		nums.push_back(stringToNumber<N>(str.substr(last, next-last)));
		last = next + 1; 
	} 
	nums.push_back(stringToNumber<N>(str.substr(last)));
	//std::cout << last;
	return nums;
}

template <typename T>
T  ICE::stringToNumber ( const std::string &Text )
{
	std::istringstream ss(Text);
	T result;
	return ss >> result ? result : 0;
}


template size_t ICE::stringToNumber<size_t>( const std::string &Text);

template int ICE::stringToNumber<int>( const std::string &Text);

template std::vector<size_t> ICE::parseStringToNum<size_t>(const std::string &str, const std::string &delimiter); 

template std::vector<int> ICE::parseStringToNum<int>(const std::string &str, const std::string &delimiter); 


