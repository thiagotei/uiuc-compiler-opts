
#ifndef Result_UIUC
#define Result_UIUC

namespace ICE 
{

	class Result {
		private:
			int valid_;

		public:
			Result (int v); //! Sets all fields to 0
			Result (const Result&); //! Copy constructor.
			~Result (); //! Shallow; does not delete fields.
			bool isValid() const;
			int getValue();
	};
}


#endif
