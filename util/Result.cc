#include "Result.hh"

ICE::Result::Result(int v) : valid_(v) 
{}

ICE::Result::Result(const Result& r) 
{
	valid_ = r.valid_;
}

ICE::Result::~Result() 
{}

bool ICE::Result::isValid() const 
{
	return valid_ == 0;
}

int ICE::Result::getValue()
{
	return valid_;
}
