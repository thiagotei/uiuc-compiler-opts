#include "Unroll.hh"


int main(int argc, char ** argv)
{
	vector<string> argvList(argv, argv+argc);
	ICE::UnrollUIUC::commandLineProcessing(argvList);	
	
	SgProject * proj = frontend(argvList);
	ROSE_ASSERT(proj);

	cout << "[ICE/UnrollUIUC]..." << endl;
	size_t count = ICE::UnrollUIUC::unrollAll(proj);

	//TODO specify the output name.
	proj->unparse();
	cout << "[ICE/UnrollUIUC] end" << endl;

	return 0;
}

size_t ICE::UnrollUIUC::unrollAll(SgProject * project) 
{
	size_t num_unrolled = 0;	
	TargetList_t targets;
	std::vector< std::vector<int> > pragmasinfo;
	if (SI::is_Fortran_language()) 
	{
		if (AnnotParser::collectFortranTarget(project, targets, PRAGMA_NAME, pragmasinfo))
		{
			if(enable_debug) {
				cout << "collectFortranTarget " << targets.size() << endl;
			}
			std::vector< std::vector<int> >::iterator j = pragmasinfo.begin();
			std::vector< std::vector<int> >::iterator j_end = pragmasinfo.end();
			for(TargetList_t::iterator i = targets.begin(); 
				i != targets.end() && j != j_end; ++i, ++j) 
					if(unrollFortran(*i, *j).isValid())
						++num_unrolled;
		}

	} else // Search for pragmas for C/C++
	{ 
		PragmaList_t pragmas;
		if(AnnotParser::collectPragmas(project, pragmas, PRAGMA_NAME))
		{
			for (PragmaList_t::iterator i = pragmas.begin(); i != pragmas.end(); ++i)
				if(unroll(*i).isValid())
					++num_unrolled;

		}
	}		
	return num_unrolled;
}

ICE::Result ICE::UnrollUIUC::unroll(SgPragmaDeclaration* prag) 
{
//	SI::dumpInfo(s);
	vector<int> pragmainfo;
	SgStatement* s = AnnotParser::processPragmaInfo (prag, PRAGMA_NAME, pragmainfo);
	if(!s) return Result(false);	

	// Expects two infos to do the tiling.
	if(pragmainfo.size() != 2) {
     		cerr<<"Error in loopUnrolling, not proper info on the pragma."<<endl;
		return Result(false);
	}

	if(enable_debug) {
		cout << "pragma info: ";
		for(vector<int>::iterator it = pragmainfo.begin(); it != pragmainfo.end(); ++it){
			cout << *it << " ";
		}	
		cout << endl;
	}
	
	return unroll(s, pragmainfo[0], pragmainfo[1]);
}


ICE::Result ICE::UnrollUIUC::unrollFortran(SgStatement* s, vector<int>& pragmainfo) 
{
	if(enable_debug) {
		cout << "pragma info: ";
		for(vector<int>::iterator it = pragmainfo.begin(); it != pragmainfo.end(); ++it){
			cout << *it << " ";
		}	
		cout << endl;
	}

        if(pragmainfo.size() != 2) {
                cerr<<"Error in loopUnrolling, not proper info on the pragma."<<endl;
                return Result(false);
        }
	return unroll(s, pragmainfo[0], pragmainfo[1]);
}


ICE::Result ICE::UnrollUIUC::unroll(SgStatement* s, size_t target_level, size_t unrolling_factor) 
{
	bool r;
//	SI::dumpInfo(s);

	if (SgForStatement * for_loop = isSgForStatement(s)) {
		cout << "[ICE/UnrollUIUC/tiling2] For loop" << endl;
		//SI::loopUnrolling(for_loop, unrolling_factor);		
		r = loopUnrolling(for_loop, target_level, unrolling_factor);		
	} else if (SgFortranDo * do_loop = isSgFortranDo(s)) {
		cout << "[ICE/UnrollUIUC/tiling2] Do loop" << endl;

		r = loopUnrolling(do_loop, target_level, unrolling_factor);

	} else {
		cerr << "[ICE/UnrollUIUC/tiling2] ERROR! SgStatement not a For neither a Do loop!" << endl;	
		r = false;
	}
	
	return Result(r);
}

bool ICE::UnrollUIUC::loopUnrolling(SgForStatement* loop_nest, size_t target_level, size_t unrolling_factor) 
{

  ROSE_ASSERT(loop_nest != NULL);
  ROSE_ASSERT(target_level >0);

  if (unrolling_factor<=1)
    return true;

  // Locate the target loop at level n
  std::vector<SgForStatement* > loops= SI::querySubTree<SgForStatement>(loop_nest,V_SgForStatement);
  ROSE_ASSERT(loops.size()>=target_level);
  SgForStatement* target_loop = loops[target_level -1]; // adjust to numbering starting from 0

  //SI::loopUnrolling(target_loop, unrolling_factor);
  bool needFringe = true;
  bool jam = false; //only used in the unrool and jam
  SIUI::loopUnrollAndJaming(target_loop, unrolling_factor, NULL, jam, needFringe, enable_debug);

  return true;
}

bool ICE::UnrollUIUC::loopUnrolling(SgFortranDo* loop_nest, size_t target_level, size_t unrolling_factor) 
{

  ROSE_ASSERT(loop_nest != NULL);
  ROSE_ASSERT(target_level >0);

  if (unrolling_factor<=1)
    return true;

  // Locate the target loop at level n
  std::vector<SgFortranDo* > loops= SI::querySubTree<SgFortranDo>(loop_nest,V_SgFortranDo);
  ROSE_ASSERT(loops.size()>=target_level);
  SgFortranDo* target_loop = loops[target_level -1]; // adjust to numbering starting from 0

  loopUnrolling(target_loop, unrolling_factor);

  return true;
}

// a brand new serious implementation for loop unrolling, Liao, 6/25/2009
/* Handle left-over iterations if iteration_count%unrolling_factor != 0
 * Handle stride (step) >1
 * Assuming loop is normalized to [lb,ub,step], ub is inclusive (<=, >=)
 *
 *  iteration_count = (ub-lb+1)%step ==0?(ub-lb+1)/step: (ub-lb+1)/step+1
 *  fringe = iteration_count%unroll_factor==0 ? 0:unroll_factor*step;
 *    fringe ==0 if no leftover iterations
 *    otherwise adjust ub so the leftover iterations will put into the last fringe loop
 *  unrolled loop's header: for (i=lb;i<=ub - fringe; i+= step*unroll_factor)
 *  loop body: copy body n times from 0 to factor -1
 *     stmt(i+ 0*step); ...; stmt (i+ (factor-1)*step);
 *  fringe loop: the same as the original loop, except for no init statement
 *
 * e.g:
 * // unrolling 3 times for the following loop with stride !=1
 *       for (i=0; i<=9; i+=3)
 *       {
 *         a[i]=i;
 *       }
 * // it becomes
 *     // iteration count = 10%3=1 -> 10/3+1 = 4
 *     // fringe = 4%3 =1 --> 3*3
 *     // ub-fringe = 9-3*3
 *        for (i=0; i<=9-3*3; i+=3*3)
 *     {
 *       a[i+3*0]=i;
 *       a[i+3*1]=i;
 *       a[i+3*2]=i;
 *     }
 *     // i=9 is the leftover iteration
 *     for (; i<=9; i+=3)
 *     {
 *       a[i]=i;
 *     }
 *
 */
bool ICE::UnrollUIUC::loopUnrolling(SgFortranDo* target_loop, size_t unrolling_factor)
{
  //Handle 0 and 1, which means no unrolling at all
  if (unrolling_factor <= 1)
    return true;
  // normalize the target loop first
  if (!SI::doLoopNormalization(target_loop));
  {// the return value is not reliable
    //    cerr<<"Error in SI::loopUnrolling(): target loop cannot be normalized."<<endl;
    //    dumpInfo(target_loop);
    //    return false;
  }
  // grab the target loop's essential header information
  SgInitializedName* ivar = NULL;
  SgExpression* lb = NULL;
  SgExpression* ub = NULL;
  SgExpression* step = NULL;
  SgStatement* orig_body = NULL;
  if (!SI::isCanonicalDoLoop(target_loop, &ivar, &lb, &ub, &step, &orig_body, NULL, NULL))
  {
    cerr<<"Error in loopUnrolling(): target loop is not canonical."<<endl;
    SI::dumpInfo(target_loop);
    return false;
  }
  ROSE_ASSERT(ivar&& lb && ub && step);
  ROSE_ASSERT(isSgBasicBlock(orig_body));

//  string iter_var_name = "iter_count_";//+ rose::StringUtility::numberToString(++SI::gensym_counter);
//  string fringe_var_name = "fringe_";//+ rose::StringUtility::numberToString(++SI::gensym_counter);

  // Declaring variable begining of the enclosing function
  SgFunctionDefinition* funcDef = SI::getEnclosingFunctionDefinition(target_loop);
  ROSE_ASSERT(funcDef);
  SgBasicBlock* funcBody = funcDef->get_body();
  ROSE_ASSERT(funcBody);

  //funcBody->print_symboltable("[Unroll] funcBody scope");

  string iter_var_name =  SIUI::findAvailVarName(string("iter_count_"), funcBody);
  string fringe_var_name = SIUI::findAvailVarName(string("fringe_"), funcBody);

  SgVariableDeclaration* iter_var_decl = SB::buildVariableDeclaration(iter_var_name, SB::buildIntType(), NULL, funcBody);

  SgVariableDeclaration* fringe_var_decl = SB::buildVariableDeclaration(fringe_var_name, SB::buildIntType(), NULL, funcBody);

  SI::insertStatementAfterLastDeclaration(iter_var_decl, funcBody->get_scope());
  SI::insertStatementAfterLastDeclaration(fringe_var_decl, funcBody->get_scope());


   SgScopeStatement* scope = target_loop->get_scope();
   ROSE_ASSERT(scope);

   // generate the fringe loop
   bool needFringe = true;
   SgFortranDo* fringe_loop = SI::deepCopy<SgFortranDo>(target_loop);
   SI::insertStatementAfter(target_loop,fringe_loop);

 ////  SI::replaceExpression(fringe_loop->get_initialization(), NULL, false);
// In C the initialization of the fringe loop would be empty, but in Fortran this cannot happen.
// So the loop variable is assigned to itself.
   SgAssignOp * fringe_init = isSgAssignOp(fringe_loop->get_initialization());
   fringe_init->set_rhs_operand(SI::deepCopy<SgVarRefExp>(isSgVarRefExp(fringe_init->get_lhs_operand())));
 
   // (ub - lb + 1)
   SgExpression* raw_range_exp = SB::buildSubtractOp(
				  SB::buildAddOp(
				    SI::copyExpression(ub),
				    SB::buildIntVal(1)),
            			  SI::copyExpression(lb));
  
   //if MOD((ub - lb + 1),step) .eq. 0
   SgExprListExp* mod_1_e_list = SB::buildExprListExp(); 
   SI::appendExpression(mod_1_e_list,  raw_range_exp);
   SI::appendExpression(mod_1_e_list,  SI::copyExpression(step));
 
   SgExprStatement* condition_1 = SB::buildExprStatement(
				      SB::buildEqualityOp(
				        SB::buildFunctionCallExp("MOD", SgTypeInt::createType(), mod_1_e_list, scope),
				        SB::buildIntVal(0)));  

   SgExprStatement* true_1 = SB::buildAssignStatement(
			       SB::buildVarRefExp(iter_var_name, scope),
        		       SB::buildDivideOp(
			         SI::copyExpression(raw_range_exp),
 				 SI::copyExpression(step)
			     ));

   SgExprStatement* false_1 = SB::buildAssignStatement(
				SB::buildVarRefExp(iter_var_name, scope),
				SB::buildAddOp(
        		          SB::buildDivideOp(
			 	    SI::copyExpression(raw_range_exp),
				    SI::copyExpression(step)),
   				  SB::buildIntVal(1)
				));

   SgIfStmt* if_iter_count = SB::buildIfStmt(
			      condition_1,
			      SB::buildBasicBlock(
				true_1
                              ),
			      SB::buildBasicBlock(
				false_1
			      ));

   SI::insertStatementBefore(target_loop, if_iter_count);
   SI::attachComment(if_iter_count, "iter_count = (ub-lb+1)%step ==0?(ub-lb+1)/step: (ub-lb+1)/step+1;");

  // fringe = iteration_count%unroll_factor==0 ? 0:unroll_factor*step
   SgExprListExp* mod_2_e_list = SB::buildExprListExp(); 
   SI::appendExpression(mod_2_e_list, SB::buildVarRefExp(iter_var_name, funcDef->get_scope()) );
   SI::appendExpression(mod_2_e_list, SB::buildIntVal(unrolling_factor)); 
 
   SgExprStatement* condition_2 = SB::buildExprStatement(
				      SB::buildEqualityOp(
				        SB::buildFunctionCallExp("MOD", SgTypeInt::createType(), mod_2_e_list, scope),
				        SB::buildIntVal(0)));  


  SgExprStatement* true_2 = SB::buildAssignStatement(
			      SB::buildVarRefExp(fringe_var_name, scope),
			      SB::buildIntVal(0) 
                            );

  SgExprStatement* false_2 = SB::buildAssignStatement(
			       SB::buildVarRefExp(fringe_var_name, scope),
			       SB::buildMultiplyOp(
				 SB::buildIntVal(unrolling_factor),
				 SI::copyExpression(step)));

  SgIfStmt* if_fringe = SB::buildIfStmt(
			      condition_2,
			      SB::buildBasicBlock(
				true_2
                              ),
			      SB::buildBasicBlock(
				false_2
			      ));

   SI::insertStatementBefore(target_loop, if_fringe);
   SI::attachComment(if_fringe, "fringe = iter_count%unroll_factor==0 ? 0:unroll_factor*step");

//////////////
   // Declare iter_cout var
/*   SgVariableDeclaration* fringe_decl = SB::buildVariableDeclaration(fringe_name, SB::buildIntType(),SB::buildAssignInitializer(initor), scope);
   SI::insertStatementBefore(target_loop, fringe_decl);
 */
  // _lu_iter_count = (ub-lb+1)%step ==0?(ub-lb+1)/step: (ub-lb+1)/step+1;
/*  SgExpression* raw_range_exp = SB::buildSubtractOp(
				  SB::buildAddOp(
				    SI::copyExpression(ub),
				    SB::buildIntVal(1)),
            			  SI::copyExpression(lb));

  raw_range_exp->set_need_paren(true);

  SgExpression* range_d_step_exp = SB::buildDivideOp(raw_range_exp,SI::copyExpression(step));//(ub-lb+1)/step

  SgExpression* condition_1 = SB::buildEqualityOp(
				SB::buildModOp(
				  SI::copyExpression(raw_range_exp),
				  SI::copyExpression(step)), 
				SB::buildIntVal(0)); //(ub-lb+1)%step ==0

  SgExpression* iter_count_exp = SB::buildConditionalExp(
				   condition_1, range_d_step_exp, SB::buildAddOp(
								    SI::copyExpression(range_d_step_exp),
								    SB::buildIntVal(1)));
*/
  // fringe = iteration_count%unroll_factor==0 ? 0:unroll_factor*step
/*  SgExpression* condition_2 = SB::buildEqualityOp(
				SB::buildModOp(
				  iter_count_exp, 
				  SB::buildIntVal(unrolling_factor)), 
				SB::buildIntVal(0));

  SgExpression* initor = SB::buildConditionalExp(condition_2, SB::buildIntVal(0), SB::buildMultiplyOp(
												      SB::buildIntVal(unrolling_factor),
												      SI::copyExpression(step)));

   SgScopeStatement* scope = target_loop->get_scope();
   ROSE_ASSERT(scope != NULL);
   string fringe_name = "_lu_fringe_"+ rose::StringUtility::numberToString(++SI::gensym_counter);
   SgVariableDeclaration* fringe_decl = SB::buildVariableDeclaration(fringe_name, SB::buildIntType(),SB::buildAssignInitializer(initor), scope);
   SI::insertStatementBefore(target_loop, fringe_decl);
   SI::attachComment(fringe_decl, "iter_count = (ub-lb+1)%step ==0?(ub-lb+1)/step: (ub-lb+1)/step+1;");
   SI::attachComment(fringe_decl, "fringe = iter_count%unroll_factor==0 ? 0:unroll_factor*step");
*/
  // compile-time evaluate to see if index is a constant of value 0
  // if so, the iteration count can be divided even by the unrolling factor
  // and no fringe loop is needed
  // WE have to fold on its parent node to get a possible constant since
  // constant folding only folds children nodes, not the current node to a constant
/*   ConstantFolding::constantFoldingOptimization(fringe_decl,false);
   SgInitializedName * ivarname = fringe_decl->get_variables().front();
   ROSE_ASSERT(ivarname != NULL);
   // points to a new address if constant folding happens
   SgAssignInitializer * init1 = isSgAssignInitializer(ivarname->get_initializer());
   if (init1)
    if (isSgIntVal(init1->get_operand_i()))
     if (isSgIntVal(init1->get_operand_i())->get_value() == 0)
       needFringe = false;
*/
  // rewrite loop header ub --> ub -fringe; step --> step *unrolling_factor
 //del  SgBinaryOp* ub_bin_op = isSgBinaryOp(ub->get_parent());
 //del  ROSE_ASSERT(ub_bin_op);

   if (needFringe) {
     //del ub_bin_op->set_rhs_operand(SB::buildSubtractOp(SI::copyExpression(ub),SB::buildVarRefExp(fringe_name,scope)));
     SI::replaceExpression(ub,SB::buildSubtractOp(SI::copyExpression(ub),SB::buildVarRefExp(fringe_var_name,scope)));
   }
   else
   {
     //del ub_bin_op->set_rhs_operand(SI::copyExpression(ub));
     SI::removeStatement(fringe_var_decl);
   }

   //del SgBinaryOp* step_bin_op = isSgBinaryOp(step->get_parent());
   //del ROSE_ASSERT(step_bin_op != NULL);
   //del step_bin_op->set_rhs_operand(SB::buildMultiplyOp(SI::copyExpression(step),SB::buildIntVal(unrolling_factor)));
  
/*   bool isPlus = false;
   if (isSgPlusAssignOp(step_bin_op))
     isPlus = true;
    else if (isSgMinusAssignOp(step_bin_op))
      isPlus = false;
    else
    {
      cerr<<"Error in SI::loopUnrolling(): illegal incremental exp of a canonical loop"<<endl;
      SI::dumpInfo(step_bin_op);
      ROSE_ASSERT(false);
    }
*/
   bool isPlus = false;
   if ( isSgIntVal(step) && isSgIntVal(step)->get_value() > 0 )
     isPlus = true;
    else if (isSgIntVal(step) && isSgIntVal(step)->get_value() < 0)
      isPlus = false;
    else
    {
      cerr<<"Error in SI::loopUnrolling(): illegal incremental exp of a canonical loop"<<endl;
      SI::dumpInfo(step);
      ROSE_ASSERT(false);
    }


   SgIntVal * val_step =isSgIntVal(step);   
   if(val_step)
   {
         if(SgProject::get_verbose() >= 1)  
                cout << "[Unroll] step value: " << val_step->get_value() << endl;
   }

   // copy loop body factor -1 times, and replace reference to ivar  with ivar +/- step*[1 to factor-1]
   for (size_t i =1; i<unrolling_factor; i++)
   {
     SgBasicBlock* body = isSgBasicBlock(SI::deepCopy(fringe_loop->get_body())); // normalized loop has a BB body
     ROSE_ASSERT(body);
     std::vector<SgVarRefExp*> refs = SI::querySubTree<SgVarRefExp> (body, V_SgVarRefExp);
     for (std::vector<SgVarRefExp*>::iterator iter = refs.begin(); iter !=refs.end(); iter++)
     {
       SgVarRefExp* refexp = *iter;
       if (refexp->get_symbol()==ivar->get_symbol_from_symbol_table())
       {
         // replace reference to ivar with ivar +/- step*i
         SgExpression* new_exp = NULL;
         //build replacement  expression for every appearance 
         if (isPlus) //ivar +/- step * i
         new_exp = SB::buildAddOp(SB::buildVarRefExp(ivar,scope),SB::buildMultiplyOp(SI::copyExpression(step),SB::buildIntVal(i)));
         else
         new_exp = SB::buildSubtractOp(SB::buildVarRefExp(ivar,scope),SB::buildMultiplyOp(SI::copyExpression(step),SB::buildIntVal(i)));

         // replace it with the right one
         SI::replaceExpression(refexp, new_exp);
       }
     }
     // copy body to loop body, this should be a better choice
     // to avoid redefinition of variables after unrolling (new scope is introduced to avoid this)
     SI::appendStatement(body,isSgBasicBlock(orig_body));
    // moveStatementsBetweenBlocks(body,isSgBasicBlock(orig_body));
   }

    SI::replaceExpression( step, SB::buildMultiplyOp(SI::copyExpression(step),SB::buildIntVal(unrolling_factor)));

   // remove the fringe loop if not needed finally
   // it is used to buffering the original loop body before in either cases
   if (!needFringe)
     SI::removeStatement(fringe_loop);

   // constant folding for the transformed AST
   ConstantFolding::constantFoldingOptimization(scope,false);
   //ConstantFolding::constantFoldingOptimization(getProject(),false);

  return true;
}


void ICE::UnrollUIUC::commandLineProcessing(std::vector<std::string> &argvList) {

  if (CommandlineProcessing::isOption (argvList,"-rose:unroll:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for unroll functions..."<<endl;
    enable_debug= true;
  }

/*  if (CommandlineProcessing::isOptionWithParameter(argvList,"-rose:tiling:","target_level", target_level, true))
  {	
    cout<<"Target level is " << target_level << endl;	
  }

  if (CommandlineProcessing::isOptionWithParameter (argvList,"-rose:tiling:","tile_size", tile_size, true))
  {	
    cout<<"Tile size is " << tile_size << endl;	
  }
*/

  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC Unroll-specific options"<<endl;
    cout<<"Usage: loopUnroll [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
    cout<<"\t-rose:unroll:enable_debug                   run in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }
}


