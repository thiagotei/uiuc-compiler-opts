
#ifndef UNROLLING_UIUC
#define UNROLLING_UIUC

#include <cstdlib>
#include <sage3basic.h>
#include <Cxx_Grammar.h>
#include <sageBuilder.h>
#include <Rose/StringUtility.h>
#include <string>
#include <vector>
#include <AnnotParser.hh>
#include <Result.hh>
#include <constantFolding.h>
#include <SageInterfaceUIUC.hh>

using namespace std;

namespace SB = SageBuilder;
namespace SI = SageInterface;
namespace SIUI = ICE::SageInterfaceUIUC;

namespace ICE
{
	namespace UnrollUIUC
	{
		bool enable_debug = false;
		static const std::string PRAGMA_NAME("uiuc_unroll");

 		void commandLineProcessing(std::vector<std::string> &argvList); 
		Result unroll(SgPragmaDeclaration* prag);
		Result unrollFortran(SgStatement * s, 
			      	     vector<int>& pragmainfo);
		Result unroll(SgStatement * s, 
                              size_t unrolling_factor,
                              size_t target_level);
		size_t unrollAll(SgProject * project);
		bool loopUnrolling(SgForStatement *s, 
        			   size_t unrolling_factor, 
				   size_t target_level);
		bool loopUnrolling(SgFortranDo* loop_nest, 
				   size_t unrolling_factor, 
				   size_t target_level);
		bool loopUnrolling(SgFortranDo* target_loop, 
				   size_t unrolling_factor);

		//bool loopUnrolling(SgForStatement * target_loop, size_t unrolling_factor);	

	}
}	

#endif
