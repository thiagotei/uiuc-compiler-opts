# UIUC Compiler Optimizations

This project implements multiple code transformations using the [Rose](http://rosecompiler.org/) framework.
Therefore you have to install Rose to use this project. Go to the [Rose webpage](https://github.com/rose-compiler/rose/wiki/How-to-Set-Up-ROSE) 
to see how to install it.

More information [on the wiki](https://bitbucket.org/thiagotei/uiuc-compiler-opts/wiki).

## How to build, compile and install

### Requirements

On Ubuntu install the following packages: `libgmp-dev uuid-dev`

### Build

The variables `ROSE_PATH` and `BOOST_ROOT` must be set to their installation path. Cmake assumes that the libs are in their `<path>/lib` and
includes are in the `<path>/include`.   

```shell
   cmake -DCMAKE_INSTALL_PREFIX=<default /usr/local> -DROSE_PATH=<rose-path> -DBOOST_ROOT=<boost-path> <path to uiuc-compiler-opts src dir>
```

If you have Boost installed on the system path, you do not need to pass the `-DBOOST_ROOT=<...>` but you need to comment out [`set(Boost_NO_SYSTEM_PATHS ON)`](https://bitbucket.org/thiagotei/uiuc-compiler-opts/src/master/CMakeLists.txt#lines-13) 
in the `CMakeLists.txt` file
so the `cmake` will look for it in the system.

If Rose installation includes the Fortran frontend, it is necessary to add the java `libjvm.so` to the path. 
It is recommended to add the variable `-DJRE_ROOT=<path to libjvm>` to the `cmake` command line. 
For instance:  

```shell
   cmake <the others already mentioned> -DJRE_ROOT=/usr/lib/jvm/java-8-oracle/jre/lib/amd64/server 
```

Or put it in `LD_LIBRARY_PATH` for every terminal that you use:  

```shell
   export LD_LIBRARY_PATH=/usr/lib/jvm/java-8-oracle/jre/lib/amd64/server:$LD_LIBRARY_PATH
```

Example:   

```shell
   mkdir -p $HOME/uiuc-compiler-opts/{build, install}
   git clone https://bitbucket.org/thiagotei/uiuc-compiler-opts.git $HOME/uiuc-compiler-opts/src-git
   cd $HOME/uiuc-compiler-opts/build
   cmake -DROSE_PATH=/usr/workspace/wsa/bluenet/thiago/spack/opt/spack/chaos_5_x86_64_ib/gcc-4.4.7/rose-0.9.7-liohtdx2kzvtroo5myaav7n6uxt64u34 -DBOOST_ROOT=/usr/workspace/wsa/bluenet/thiago/spack/opt/spack/chaos_5_x86_64_ib/gcc-4.4.7/boost-1.54.0-6qgaiz54p56azkpjpzeqmvedlfm4cirn -DCMAKE_INSTALL_PREFIX=$HOME/uiuc-compiler-opts/install ../src-git
```

Example 2 -
Using a compiler build with spack.  

```shell
   spack use gcc@5.3.0
   CC=gcc CXX=g++ cmake -DROSE_PATH=/usr/workspace/wsa/bluenet/thiago/tools/rose-install-03172017%gcc@5.3.0_autotools -DBOOST_ROOT=$(spack location -i boost@1.58.0%gcc@5.3.0) ..
```

In order to debug the boost library ```-DBoost_DEBUG=ON``` can be used.

### Compile and Install

After build:

```make``` 

or to see detailed compilation:

```make VERBOSE=1``` 

Then install:

```make install```

-----
## How to use the transformations

### Loop Unrolling
Loop indices in the nest starts from 1.
Options: `./loopUnroll -rose:unroll:enable_debug`

#### Fortran 
Just add the one of the annotations below just above the loop nest: 
```
!$ uiuc_unroll <loop> <factor>  
c$ uiuc_unroll <loop> <factor> 
*$ uiuc_unroll <loop> <factor>
```
#### C
Just add the annotation `#pragma uiuc_unroll <loop> <factor>` before the loop nest to be unrolled. 
Example unrolling the 2nd loop (index variable d) 3 times:

```
int main()
{
  int m = 10, p = 10, q = 10, c, d, k, sum = 0;
  int first[10][10], second[10][10], multiply[10][10];

#pragma uiuc_unroll 2 3
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          sum = sum + first[c][k]*second[k][d];
        }
        printf("%d\t", sum);
        multiply[c][d] = sum;
        sum = 0;
      }
    }
   return 0;
}
```
-----
### Loop UnrollAndJam
Loop indices in the nest starts from 1.
Options: `./loopUnrollAndJam -rose:unrollandjam:enable_debug -rose:unrollandjam:no_fringe`

#### Fortran 
Just add the one of the annotations below just above the loop nest: 
```
!$ uiuc_unrollandjam <loop> <factor>  
c$ uiuc_unrollandjam <loop> <factor> 
*$ uiuc_unrollandjam <loop> <factor>
```
#### C
Just add the annotation `#pragma uiuc_unrollandjam <loop> <factor>` before the loop nest to be unrolled and jammed.
It is used for outer loops. Because of inconsistency in Rose the loops must have brackets. 
Example unrolling the 2nd loop (index variable d) 3 times:

```
int main()
{
  int m = 10, p = 10, q = 10, c, d, k, sum = 0;
  int first[10][10], second[10][10], multiply[10][10];

#pragma uiuc_unrollandjam 2 3
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          multiply[c][d] += first[c][k]*second[k][d];
        }
      }
    }
   return 0;
}
```
-----
### Loop StripMine
Loop indices in the nest starts from 1. 
Options: `./loopStripMine -rose:stripmine:enable_debug`

#### Fortran
Just add the one of the annotations below just above the loop nest: 
```
!$ uiuc_stripmine <loop> <factor>
c$ uiuc_stripmine <loop> <factor>
*$ uiuc_stripmine <loop> <factor>
```

#### C
Just add the annotation `#pragma uiuc_stripmine <loop> <factor>` before the loop nest to be strip-mined. 
Example:
```
int main()
{
  int m = 10, p = 10, q = 10, c, d, k, sum = 0;
  int first[10][10], second[10][10], multiply[10][10];

#pragma uiuc_stripmine 3 10 
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          sum = sum + first[c][k]*second[k][d];
        }
        printf("%d\t", sum);
        multiply[c][d] = sum;
        sum = 0;
      }
    }
    return 0;
}
```
-----
### Loop Tiling
Loop indices in the nest starts from 1. 
Options: `./loopTiling -rose:tiling:enable_debug`

#### Fortran
Just add the one of the annotations below just above the loop nest: 
```
!$ uiuc_tiling <loop> <factor>
c$ uiuc_tiling <loop> <factor>
*$ uiuc_tiling <loop> <factor>
```

#### C
Just add the annotation `#pragma uiuc_tiling <loop> <factor>` before the loop nest to be tiled. 
Example:
```
int main()
{
  int m = 10, p = 10, q = 10, c, d, k, sum = 0;
  int first[10][10], second[10][10], multiply[10][10];

#pragma uiuc_tiling 3 10 
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          sum = sum + first[c][k]*second[k][d];
        }
        printf("%d\t", sum);
        multiply[c][d] = sum;
        sum = 0;
      }
    }
    return 0;
}
```
-----
### Loop interchange
Loop indices in the nest starts from 0. Options: ./loopInterchange -rose:interchange:enable_debug

**The current version works correctly only on tightly nested loops!**


#### Fortran
Just add the one of the annotations below just above the loop nest: 
```
!$ uiuc_interchange < order >
c$ uiuc_interchange < order >
*$ uiuc_interchange < order >
```

#### C
Just add the annotation `#pragma uiuc_interchange <order>` before the loop nest to interchange. The loop nest indices start from 0. The order must reflect the full depth of the loop nest below of the annotation.
Example:
```
int main()
{
  int m = 10, p = 10, q = 10, c, d, k, sum = 0;
  int first[10][10], second[10][10], multiply[10][10];

#pragma uiuc_interchange 1,2,0
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          sum = sum + first[c][k]*second[k][d];
        }
        printf("%d\t", sum);
        multiply[c][d] = sum;
        sum = 0;
      }
    }
    return 0;
}
```
-----
### Loop Invariant Code Motion
I have implemented a LICM for C/C++ that is being tested on Kripke.

#### C
Just add the annotation `#pragma uiuc_licm` before the loop nest to.
It hoists the statement inside the loop nest as high as possible. It
does not break statements.
Example:
```
int main()
{
  int m = 10, p = 10, q = 10, c, d, k, sum = 0;
  int first[10][10], second[10][10], multiply[10][10];

#pragma uiuc_licm
    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          sum = sum + first[c][k]*second[k][d];
        }
        printf("%d\t", sum);
        multiply[c][d] = sum;
        sum = 0;
      }
    }
    return 0;
}
```

-----
### Generate AST dot and pdf figures

```
/usr/workspace/wsa/bluenet/thiago/tools/build/rose/obj-git-fork%gcc@4.4.7/exampleTranslators/DOTGenerator/dotGeneratorWholeASTGraph mm.c
```
```
dot -Tpdf mm_nobracks.c_AST.dot  -o mm_nobracks_AST.pdf
```