#!/bin/bash


#export LD_LIBRARY_PATH=/usr/workspace/wsa/bluenet/thiago/spack/opt/spack/chaos_5_x86_64_ib/gcc-4.4.7/jdk-8u77-linux-x64-eqljub5l3poe75o63bpjv7hxxofjr7qw/jre/lib/amd64/server:${LD_LIBRARY_PATH}

java_path_=`spack find -p jdk | awk '/jdk/{print $2}'`
#echo "UOW ", $java_path_
if [ $? -eq 0 ]; then
	export LD_LIBRARY_PATH=${java_path_}/jre/lib/amd64/server:${LD_LIBRARY_PATH}
else
	echo "ERROR! Could not find jdk path installed in spack"
fi
