#ifndef TILING_UIUC
#define TILING_UIUC

#include <cstdlib>
#include <sage3basic.h>
#include <Cxx_Grammar.h>
#include <sageBuilder.h>
#include <Rose/StringUtility.h>
#include <string>
#include <vector>
#include <AnnotParser.hh>
#include <Result.hh>
#include <SageInterfaceUIUC.hh>

using namespace std;

namespace SIUI = ICE::SageInterfaceUIUC;

namespace ICE
{
	namespace TilingUIUC
	{

		bool enable_debug = false;
		static const std::string PRAGMA_NAME("uiuc_tiling");
 		void commandLineProcessing(std::vector<std::string> &argvList); 
 		size_t tilingAll(SgProject * project);
		Result tiling(SgPragmaDeclaration* prag);
		Result tilingFortran(SgStatement * s, std::vector<int>& pragmainfo);
		Result tiling(SgStatement * s, size_t targetLevel, size_t tileSize);
		bool loopTiling(SgFortranDo* loopNest, size_t targetLevel, size_t tileSize);

	}
}
#endif
