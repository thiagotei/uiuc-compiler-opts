#include "Tiling.hh"


int main(int argc, char ** argv)
{
	vector<string> argvList(argv, argv+argc);
	ICE::TilingUIUC::commandLineProcessing(argvList);	
	
	SgProject * proj = frontend(argvList);
	ROSE_ASSERT(proj);

	cout << "[ICE/TilingUIUC]..." << endl;
	size_t count = ICE::TilingUIUC::tilingAll(proj);

	//TODO specify the output name.
	proj->unparse();
	cout << "[ICE/TilingUIUC] end" << endl;

	
	
	return 0;
}




ICE::Result ICE::TilingUIUC::tiling(SgPragmaDeclaration* prag) 
{
//	SageInterface::dumpInfo(s);
	vector<int> pragmainfo;
	SgStatement* s = AnnotParser::processPragmaInfo (prag, PRAGMA_NAME, pragmainfo);
	if(!s) return Result(false);	

	// Expects two infos to do the tiling.
	if(pragmainfo.size() != 2) {
     		cerr<<"Error in loopTiling, not proper info on the pragma."<<endl;
		return Result(false);
	}

	if(enable_debug) {
		cout << "pragma info: ";
		for(vector<int>::iterator it = pragmainfo.begin(); it != pragmainfo.end(); ++it){
			cout << *it << " ";
		}	
		cout << endl;
	}
	
	return tiling(s, pragmainfo[0], pragmainfo[1]);
}

ICE::Result ICE::TilingUIUC::tilingFortran(SgStatement * s, vector<int>& pragmainfo) {

	if(pragmainfo.size() != 2) {
     		cerr<<"Error in loopTiling, not proper info on the pragma."<<endl;
		return Result(false);
	}

	return tiling(s, pragmainfo[0], pragmainfo[1]);
}


ICE::Result ICE::TilingUIUC::tiling(SgStatement* s, size_t targetLevel, size_t tileSize) 
{
	bool r;
//	SageInterface::dumpInfo(s);

	if (SgForStatement * for_loop = isSgForStatement(s)) {
		cout << "[ICE/TilingUIUC/tiling2] For loop" << endl;
		SageInterface::loopTiling(for_loop, targetLevel, tileSize);		
		r = true;
	} else if (SgFortranDo * do_loop = isSgFortranDo(s)) {
		cout << "[ICE/TilingUIUC/tiling2] Do loop" << endl;
		loopTiling(do_loop, targetLevel, tileSize);
		r = true;
	} else {
		cerr << "[ICE/TilingUIUC/tiling2] ERROR! SgStatement not a For neither a Do loop!" << endl;	
		r = false;
	}
	
	return Result(r);
}

size_t ICE::TilingUIUC::tilingAll(SgProject * project) 
{

	size_t num_tiled = 0;	
	TargetList_t targets;
        std::vector< std::vector<int> > pragmasinfo;
        //Rose_STL_Container< std::vector<int> > pragmasinfo;
	if (SageInterface::is_Fortran_language()) 
	{
		if (AnnotParser::collectFortranTarget(project, targets, PRAGMA_NAME, pragmasinfo))
		{
			std::vector< std::vector<int> >::iterator j = pragmasinfo.begin();
			std::vector< std::vector<int> >::iterator j_end = pragmasinfo.end();
			//Rose_STL_Container< std::vector<int> >::iterator j = pragmasinfo.begin();
			//Rose_STL_Container< std::vector<int> >::iterator j_end = pragmasinfo.end();
			for(TargetList_t::iterator i = targets.begin(); 
                            i != targets.end() && j != j_end; ++i, ++j) 
				if(tilingFortran(*i, *j).isValid())
					++num_tiled;
		}

	} else // Search for pragmas for C/C++
	{ 
		PragmaList_t pragmas;
		if(AnnotParser::collectPragmas(project, pragmas, PRAGMA_NAME))
		{
			for (PragmaList_t::iterator i = pragmas.begin(); i != pragmas.end(); ++i)
				if(tiling(*i).isValid())
					++num_tiled;

		
		}
	}		

	return num_tiled;
}

//! Tile the n-level (starting from 1) of a perfectly nested loop nest using tiling size s
/* Translation
 Before:
  for (i = 0; i < 100; i++)
    for (j = 0; j < 100; j++)
      for (k = 0; k < 100; k++)
        c[i][j]= c[i][j]+a[i][k]*b[k][j];

  After tiling i loop nest's level 3 (k-loop) with size 5, it becomes

// added a new controlling loop at the outer most level
  int _lt_var_k;
  for (_lt_var_k = 0; _lt_var_k <= 99; _lt_var_k += 1 * 5) {
    for (i = 0; i < 100; i++)
      for (j = 0; j < 100; j++)
        // rewritten loop header , normalized also
        for (k = _lt_var_k; k <= (99 < (_lt_var_k + 5 - 1))?99 : (_lt_var_k + 5 - 1); k += 1) {
          c[i][j] = c[i][j] + a[i][k] * b[k][j];
        }
  }
// finally run constant folding

 */
bool ICE::TilingUIUC::loopTiling(SgFortranDo* loopNest, size_t targetLevel, size_t tileSize)
{
	return SIUI::loopBlocking(loopNest, targetLevel, tileSize, 0);
}

void ICE::TilingUIUC::commandLineProcessing(std::vector<std::string> &argvList) {

  if (CommandlineProcessing::isOption (argvList,"-rose:tiling:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for loopTiling functions..."<<endl;
    enable_debug= true;
  }

/*  if (CommandlineProcessing::isOptionWithParameter(argvList,"-rose:tiling:","target_level", target_level, true))
  {	
    cout<<"Target level is " << target_level << endl;	
  }

  if (CommandlineProcessing::isOptionWithParameter (argvList,"-rose:tiling:","tile_size", tile_size, true))
  {	
    cout<<"Tile size is " << tile_size << endl;	
  }
*/
  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC Tiling-specific options"<<endl;
    cout<<"Usage: tiling [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
  //  cout<<"\t-rose:tiling:target_level                   Target loop n-level starting from 1."<<endl;
  //  cout<<"\t-rose:tiling:tile_size                      Tile Size of the targeted loop level."<<endl;
    cout<<"\t-rose:tiling:enable_debug                   run tiling in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }
}


