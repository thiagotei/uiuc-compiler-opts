#!/bin/bash

export LD_LIBRARY_PATH=/usr/lib/jvm/java-8-oracle/jre/lib/amd64/server:$LD_LIBRARY_PATH
#cmake -DROSE_PATH=/home/thiago/rose-thiago/install-gcc@5.4.0 -DBOOST_ROOT=/home/thiago/spack/opt/spack/linux-ubuntu16.04-x86_64/gcc-5.4.0/boost-1.58.0-bbskc3ymggbnfqjmkermcqes55l3rjgb ..

cmake -DROSE_PATH=/home/thiago/rose-thiago/install-gcc@5.4.0 \
	-DBOOST_ROOT=/home/thiago/spack/opt/spack/linux-ubuntu16.04-x86_64/gcc-5.4.0/boost-1.58.0-bbskc3ymggbnfqjmkermcqes55l3rjgb \
	-DCANDL_PATH=/home/thiago/Documents/periscop/install \
	-DOSL_PATH=/home/thiago/Documents/periscop/install \
	-DPIPLIB_PATH=/home/thiago/Documents/periscop/install   \
	-DPOCC_PATH=/home/thiago/Documents/pocc/pocc-1.4.2/driver/install-pocc \
	-DPLUTO_PATH=/home/thiago/Documents/pocc/pocc-1.4.2/optimizers/install-pluto \
	-DLETSEE_PATH=/home/thiago/Documents/pocc/pocc-1.4.2/optimizers/install-letsee \
	-DFM_PATH=/home/thiago/Documents/pocc/pocc-1.4.2/math/install-fm  ..
