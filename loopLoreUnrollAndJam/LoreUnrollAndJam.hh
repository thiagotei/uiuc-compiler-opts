
#ifndef INTERCHANGE_UIUC
#define INTERCHANGE_UIUC

#include <cstdlib>
#include <sage3basic.h>
#include <Cxx_Grammar.h>
#include <sageBuilder.h>
#include <string>
#include <vector>
#include <AnnotParser.hh>
#include <Result.hh>
#include <Misc.hh>
#include <dependence.hh>
#include <SageInterfaceUIUC.hh>

using namespace std;
using namespace restructurer;

namespace SI = SageInterface;
namespace SIUI = ICE::SageInterfaceUIUC;

namespace ICE
{
	namespace LoreUnrollAndJamUIUC
	{
		bool enable_debug = false;
		static const std::string PRAGMA_NAME("uiuc_loreunrollandjam");
		//Delimiter to the new order string in the annotation  
		static const std::string PRAGMA_DELIMITER(",");

 		void commandLineProcessing(std::vector<std::string> &argvList); 
		Result unrollandjam(SgPragmaDeclaration* prag);
		Result unrollandjam(SgStatement* s, size_t targetLevel, size_t factor);
		bool unrollandjam(SgForStatement* s, size_t targetLevel, size_t factor);
		int unrollandjamAll(SgProject * project);
	}
}	

#endif
