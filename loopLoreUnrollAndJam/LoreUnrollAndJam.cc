#include "LoreUnrollAndJam.hh"


int main(int argc, char ** argv)
{
	vector<string> argvList(argv, argv+argc);
	ICE::LoreUnrollAndJamUIUC::commandLineProcessing(argvList);	
	
	SgProject * proj = frontend(argvList);
	ROSE_ASSERT(proj);

	cout << "[ICE/LoreUnrollAndJamUIUC]..." << endl;
	int res = ICE::LoreUnrollAndJamUIUC::unrollandjamAll(proj);

	//TODO specify the output name.
	proj->unparse();
	cout << "[ICE/LoreUnrollAndJamUIUC] end" << endl;

	return res;
}

int ICE::LoreUnrollAndJamUIUC::unrollandjamAll(SgProject * project) 
{
	size_t num_tiled = 0;
	int res = 0;
	// Search for pragmas for C/C++
	PragmaList_t pragmas;
	if(AnnotParser::collectPragmas(project, pragmas, PRAGMA_NAME))
	{
		for (PragmaList_t::iterator i = pragmas.begin(); i != pragmas.end(); ++i) {
			ICE::Result resopt = unrollandjam(*i);
			if(resopt.isValid()) {
				++num_tiled;
			} else {
				res = resopt.getValue();
				break;
			}
		}
	}
	return res;
}

//Tile the n-level (starting from 1)
ICE::Result ICE::LoreUnrollAndJamUIUC::unrollandjam(SgPragmaDeclaration* prag) 
{
//	SageInterface::dumpInfo(s);
	vector<int> pragmainfo;
	SgStatement* s = AnnotParser::processPragmaInfo (prag, PRAGMA_NAME, pragmainfo);
	if(!s) return Result(1);	

	// Expects two infos to do the unrollandjam.
	if(pragmainfo.size() != 2) {
     		cerr<<"Error in loopLoreUnrollAndJam, not proper info on the pragma."<<endl;
		return Result(1);
	}

	if(enable_debug) {
		cout << "pragma info: ";
		for(vector<int>::iterator it = pragmainfo.begin(); it != pragmainfo.end(); ++it){
			cout << *it << " ";
		}	
		cout << endl;
	}
	size_t targetLevel = pragmainfo[0];
	size_t factor = pragmainfo[1];

	ROSE_ASSERT(targetLevel >0);
	if (factor <= 1) return Result(0);

	SgPragmaDeclaration *newpragbefor, *newpragmafter;
	SIUI::addScopPragma(prag, newpragbefor, newpragmafter, isSgForStatement(s));
	cout << "Added Scop pragmas" << endl;
	
	DependenceGraph orig_dep_graph;
	orig_dep_graph.printDepSet(cout);

	dep_set_t orig_dep_set = orig_dep_graph.getDepSet();

	SgBasicBlock *root = isSgBasicBlock(orig_dep_graph.getRoot());
	SIUI::undoScopPragma(prag, newpragbefor, newpragmafter);
	if (root == NULL) {
		cout << "UnrollAndJam ABORTED! No proper SCoP is found in the input file." << endl;
		return Result(1);
	}

	bool legal = orig_dep_graph.isUnrollAndJamLegal(targetLevel - 1, factor);
	cout << "Legality: " << legal << endl;
	if(legal) {
		cout << "Legal!" << endl;
		return unrollandjam(s, targetLevel, factor);
	} else {
		cout <<  "NOT legal!" << endl;
		return Result(2);
	}
}

//Tile the n-level (starting from 1)
ICE::Result ICE::LoreUnrollAndJamUIUC::unrollandjam(SgStatement* s, size_t targetLevel, size_t factor)
{
	bool r;
//	SageInterface::dumpInfo(s);

	if (SgForStatement * for_loop = isSgForStatement(s)) {
		cout << "[ICE/LoreUnrollAndJamUIUC/unrollandjam] For loop " << targetLevel  << " " << factor << endl;
		r = unrollandjam(for_loop, targetLevel, factor);
	} else {
		cerr << "[ICE/LoreUnrollAndJamUIUC/unrollandjam] ERROR! SgStatement not a For neither a Do loop!" << endl;	
		r = false;
	}
	int ret = 1;
	if (r) { ret = 0;}
	else {ret = 1;}

	return Result(ret);
}


bool ICE::LoreUnrollAndJamUIUC::unrollandjam(SgForStatement* loopNest, size_t targetLevel, size_t factor)
{
	bool res = false;
	SgBasicBlock* enclosing_body = isSgBasicBlock(loopNest->get_parent());
	std::vector<SgForStatement* > loops= SageInterface::querySubTree<SgForStatement>(loopNest,V_SgForStatement);
	ROSE_ASSERT(loops.size()>=targetLevel);
	SgForStatement* target_loop = loops[targetLevel -1]; // adjust to numbering starting from 0

	size_t enclosing_size = enclosing_body->get_numberOfTraversalSuccessors();

	SgBasicBlock *target_body = isSgBasicBlock(target_loop->get_loop_body());
	ROSE_ASSERT(target_body != NULL);

//	cout << "OPA 1" << endl;
	if (target_body->get_numberOfTraversalSuccessors() == 1) {

//		cout << "OPA 1.5" << endl;
		int trip = SIUI::getForLoopTripCount(target_loop);
//		cout << "OPA 2" << endl;
		if (factor == 2 || trip >= factor || trip == -1) {
			if (SageInterface::loopUnrolling(target_loop, factor)) {
//				cout << "OPA 3" << endl;

				SgStatementPtrList stmts = target_body->get_statements();
				SgForStatement *inner_loop = isSgForStatement(stmts[0]);
				if (inner_loop == NULL) {
					cerr << "[ICE/LoreUnrollAndJamUIUC/unrollandjam] inner_loop is NULL!" << endl;
					return false;
				}	
				//ROSE_ASSERT(inner_loop != NULL);

				SgBasicBlock *inner_scope = isSgBasicBlock(inner_loop->get_loop_body());
				if (inner_scope == NULL) {
					cerr << "[ICE/LoreUnrollAndJamUIUC/unrollandjam] inner_scope is NULL!" << endl;
					return false;
				}
				//ROSE_ASSERT(inner_scope != NULL);

//				cout << "OPA 4" << endl;
				for (int j = 1; j < factor; ++j) {
//					cout << "OPA 4.1" << endl;
					SgBasicBlock *scope = isSgBasicBlock(stmts[j]);
					if (scope == NULL) {
						cerr << "[ICE/LoreUnrollAndJamUIUC/unrollandjam] scope is NULL!" << endl;
						return false;
					}
					//ROSE_ASSERT(scope != NULL);

//					cout << "OPA 4.2" << endl;
					SgForStatement *dup_loop = isSgForStatement(scope->get_traversalSuccessorByIndex(0));
					if (dup_loop == NULL) {
						cerr << "[ICE/LoreUnrollAndJamUIUC/unrollandjam] dup_loop is NULL!" << endl;
						return false;
					}
					//ROSE_ASSERT(dup_loop != NULL);

//					cout << "OPA 4.3" << endl;
					SgBasicBlock *dup_scope = isSgBasicBlock(dup_loop->get_loop_body());
					if (dup_scope == NULL) {
						cerr << "[ICE/LoreUnrollAndJamUIUC/unrollandjam] dup_scope is NULL!" << endl;
						return false;
					}
					//ROSE_ASSERT(dup_scope != NULL);

//					cout << "OPA 4.4" << endl;
					SgStatementPtrList dup_stmts = dup_scope->get_statements();
					for (SgStatementPtrList::iterator itr2 = dup_stmts.begin(); itr2 != dup_stmts.end(); ++itr2) {
						SgStatement *new_stmt = SageInterface::copyStatement(*itr2);
						SageInterface::appendStatement(new_stmt, inner_scope);
					}

//					cout << "OPA 4.5" << endl;
					SageInterface::removeStatement(scope);
					SageInterface::deepDelete(scope);
//					cout << "OPA 4.6" << endl;
				}
//				cout << "OPA 5" << endl;

				if (enclosing_body->get_numberOfTraversalSuccessors() == enclosing_size + 2) { // we have a residue loop
					SgForStatement *residue_loop = isSgForStatement(enclosing_body->get_traversalSuccessorByIndex(enclosing_body->get_childIndex(target_loop) + 1));
					if (residue_loop == NULL) {
						cerr << "[ICE/LoreUnrollAndJamUIUC/unrollandjam] residue_loop is NULL!" << endl;
						return false;
					}
					//ROSE_ASSERT(residue_loop != NULL);

					residue_loop->set_for_init_stmt(SageBuilder::buildForInitStatement(SageBuilder::buildNullStatement()));
					// need to fix this because ROSE's unrolling has a bug
				}

				cout << "Unroll-jammed at level " << targetLevel << " with factor " << factor << endl;
				res = true;
			}
		}
	}
	if (res == false) {
		cout << "Failed to unroll-jammed at level " << targetLevel << " with factor " << factor << endl;
	}
	return res;
}


void ICE::LoreUnrollAndJamUIUC::commandLineProcessing(std::vector<std::string> &argvList) {

  if (CommandlineProcessing::isOption (argvList,"-rose:loreunrollandjam:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for unrollandjam functions..."<<endl;
    enable_debug= true;
  }

/*  if (CommandlineProcessing::isOptionWithParameter(argvList,"-rose:unrollandjam:","target_level", target_level, true))
  {	
    cout<<"Target level is " << target_level << endl;	
  }

  if (CommandlineProcessing::isOptionWithParameter (argvList,"-rose:unrollandjam:","tile_size", tile_size, true))
  {	
    cout<<"Tile size is " << tile_size << endl;	
  }
*/

  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC LoreUnrollAndJam-specific options"<<endl;
    cout<<"Usage: loopLoreUnrollAndJam [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
    cout<<"\t-rose:loreunrollandjam:enable_debug                   run tiling in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }
}

