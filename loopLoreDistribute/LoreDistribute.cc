#include "LoreDistribute.hh"


int main(int argc, char ** argv)
{
	vector<string> argvList(argv, argv+argc);
	ICE::LoreDistributeUIUC::commandLineProcessing(argvList);	
	
	SgProject * proj = frontend(argvList);
	ROSE_ASSERT(proj);

	cout << "[ICE/LoreDistributeUIUC]..." << endl;
	int res = ICE::LoreDistributeUIUC::distributeAll(proj);

	//TODO specify the output name.
	proj->unparse();
	cout << "[ICE/LoreDistributeUIUC] end" << endl;

	return res;
}

int ICE::LoreDistributeUIUC::distributeAll(SgProject * project) 
{
	size_t num_tiled = 0;
	int res = 0;
	// Search for pragmas for C/C++
	PragmaList_t pragmas;
	if(AnnotParser::collectPragmas(project, pragmas, PRAGMA_NAME))
	{
		for (PragmaList_t::iterator i = pragmas.begin(); i != pragmas.end(); ++i) {
			ICE::Result resopt = distribute(*i);
			if(resopt.isValid()) {
				++num_tiled;
			} else {
				res = resopt.getValue();
				break;
			}
		}
	}
	return res;
}

ICE::Result ICE::LoreDistributeUIUC::distribute(SgPragmaDeclaration* prag) 
{
//	SageInterface::dumpInfo(s);
	vector<int> pragmainfo;
	SgStatement* s = AnnotParser::processPragmaInfo (prag, PRAGMA_NAME, pragmainfo);
	if(!s) return Result(1);	

	// Expects two infos to do the distribute.
	if(pragmainfo.size() != 1) {
     		cerr<<"Error in loopLoreDistribute, not proper info on the pragma."<<endl;
		return Result(1);
	}

	if(enable_debug) {
		cout << "pragma info: ";
		for(vector<int>::iterator it = pragmainfo.begin(); it != pragmainfo.end(); ++it){
			cout << *it << " ";
		}	
		cout << endl;
	}
	size_t targetLevel = pragmainfo[0];
	ROSE_ASSERT(targetLevel >0);

	SgPragmaDeclaration *newpragbefor, *newpragmafter;
	SIUI::addScopPragma(prag, newpragbefor, newpragmafter, isSgForStatement(s));
	cout << "Added Scop pragmas" << endl;
	
	DependenceGraph orig_dep_graph;
	orig_dep_graph.printDepSet(cout);

	dep_set_t orig_dep_set = orig_dep_graph.getDepSet();

	SgBasicBlock *root = isSgBasicBlock(orig_dep_graph.getRoot());
	SIUI::undoScopPragma(prag, newpragbefor, newpragmafter);
	if (root == NULL) {
		cout << "Distribute ABORTED! No proper SCoP is found in the input file." << endl;
		return Result(1);
	}

	vector<set<int> > disjoint_sets = orig_dep_graph.getDisjointStatmentSets();
	cout << "Number of disjoint_sets: " << disjoint_sets.size() << endl;
	if(disjoint_sets.size() > 1) {
		cout << "Going for distribution!" << endl;
		return distribute(s, targetLevel, disjoint_sets, orig_dep_graph);
	} else {
		cout <<  "NO distribute called!" << endl;
		return Result(0);
	}
}

ICE::Result ICE::LoreDistributeUIUC::distribute(SgStatement* s, size_t targetLevel,
	vector<set<int> > &disjoint_sets,
	const DependenceGraph &dep_graph)
{
	bool r;
//	SageInterface::dumpInfo(s);

	if (SgForStatement * for_loop = isSgForStatement(s)) {
		cout << "[ICE/LoreDistributeUIUC/distribute] For loop " << targetLevel  << " " << disjoint_sets.size()<< endl;
		r = distribute(for_loop, targetLevel, disjoint_sets, dep_graph);
	} else {
		cerr << "[ICE/LoreDistributeUIUC/distribute] ERROR! SgStatement not a For neither a Do loop!" << endl;	
		r = false;
	}
	int ret = 1;
	if (r) { ret = 0;}
	else {ret = 1;}
	
	return Result(ret);
}


bool ICE::LoreDistributeUIUC::distribute(SgForStatement* loopNest, size_t targetLevel, vector<set<int> > &disjoint_sets,
	const DependenceGraph &dep_graph)
{
	bool res = false;
	SgBasicBlock* enclosing_body = isSgBasicBlock(loopNest->get_parent());
	std::vector<SgForStatement* > loops= SageInterface::querySubTree<SgForStatement>(loopNest,V_SgForStatement);
	ROSE_ASSERT(loops.size()>=targetLevel);
	SgForStatement* target_loop = loops[targetLevel -1]; // adjust to numbering starting from 0

        int cnt = 0;
        for (vector<set<int> >::iterator v_itr = disjoint_sets.begin(); v_itr != disjoint_sets.end(); ++v_itr) {

            vector<SgExprStatement *> expr_list;
            for (set<int>::iterator s_itr = v_itr->begin(); s_itr != v_itr->end(); ++s_itr) {
                SgExprStatement *expr = dep_graph.getStatementById(*s_itr);
                if (SageInterface::isAncestor(target_loop, expr)) {
                    //cout << expr->unparseToString() << endl;
                    expr_list.push_back(isSgExprStatement(SageInterface::copyStatement(expr)));
                }
            }

            if (expr_list.size()) {

                SgForStatement *new_loop = isSgForStatement(SageInterface::copyStatement(target_loop));
                SageInterface::insertStatement(target_loop, new_loop);

                SgStatement *old_body = new_loop->get_loop_body();
                SgBasicBlock *new_body = SageBuilder::buildBasicBlock();

                for (vector<SgExprStatement *>::iterator expr_itr = expr_list.begin(); expr_itr != expr_list.end(); ++expr_itr) {
                    SageInterface::appendStatement(*expr_itr, new_body);
                }

                new_loop->set_loop_body(new_body);
                SageInterface::deepDelete(old_body);

                cnt++;

            }

        }

        if (cnt > 1) {
            cout << "Distributed a loop into " << cnt << " loops" << endl;
            res = true;
        }

        SageInterface::removeStatement(target_loop);
        SageInterface::deepDelete(target_loop);

	return res;
}


void ICE::LoreDistributeUIUC::commandLineProcessing(std::vector<std::string> &argvList) {

  if (CommandlineProcessing::isOption (argvList,"-rose:loredistribute:","enable_debug",true))
  {
    cout<<"Enabling debugging mode for distribute functions..."<<endl;
    enable_debug= true;
  }

/*  if (CommandlineProcessing::isOptionWithParameter(argvList,"-rose:distribute:","target_level", target_level, true))
  {	
    cout<<"Target level is " << target_level << endl;	
  }

  if (CommandlineProcessing::isOptionWithParameter (argvList,"-rose:distribute:","tile_size", tile_size, true))
  {	
    cout<<"Tile size is " << tile_size << endl;	
  }
*/

  // keep --help option after processing, let other modules respond also
  if ((CommandlineProcessing::isOption (argvList,"--help","",false)) ||
      (CommandlineProcessing::isOption (argvList,"-help","",false)))
  {
    cout<<"UIUC LoreDistribute-specific options"<<endl;
    cout<<"Usage: loopLoreDistribute [OPTION]... FILENAME..."<<endl;
    cout<<"Main operation mode:"<<endl;
    cout<<"\t-rose:loredistribute:enable_debug                   run distribute in a debugging mode"<<endl;
    cout <<"---------------------------------------------------------------"<<endl;
  }
}

