#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cstdio>

#include "rose.h"

#include <CPPAstInterface.h>
#include <ArrayAnnot.h>
#include <ArrayRewrite.h>

#include <AstInterface_ROSE.h>
#include <LoopTransformInterface.h>
#include <AnnotCollect.h>
#include <OperatorAnnotation.h>

//#include <candl/candl.h>
//#include <scoplib/scop.h>

//#include <polyopt/PolyOpt.hpp>
//#include <polyopt/ScopExtractor.hpp>
//#include <polyopt/SageNodeAnnotation.hpp>

//#include <utils.hh>
//#include <transformation.hh>
//#include <dependence.hh>
//#include <analysis.hh>
//#include <database.hh>
//#include <config.hh>
//#include <staticfeature.hh>

using namespace std;
//using namespace restructurer;

// Copied and replaced the asserts!
bool ThiagogetForLoopInformations(
  SgForStatement * for_loop,
  SgVariableSymbol * & iterator,
  SgExpression * & lower_bound,
  SgExpression * & upper_bound,
  SgExpression * & stride
) {
  /// \todo handle more case. For example: declaration in initialization
  /// \todo replace most assertions by error messages and 'return false;'

  iterator = NULL;
  lower_bound = NULL;
  upper_bound = NULL;
  stride = NULL;

  SgForInitStatement * for_init_stmt = for_loop->get_for_init_stmt();
  const std::vector<SgStatement *> & init_stmts = for_init_stmt->get_init_stmt();

  SgStatementPtrList &init_b = for_loop ->get_init_stmt();
  if (init_b.size() !=1) { // We only handle one statement case
	  cout << "Opa init size is bblah!"  << endl;
  }

  //assert(init_stmts.size() == 1);
  //cout << "init_stmts size " << init_stmts.size() << endl;
  //SageInterface::dumpInfo(init_stmts[0]);
  //cout << "000000" << endl;
  //SageInterface::dumpInfo(init_stmts[1]);
  if(init_stmts.size() != 1) return false;
  SgExprStatement * init_stmt = isSgExprStatement(init_stmts[0]);
  if(init_stmt == NULL) return false;
  SgExpression * init = init_stmt->get_expression();

  SgAssignOp * assign_init = isSgAssignOp(init);
  if(assign_init == NULL) return false;
  SgVarRefExp * iterator_init_ref = isSgVarRefExp(assign_init->get_lhs_operand_i());
  if(iterator_init_ref == NULL) return false;
  iterator = iterator_init_ref->get_symbol();
  if(iterator == NULL) return false;
  lower_bound = assign_init->get_rhs_operand_i();

  SgExprStatement * test_stmt = isSgExprStatement(for_loop->get_test());
  if(test_stmt == NULL) return false;
  SgExpression * test = test_stmt->get_expression();
  SgBinaryOp * bin_test = isSgBinaryOp(test);
  assert(bin_test);

  SgExpression * lhs_exp = bin_test->get_lhs_operand_i();
  while (isSgCastExp(lhs_exp)) lhs_exp = ((SgCastExp *)lhs_exp)->get_operand_i();
  SgVarRefExp * lhs_var_ref = isSgVarRefExp(lhs_exp);
  bool lhs_it = (lhs_var_ref != NULL) && (lhs_var_ref->get_symbol() == iterator);

  SgExpression * rhs_exp = bin_test->get_rhs_operand_i();
  while (isSgCastExp(rhs_exp)) rhs_exp = ((SgCastExp *)rhs_exp)->get_operand_i();
  SgVarRefExp * rhs_var_ref = isSgVarRefExp(rhs_exp);
  bool rhs_it = (rhs_var_ref != NULL) && (rhs_var_ref->get_symbol() == iterator);

// DQ (4/21/2016): Replacing use of bitwise xor with something more approriate for logical types.
// Note that the xor logica operator does not exist in C/C++ and that this is a case of using the
// bitwise xor operator on boolean values (not a great idea).  Note that logical "a xor b" is 
// equivalent to "!a != !b"  the use of "!" only make sure that the "!=" is applied to a boolean 
// value.  Since these are boolean typed values we can use "a != b", directly.
// assert(lhs_it xor rhs_it);
  assert(lhs_it != rhs_it);

  upper_bound = lhs_it ? bin_test->get_rhs_operand_i() : bin_test->get_lhs_operand_i();

  bool inclusive;
  bool reversed;

  switch (test->variantT()) {
    case V_SgGreaterOrEqualOp:
      inclusive = lhs_it;
      reversed = lhs_it;
      break;
    case V_SgGreaterThanOp:
      inclusive = !lhs_it;
      reversed = lhs_it;
      break;
    case V_SgLessOrEqualOp:
      inclusive = lhs_it;
      reversed = !lhs_it;
      break;
    case V_SgLessThanOp:
      inclusive = !lhs_it;
      reversed = !lhs_it;
      break;
    case V_SgEqualityOp:
    case V_SgNotEqualOp:
    default:
      assert(false);
  }

  SgExpression * increment = for_loop->get_increment();
  switch (increment->variantT()) {
    case V_SgPlusPlusOp:
      assert(!reversed);
      stride = SageBuilder::buildIntVal(1);
      break;
    case V_SgMinusMinusOp:
      assert(reversed);
      stride = SageBuilder::buildIntVal(-1);
      break;
    case V_SgPlusAssignOp:
    {
      SgBinaryOp * bin_op = (SgBinaryOp *)increment;
      SgVarRefExp * var_ref_lhs = isSgVarRefExp(bin_op->get_lhs_operand_i());
      assert(var_ref_lhs != NULL && var_ref_lhs->get_symbol() == iterator);
      stride = bin_op->get_rhs_operand_i();
      break;
    }
    case V_SgMinusAssignOp:
    {
      SgBinaryOp * bin_op = (SgBinaryOp *)increment;
      SgVarRefExp * var_ref_lhs = isSgVarRefExp(bin_op->get_lhs_operand_i());
      assert(var_ref_lhs != NULL && var_ref_lhs->get_symbol() == iterator);
      stride = bin_op->get_rhs_operand_i();
      break;
    }
    case V_SgAssignOp:
    {
      SgAssignOp * assign_op = (SgAssignOp *)increment;
      SgVarRefExp * inc_assign_lhs = isSgVarRefExp(assign_op->get_lhs_operand_i());
      assert(inc_assign_lhs != NULL && inc_assign_lhs->get_symbol() == iterator);
      SgBinaryOp * inc_assign_rhs = isSgBinaryOp(assign_op->get_rhs_operand_i());
      if(inc_assign_rhs == NULL) return false;;
      SgVarRefExp * inc_assign_rhs_lhs = isSgVarRefExp(inc_assign_rhs->get_lhs_operand_i());
      if (inc_assign_rhs_lhs != NULL && inc_assign_rhs_lhs->get_symbol() == iterator)
        stride = inc_assign_rhs->get_rhs_operand_i();
      SgVarRefExp * inc_assign_rhs_rhs = isSgVarRefExp(inc_assign_rhs->get_rhs_operand_i());
      if (inc_assign_rhs_rhs != NULL && inc_assign_rhs_rhs->get_symbol() == iterator)
        stride = inc_assign_rhs->get_lhs_operand_i();
      break;
    }
    default:
      assert(false);
  }

  // DQ (7/19/2015): Added braces to avoid compiler warning about ambigious "else" case.
     if (!inclusive)
        {
          if (reversed)
               upper_bound = SageBuilder::buildAddOp(upper_bound, SageBuilder::buildIntVal(1));
            else
               upper_bound = SageBuilder::buildSubtractOp(upper_bound, SageBuilder::buildIntVal(1));
        }

  return true;
}

int main(int argc, char* argv[]) {

    SgStringList args = CommandlineProcessing::generateArgListFromArgcArgv(argc, argv);
    SgStringList files = CommandlineProcessing::generateSourceFilenames(args, false);
    ROSE_ASSERT(files.size() == 1);

    string orig_file = files[0];
    string new_file = orig_file + ".preproc.c";


    SgProject* project = frontend(args);

    ROSE_ASSERT(project != NULL);

    SageInterface::changeAllBodiesToBlocks(project);

    SgBasicBlock *body = NULL;
    bool skip_interchange_tiling = false;

    VariantVector vv_func(V_SgFunctionDefinition); 
    Rose_STL_Container<SgNode*> funcion_list = NodeQuery::queryMemoryPool(vv_func);

    for (Rose_STL_Container<SgNode*>::iterator f_itr = funcion_list.begin(); f_itr != funcion_list.end(); ++f_itr) {

        SgFunctionDefinition *cur_func = isSgFunctionDefinition(*f_itr);
        string name = cur_func->get_declaration()->get_name().getString();
        SgBasicBlock *func_body = cur_func->get_body();
        if (name == "loop" && func_body) {

            body = func_body;

            set<SgVariableSymbol *> local_set;
            set<SgVariableSymbol *> itr_set;
            vector<SgExprStatement *> reassigns;

            Rose_STL_Container<SgNode *> loop_list = NodeQuery::querySubTree(body, V_SgForStatement);


            set<SgInitializedName *> r_vars;
            set<SgInitializedName *> w_vars;

            SageInterface::collectReadWriteVariables(body, r_vars, w_vars);

            set<SgSymbol *> w_syms;
            for (set<SgInitializedName *>::iterator s_itr = w_vars.begin(); s_itr != w_vars.end(); ++s_itr) {
                SgVariableSymbol * w_sym = isSgVariableSymbol((*s_itr)->search_for_symbol_from_symbol_table());
                w_syms.insert(w_sym);
            }


            for (Rose_STL_Container<SgNode*>::iterator l_itr = loop_list.begin(); l_itr != loop_list.end(); ++l_itr) {

                SgForStatement *cur_loop = isSgForStatement(*l_itr);

                SgStatement *ub = cur_loop->get_test();
                SgExprStatement* estmtub = isSgExprStatement(ub);
                
                if (estmtub != NULL) {
                    SgExpression *eub = estmtub->get_expression();
                    Rose_STL_Container<SgNode *> ref_list = NodeQuery::querySubTree(eub, V_SgPntrArrRefExp);

                    for (Rose_STL_Container<SgNode*>::iterator r_itr = ref_list.begin(); r_itr != ref_list.end(); ++r_itr) {
                        SgPntrArrRefExp *cur_ref = isSgPntrArrRefExp(*r_itr);

                        SgType *type = cur_ref->get_type();
                        bool is_final = type->isIntegerType() || type->isFloatType();
                        if (is_final) {

                            bool changed = false;
                            Rose_STL_Container<SgNode *> var_list = NodeQuery::querySubTree(cur_ref, V_SgVarRefExp);
                            for (Rose_STL_Container<SgNode*>::iterator v_itr = var_list.begin(); v_itr != var_list.end(); ++v_itr) {
                                SgVarRefExp *cur_var = isSgVarRefExp(*v_itr);
                                SgSymbol *sym = cur_var->get_symbol();

                                if (w_syms.find(sym) != w_syms.end()) {
                                    changed = true;
                                    break;
                                }

                            }

                            if (changed) {
                                continue;
                            }

                            // this function is bugged
                            string new_name = SageInterface::generateUniqueVariableName(body, "arr_sub_");

                            //stringstream ss;
                            //ss << "arr_sub_" << var_name_cnt++; 
                            //string new_name(ss.str());
                            
                            SgAssignInitializer *init = SageBuilder::buildAssignInitializer(isSgPntrArrRefExp(SageInterface::copyExpression(cur_ref)), type);
                            SgVariableDeclaration *new_def = SageBuilder::buildVariableDeclaration(new_name, type, init, body);

                            SgVarRefExp *new_ref = SageBuilder::buildVarRefExp(new_name);
                            
                            SgStatement *last_decl = SageInterface::findLastDeclarationStatement(body);
                            if (last_decl) {
                                SageInterface::insertStatementAfter(last_decl, new_def);
                            } else {
                                SageInterface::prependStatement(new_def, body);
                            }
                            SageInterface::replaceExpression(cur_ref, new_ref);

                        }

                    }

                }

            }


            for (Rose_STL_Container<SgNode*>::iterator l_itr = loop_list.begin(); l_itr != loop_list.end(); ++l_itr) {

                SgForStatement *cur_loop = isSgForStatement(*l_itr);

		//SageInterface::dumpInfo(cur_loop);
                //SageInterface::forLoopNormalization(cur_loop);
                SageInterface::splitVariableDeclaration(cur_loop);

                SgVariableSymbol *for_ierator;
                SgExpression * lower_bound;
                SgExpression * upper_bound;
                SgExpression * stride;
                //bool success = SageInterface::getForLoopInformations(cur_loop, for_ierator, lower_bound, upper_bound, stride);
                bool success = ThiagogetForLoopInformations(cur_loop, for_ierator, lower_bound, upper_bound, stride);
                if (success) {

                    vector<SgVariableSymbol *> symbols = SageInterface::getSymbolsUsedInExpression(lower_bound);
                    vector<SgVariableSymbol *> ub_symbols = SageInterface::getSymbolsUsedInExpression(upper_bound);
                    vector<SgVariableSymbol *> st_symbols = SageInterface::getSymbolsUsedInExpression(stride);

                    symbols.insert(symbols.end(), ub_symbols.begin(), ub_symbols.end());
                    symbols.insert(symbols.end(), st_symbols.begin(), st_symbols.end());

                    for (vector<SgVariableSymbol *>::iterator symbol_itr = symbols.begin(); symbol_itr != symbols.end(); ++symbol_itr) {
                        if (local_set.find(*symbol_itr) != local_set.end()) {
                            skip_interchange_tiling = true;
                            cout << "Skip interchange and tiling" << endl;
                            break;
                        }
                    }

                    //local_set.insert(isSgVariableSymbol(for_ierator));
                    if (itr_set.find(isSgVariableSymbol(for_ierator)) == itr_set.end()) {

                        SgInitializedName *init_name = for_ierator->get_declaration();

                        SgAssignInitializer* itr_init = SageBuilder::buildAssignInitializer(SageBuilder::buildVarRefExp(init_name->get_name()),
                            SageBuilder::buildIntType());

                        string new_name = SageInterface::generateUniqueVariableName(body, init_name->get_name().getString() + string("_"));

                        SgVariableDeclaration *new_def = SageBuilder::buildVariableDeclaration(new_name, SageBuilder::buildIntType(), itr_init, body);

                        SgStatement *last_decl = SageInterface::findLastDeclarationStatement(body);
                        if (last_decl) {
                            SageInterface::insertStatementAfter(last_decl, new_def);
                        } else {
                            SageInterface::prependStatement(new_def, body);
                        }

                        SgInitializedName *new_init_name = new_def->get_decl_item(new_name);
                        SgSymbol *new_symbol = new_init_name->search_for_symbol_from_symbol_table();

                        SageInterface::replaceVariableReferences(isSgVariableSymbol(for_ierator), isSgVariableSymbol(new_symbol), body);
                        local_set.insert(isSgVariableSymbol(new_symbol));
                        itr_set.insert(isSgVariableSymbol(new_symbol));

                        SgExprStatement *reassign = SageBuilder::buildAssignStatement(SageBuilder::buildVarRefExp(init_name->get_name()),
                            SageBuilder::buildVarRefExp(new_init_name->get_name()));
                        reassigns.push_back(reassign);
                    }

                }
                //SageInterface::clearUnusedVariableSymbols(body);
                //cur_loop->set_symbol_table(NULL);
                //SageInterface::rebuildSymbolTable(cur_loop);
                //SageInterface::forLoopNormalization(cur_loop);

                SgBasicBlock *loop_body = SageInterface::ensureBasicBlockAsBodyOfFor(cur_loop);
                ROSE_ASSERT(loop_body);


                Rose_STL_Container<SgNode *> def_list = NodeQuery::querySubTree(loop_body, V_SgVariableDeclaration);
                for (Rose_STL_Container<SgNode *>::iterator d_itr = def_list.begin(); d_itr != def_list.end(); ++d_itr) {

                    SgVariableDeclaration *decl = isSgVariableDeclaration(*d_itr);


                    SageInterface::splitVariableDeclaration(decl);

                    SgInitializedNamePtrList name_list = decl->get_variables();
                    for (SgInitializedNamePtrList::iterator n_itr = name_list.begin(); n_itr != name_list.end(); ++n_itr) {
                        
                        SgInitializedName *init_name = isSgInitializedName(*n_itr);
                        SgSymbol *old_symbol = init_name->search_for_symbol_from_symbol_table();

                        string new_name = SageInterface::generateUniqueVariableName(body, init_name->get_name().getString() + string("_"));

                        SgVariableDeclaration *new_def = SageBuilder::buildVariableDeclaration(new_name, init_name->get_type(), NULL, body);                        
                        SgStatement *last_decl = SageInterface::findLastDeclarationStatement(body);
                        if (last_decl) {
                            SageInterface::insertStatementAfter(last_decl, new_def);
                        } else {
                            SageInterface::prependStatement(new_def, body);
                        }

                        SgInitializedName *new_init_name = new_def->get_decl_item(new_name);
                        SgSymbol *new_symbol = new_init_name->search_for_symbol_from_symbol_table();

                        SageInterface::replaceVariableReferences(isSgVariableSymbol(old_symbol), isSgVariableSymbol(new_symbol), loop_body);

                        local_set.insert(isSgVariableSymbol(new_symbol));

                    }

                    SageInterface::removeStatement(decl);
                    SageInterface::deepDelete(decl);
                }


            }


            for (vector<SgExprStatement *>::iterator itr = reassigns.begin(); itr != reassigns.end(); ++itr) {
                SageInterface::appendStatement(*itr, body);
            }

        }
    }

    SgFile &file = project->get_file(0);
    file.set_unparse_output_filename(new_file);

    project->unparse();

    //CommandlineProcessing::removeAllFileNamesExcept(args, files, string());
    //if (skip_interchange_tiling) {
    //    args.push_back("--skipinterchangetiling");
   // }
    //args.push_back(new_file);

    //string cmd(args[0].substr(0, args[0].size() - 7) + "restructurer");
    //SgStringList::iterator itr = args.begin();
    //for (++itr; itr != args.end(); ++itr) {
    //    cmd += " " + *itr;
    //}

    //cout << "Execute: " << cmd.c_str() << endl;
    return 0; //system(cmd.c_str());

}
